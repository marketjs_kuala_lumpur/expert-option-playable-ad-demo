
var GameHelper = function(){
    
};

GameHelper.prototype={
    
    startGame:function(startGameFn,time){
        window.setTimeout(startGameFn, time);
    },
    
    initSound:function(){
        if(Howler){
            
        }
    },
    
    
    triggerUserAction: function() {
        ig.game.userAction++;
        
        if(ig.game.userAction === 1) {
            // first user action
            ig.game.triggerFirstUserAction();
        }
		else if(ig.game.userAction === 2) {
			ig.game.triggerSecondUserAction();
		}
    },
    
    triggerFirstUserAction: function() {
        //Analytics
        //first_user_action
        //PlayableSdk.track.gameAction("first_user_action");
        
		if(igh.settings.Property1 === "on" ) {
			igh.disableCloseButton();
		}
		
		if(igh.settings.Property4 === "on") {
			window.setTimeout(igh.disableCloseButton, 10*1000);
		} 
        
    },
	
    triggerSecondUserAction: function() {
        if(igh.settings.Property2 === "on")
        {
            igh.disableCloseButton();
        }            
    },
    
    clone:function(){
        
        var output = new GameHelper();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
GameHelper.prototype.constructor = GameHelper;