var SystemHelper = function(){
    this.gameSetting={};
    this.EVENTS={
        CLOSE_BUTTON_INIT:"closeButtonInit",
        ORIENTATION_CHANGE:"orientationchange",
        RESIZE:"resize",
        MRAID_STATE_CHANGE:"MRAIDStateChange",
        ERROR:"error"
    };
    this.callbacks={
        onCloseButtonInit:[],
        onOrientationChange:[],
        onSizeChange:[],
        MRAIDStateChange:[],
    };
};

SystemHelper.prototype={
    
    registerCallback:function(event,callback){
        switch(event){
            case this.EVENTS.CLOSE_BUTTON_INIT:
                this.callbacks.onCloseButtonInit.push(callback);
                
            break;
            case this.EVENTS.ORIENTATION_CHANGE:
                this.callbacks.onOrientationChange.push(callback);
                window.addEventListener(this.EVENTS.ORIENTATION_CHANGE,callback);
            break;
            case this.EVENTS.RESIZE:
                this.callbacks.onSizeChange.push(callback);
                window.addEventListener(this.EVENTS.RESIZE,callback);
            break;
            case this.EVENTS.MRAID_STATE_CHANGE:
                this.callbacks.MRAIDStateChange.push(callback);
            break;
            case "error":
                //this.errorHandler.registerErrorCallback(t);
            break;
            default:
                console.error("registerCallback failed: `" + e + "` is not a valid eventName.")
            break;
        }
    },
   
    clone:function(){
        
        var output = new SystemHelper();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
SystemHelper.prototype.constructor = SystemHelper;
