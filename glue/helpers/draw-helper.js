
var DrawHelper = function(){
};

DrawHelper.prototype={
    
    
    setFillText:function(){
        
    },
    
    drawSprite: function(context, spriteImage, spriteSheet, spriteID, x, y, settings) {
        var context = context,
            frame = spriteSheets[spriteSheet].frames[spriteID].frame,
            size = spriteSheets[spriteSheet].frames[spriteID].size,
            settings = MJS.view.mergeSettings(
                {
                    pivot : { x:   0.5, y:   0.5 },
                    scale : { x:   1.0, y:   1.0 },
                    flip  : { x: false, y: false },
                    fill  : { x:   1.0, y:   1.0 },
                    alpha : 1,
                    angle : 0
                },
                settings
            );
            
        var scaleX = settings.scale.x*(settings.flip.x ? -1 : 1), 
            scaleY = settings.scale.y*(settings.flip.y ? -1 : 1);

        var frameOffsetRight = size.w - frame.ox - frame.w,
            frameOffsetBottom = size.h - frame.oy - frame.h;

        context.globalAlpha = (settings.alpha).limit(0,1);

        context.translate(
            x, y
        );
        
        if(settings.angle) {
            context.rotate( settings.angle );
        }
      
        if( settings.scale.x || settings.scale.y || settings.flip.x || settings.flip.y ) {
            context.scale( scaleX, scaleY );
        }

        context.drawImage(
            spriteImage.data,
            frame.x, frame.y, 
            frame.w*settings.fill.x, frame.h*settings.fill.y,
            ( (-settings.pivot.x*size.w) ) + 
            ( settings.flip.x ? 
                -frame.w-frameOffsetRight : 
                frame.ox
            ), 
            ( (-settings.pivot.y*size.h) ) + 
            ( settings.flip.y ? 
                -frame.w-frameOffsetBottom : 
                frame.oy
            ),			
            frame.w*settings.fill.x,
            frame.h*settings.fill.y
        );
      
        if( settings.scale.x || settings.scale.y || settings.flip.x || settings.flip.y ) {
            context.scale( 
                1/scaleX, 
                1/scaleY 
            );
        }

        if(settings.angle) {
            context.rotate( -settings.angle );
        }

        context.translate(
            -x, -y
        );

        context.globalAlpha = 1;
    },
    
    clone:function(){
        
        var output = new DrawHelper();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
DrawHelper.prototype.constructor = DrawHelper;
