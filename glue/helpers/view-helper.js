var ViewHelper = function(){
    this.portraitWidth=540;
    this.portraitHeight=960;
    this.landscapeWidth=960;
    this.landscapeHeight=540;
    this.forceOrientation="";// can be "portrait", "landscape" or ""/false/null
    this.changeSize=0;
    
    this.changeSize=this.getBrowserWidth();
    //igh.updateSettings();
    
    //igh.globalizeHandlers();
    //igh.applovinSupport();
    
    this.viewportSettings={};
    
    
    this.gameBox=document.getElementById(CONSTANTS.GAMEBOX);
    this.canvas= document.getElementById(CONSTANTS.CANVAS);
};

ViewHelper.prototype={
    
    initViewport:function(){
        this.viewport= {forceOrientation: this.forceOrientation, 
            portraitWidth: this.portraitWidth, 
            portraitHeight: this.portraitHeight, 
            landscapeWidth: this.landscapeWidth, 
            landscapeHeight: this.landscapeHeight, 
        
            widthRatio: 1, 
            heightRatio: 1,
            orientation: {
                portrait: false,
                landscape: false,
                degree: 0
            },
            screenWidth: this.getBrowserWidth(),
            screenHeight: this.getBrowserHeight()
        };
        
        // alias for backward compatibility
        this.viewportSettings = this.viewport;
    },
    
    globalizeHandlers: function() {
        
        window.orientationDelayHandler = this.orientationDelayHandler.bind(this);
        window.orientationHandler = this.orientationHandler.bind(this);
        window.setupCloseButton = MJS.ad.setupCloseButton.bind(this);
        
        // alias for backward compatibility
        window.initViewport = MJS.viewinitViewport;
        window.updateViewport = MJS.viewupdateViewport;
    },
    
    applovinSupport: function() {
        // support for applovin script: https://assets.applovin.com/js/al_ext.js
        if( typeof(al_updateViewportOverride) == "function" ) {
            window.updateViewport = al_updateViewportOverride;
        }
        MJS.viewupdateViewport = window.updateViewport;
    },
    
    updateCanvasDirection: function() {
        var canvas = document.getElementById("canvas");
        
        var language = MJS.settings.data.language;
        switch(language) {
            case "ar" :
                // right to left 
                canvas.setAttribute("dir", "rtl");
            break;
        
            default : 
                // left to right 
                canvas.setAttribute("dir", "ltr");
            break;
        }
    },
    
    updateViewport: function() {
        if(!MJS.view.viewport) {
            return false;
        }
    
        /*
        workaround: fixing weird igh.getBrowserWidth(), igh.getBrowserHeight() bug in mraid
        */
        if(window.mraid) {
            var tempScreen = {
                origWidth : MJS.view.viewport.screenWidth,
                origHeight: MJS.view.viewport.screenHeight
            };
            // iOS hack: to avoid igh.getBrowserWidth(), igh.getBrowserHeight() = 1 initially
            if( !(tempScreen.origWidth > 1 && tempScreen.origHeight > 1) ) {
                tempScreen.origWidth = MJS.view.getBrowserWidth();
                tempScreen.origHeight = MJS.view.getBrowserHeight();
            }
            
            if(MJS.view.getBrowserWidth() <= MJS.view.getBrowserHeight()) {
                //Portrait
                if(MJS.view.isiOS()){
                    tempScreen.origWidth=window.screen.width
                    tempScreen.origHeight = window.screen.height
                }
                
                if(ig.ua.android){
                    tempScreen.origWidth=window.screen.width
                    tempScreen.origHeight=window.screen.height
                }
                
                MJS.view.viewport.screenWidth = Math.min(tempScreen.origWidth,tempScreen.origHeight);
                MJS.view.viewport.screenHeight = Math.max(tempScreen.origWidth,tempScreen.origHeight);
            }
            else {
                //landscape
                if(MJS.view.isiOS()){
                    tempScreen.origWidth=window.screen.width;
                    tempScreen.origHeight = window.screen.height;
                }
                
                if(ig.ua.android){
                    tempScreen.origWidth=window.screen.width
                    tempScreen.origHeight=window.screen.height
                } 
                
                MJS.view.viewport.screenWidth = Math.max(tempScreen.origWidth,tempScreen.origHeight);
                MJS.view.viewport.screenHeight = Math.min(tempScreen.origWidth,tempScreen.origHeight);
            }
        }else {
           MJS.view.viewport.screenWidth = MJS.view.getBrowserWidth();
           MJS.view.viewport.screenHeight = MJS.view.getBrowserHeight();
       }
        /*
        var div = ig.$new("div");
        div.setAttribute("id","test");
            
                var cssString = "" +
                    "position: " + "absolute" + ";" +
                    "left: " + 10 + "px;" +
                    "top: " + 10 + "px;" +
                    "z-index: " + 1000 + "; ";
        
        div.setAttribute("style",cssString);
        div.innerHTML=(igh.getBrowserWidth());
        
        var div2 = ig.$new("div");
        div2.setAttribute("id","test2");
            
                var cssString2 = "" +
                    "position: " + "absolute" + ";" +
                    "left: " + 100 + "px;" +
                    "top: " + 10 + "px;" +
                    "z-index: " + 1000 + "; ";
        
        div2.setAttribute("style",cssString2);
        div2.innerHTML=(igh.getBrowserHeight());
        
        // append to document
        gamebox.appendChild(div);
        gamebox.appendChild(div2);
        */
        
        switch(MJS.view.viewport.forceOrientation) {
            case "portrait":
                MJS.view.viewport.desktopWidth = MJS.view.viewport.portraitWidth;
                MJS.view.viewport.desktopHeight = MJS.view.viewport.portraitHeight;
                MJS.view.viewport.mobileWidth = MJS.view.viewport.portraitWidth;
                MJS.view.viewport.mobileHeight = MJS.view.viewport.portraitHeight;
            break;
            
            case "landscape":
                MJS.view.viewport.desktopWidth = MJS.view.viewport.landscapeWidth;
                MJS.view.viewport.desktopHeight = MJS.view.viewport.landscapeHeight;
                MJS.view.viewport.mobileWidth = MJS.view.viewport.landscapeWidth;
                MJS.view.viewport.mobileHeight = MJS.view.viewport.landscapeHeight;
            break;
            
            default:
                if(MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight) {
                    
                    MJS.view.viewport.desktopWidth = MJS.view.viewport.portraitWidth;
                    MJS.view.viewport.desktopHeight = MJS.view.viewport.portraitHeight;
                    MJS.view.viewport.mobileWidth = MJS.view.viewport.portraitWidth;
                    MJS.view.viewport.mobileHeight = MJS.view.viewport.portraitHeight;
                }
                else {
                    MJS.view.viewport.desktopWidth = MJS.view.viewport.landscapeWidth;
                    MJS.view.viewport.desktopHeight = MJS.view.viewport.landscapeHeight;
                    MJS.view.viewport.mobileWidth = MJS.view.viewport.landscapeWidth;
                    MJS.view.viewport.mobileHeight = MJS.view.viewport.landscapeHeight;
                }
            break;
        }
    },
    
    setupPage: function() {
        // styling game box
        var gamebox = document.getElementById("gamebox");
        gamebox.style.position = "absolute";
        gamebox.style.top = 0+"px";
        gamebox.style.left = 0+"px";
    
        this.updateCanvasDirection();
    
        if(ig.ua.iOS)
        {
            MJS.sys.registerCallback(MJS.sys.EVENTS.ORIENTATION_CHANGE,this.orientationDelayHandler);
            MJS.sys.registerCallback(MJS.sys.EVENTS.RESIZE,this.orientationDelayHandler);
            
            // viewport
            //PlayableSdk.registerCallback("orientationChange", orientationDelayHandler);
            //PlayableSdk.registerCallback("sizeChange", orientationDelayHandler);
        }
        else
        {
            MJS.sys.registerCallback(MJS.sys.EVENTS.ORIENTATION_CHANGE,this.orientationHandler);
            MJS.sys.registerCallback(MJS.sys.EVENTS.RESIZE,this.orientationHandler);
            
            // viewport
            //PlayableSdk.registerCallback("orientationChange", orientationHandler);
            //PlayableSdk.registerCallback("sizeChange", orientationHandler);
        }
        
        
        MJS.sys.registerCallback(MJS.sys.EVENTS.CLOSE_BUTTON_INIT,MJS.ad.setupCloseButton);
        
        // close button
        //ig.MJSSDK.registerCallback("closeButtonInit", igh.setupCloseButton);
        
        // Prevent page from scrolling
        document.ontouchmove = function( event ) { 
            window.scrollTo(0, 0);
            event.preventDefault();
        };
    },
    
    
    
    
    orientationHandler: function()
    {
        if(MJS.view.changeSize === MJS.view.getBrowserWidth())
        {
            
        }
        else
        {
            MJS.view.changeSize=MJS.view.getBrowserWidth();
            MJS.view.clearAllIntervals();
        }
        
        //console.log("orientationhandler");
        if(!MJS.view.viewport) {
            return false;
        }
        MJS.view.updateViewport();
        
        var gamebox = document.getElementById("gamebox");
        var canvas = document.getElementById("canvas"); 
    
        var degree = 0;
        var changedOrientation = false;
    
        // orientation degree
        if( window.screen ) {
            if( window.screen.orientation ) {
                if( !isNaN(window.screen.orientation.degree) ) {
                    degree = window.screen.orientation.degree;
                }else if(!isNaN(window.screen.orientation.angle)){
                    degree = window.screen.orientation.angle;
                }
            }
            else
            {
                if( !isNaN(window.orientation) ) {
                    degree = window.orientation;
                }
            }
        } 
        else if( !isNaN(window.orientation) ) {
            degree = window.orientation;
        }
    
        switch(MJS.view.viewport.forceOrientation) {
            case "portrait":
                // always portrait
                if(MJS.view.viewport.orientation) {
                        
                    if(MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.portrait) {
            
                        if( Math.abs(degree)%180 !== 0 || !degree ) degree = 0;
                        if( !isNaN(degree) ) MJS.view.viewport.orientation.degree = (degree)%180;
                                            
                        changedOrientation = true;
                    }
                    else if(MJS.view.viewport.screenWidth > MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.landscape) {
            
                        if( Math.abs(degree)%90 !== 0 || !degree ) degree = -90;
                        if( !isNaN(degree) ) MJS.view.viewport.orientation.degree = (degree)%180;
                            
                        changedOrientation = true;
                    }
                }
                break;
            
            case "landscape":
                // always landscape
                if(MJS.view.viewport.orientation) {
        
                    if(MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.portrait) {
            
                        if( Math.abs(degree)%90 !== 0 || !degree ) degree = -90;
                        if( !isNaN(degree) ) MJS.view.viewport.orientation.degree = (degree)%180;
            
                        changedOrientation = true;
                    }
                    else if(MJS.view.viewport.screenWidth > MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.landscape) {

                        if( Math.abs(degree)%180 !== 0 || !degree ) degree = 0;
                        if( !isNaN(degree) ) MJS.view.viewport.orientation.degree = (degree)%180;
                            
                        changedOrientation = true;
                    }
                }
                break;
            
            default:
                if(MJS.view.viewport.orientation) {
                    if(MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.portrait) {
                        if(ig.system) ig.system.resize(MJS.view.viewport.portraitWidth,MJS.view.viewport.portraitHeight);
            
                        changedOrientation = true;
                    }
                    else if(MJS.view.viewport.screenWidth > MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.landscape) {
                        if(ig.system) ig.system.resize(MJS.view.viewport.landscapeWidth,MJS.view.viewport.landscapeHeight);
                        
                        changedOrientation = true;
                    }
                }
                break;
        }
    
        // set correct orientation
        MJS.view.viewport.orientation.portrait = MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight;
        MJS.view.viewport.orientation.landscape = MJS.view.viewport.screenWidth > MJS.view.viewport.screenHeight;
    
        // update custom close button
        MJS.ad.updateCustomCloseButtonDiv();
        MJS.ad.updateCountDownButtonDiv();
    
        // orientation changed, re-position/update css
        if(ig.game) {
    
            // Game Settings
            MJS.view.updateSettings();
        
            if(changedOrientation) {
                ig.input.clearPressed();
            
                for(var i = 0; i < ig.game.entities.length; i++) {
                    if( typeof(ig.game.entities[i].updateOnOrientationChange) === "function" ) {
                        ig.game.entities[i].updateOnOrientationChange();
                    }
                }
            
                ig.game.pointer.update();
            }
        }
    
        MJS.view.sizeHandler();
    },
    
    reorient:function(){
        if (MJS.view.changeSize === this.getBrowserWidth()){
            
        } else{
            MJS.view.changeSize = MJS.view.getBrowserWidth();
            MJS.view.clearAllIntervals();
        }
        
        if(!MJS.view.viewport){
            return false;
        }
        

        MJS.view.updateViewport();
        
        var gamebox = document.getElementById("gamebox");
        var canvas = document.getElementById("canvas"); 
    
        var degree = 0;
        var changedOrientation = false;
    
        // orientation degree
        if( window.screen ) {
            if( window.screen.orientation ) {
                if( !isNaN(window.screen.orientation.degree) ) {
                    degree = window.screen.orientation.degree;
                }
            }
            else
            {
                if( !isNaN(window.orientation) ) {
                    degree = window.orientation;
                }
            }
        } 
        else if( !isNaN(window.orientation) ) {
            degree = window.orientation;
        }
    
        switch(MJS.view.viewport.forceOrientation) {
            case "portrait":
                // always portrait
                if(MJS.view.viewport.orientation) {
                        
                    if(MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.portrait) {
            
                        if( Math.abs(degree)%180 !== 0 || !degree ) degree = 0;
                        if( !isNaN(degree) ) MJS.view.viewport.orientation.degree = (degree)%180;
                                            
                        changedOrientation = true;
                    }
                    else if(MJS.view.viewport.screenWidth > MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.landscape) {
            
                        if( Math.abs(degree)%90 !== 0 || !degree ) degree = -90;
                        if( !isNaN(degree) ) MJS.view.viewport.orientation.degree = (degree)%180;
                            
                        changedOrientation = true;
                    }
                }
                break;
            
            case "landscape":
                // always landscape
                if(MJS.view.viewport.orientation) {
        
                    if(MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.portrait) {
            
                        if( Math.abs(degree)%90 !== 0 || !degree ) degree = -90;
                        if( !isNaN(degree) ) MJS.view.viewport.orientation.degree = (degree)%180;
            
                        changedOrientation = true;
                    }
                    else if(MJS.view.viewport.screenWidth > MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.landscape) {

                        if( Math.abs(degree)%180 !== 0 || !degree ) degree = 0;
                        if( !isNaN(degree) ) MJS.view.viewport.orientation.degree = (degree)%180;
                            
                        changedOrientation = true;
                    }
                }
                break;
            
            default:
                if(MJS.view.viewport.orientation) {
                    if(MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.portrait) {
                        if(ig.system) ig.system.resize(MJS.view.viewport.portraitWidth,MJS.view.viewport.portraitHeight);
            
                        changedOrientation = true;
                    }
                    else if(MJS.view.viewport.screenWidth > MJS.view.viewport.screenHeight && !MJS.view.viewport.orientation.landscape) {
                        if(ig.system) ig.system.resize(MJS.view.viewport.landscapeWidth,MJS.view.viewport.landscapeHeight);
                        
                        changedOrientation = true;
                    }
                }
                break;
        }
    
        // set correct orientation
        MJS.view.viewport.orientation.portrait = MJS.view.viewport.screenWidth <= MJS.view.viewport.screenHeight;
        MJS.view.viewport.orientation.landscape = MJS.view.viewport.screenWidth > MJS.view.viewport.screenHeight;
    
        // update custom close button
        MJS.ad.updateCustomCloseButtonDiv();
        MJS.ad.updateCountDownButtonDiv();
    
        // orientation changed, re-position/update css
        if(ig.game) {
    
            // Game Settings
            MJS.view.updateSettings();
        
            if(changedOrientation) {
                ig.input.clearPressed();
            
                for(var i = 0; i < ig.game.entities.length; i++) {
                    if( typeof(ig.game.entities[i].updateOnOrientationChange) === "function" ) {
                        ig.game.entities[i].updateOnOrientationChange();
                    }
                }
            
                ig.game.pointer.update();
            }
        }
    
        this.sizeHandler();
        
    },
    
    mergeSettings: function( original, extended ) {
        for( var key in extended ) {
            var ext = extended[key];
            if(
                typeof(ext) != 'object' ||
                ext instanceof HTMLElement ||
                ext === null
            ) {
                original[key] = ext;
            }
            else {
                if( !original[key] || typeof(original[key]) != 'object' ) {
                    original[key] = (ext instanceof Array) ? [] : {};
                }
                MJS.view.mergeSettings( original[key], ext );
            }
        }
        return original;
    },
    
    clearAllIntervals:function()
    {
        window.clearInterval(this.orientationInterval);
        this.orientationInterval=null;
        window.clearTimeout(this.orientationTimeout);
        this.orientationTimeout=null;
    },
    orientationInterval:null,
    orientationTimeout:null,
    orientationDelayHandler:function(){
        if(this.orientationInterval == null)
        {
            this.orientationInterval = window.setInterval(MJS.view.orientationHandler,100);
        }
        if(this.orientationTimeout == null)
        {
            this.orientationTimeout = window.setTimeout(function(){MJS.view.clearAllIntervals();},1200);
        }
    },
    
    sizeHandler: function()
    {
        if(!MJS.view.viewport) {
            return false;
        }
        
        var gamebox = document.getElementById("gamebox");
        var canvas = document.getElementById("canvas");
        var translation = "0px, 0px";
        
        
        var isIphoneX = screen.width / screen.height > 0.461 && screen.width / screen.height < 0.462;
        var width = MJS.view.getBrowserWidth()
        var height = MJS.view.getBrowserHeight()
        var iPhone = /iPhone/i.test(navigator.userAgent);
        var iPad = /iPad/i.test(navigator.userAgent);
        var iOS = iPhone || iPad;
        
        if(iOS){
            if(isIphoneX){
                var blackBarSize = 175; 
                // Is IPhoneX
                if(width <= height){
                    //Portrait
                    MJS.view.viewport.screenWidth=MJS.view.getBrowserWidth();
                    MJS.view.viewport.screenHeight=screen.height - blackBarSize;
                }else{
                    //landscape
                    MJS.view.viewport.screenWidth=MJS.view.getBrowserWidth() - blackBarSize;
                    MJS.view.viewport.screenHeight=MJS.view.getBrowserHeight();
                }
                
            
            }else{
                // Is not IPhoneX
                // console.log(width +'|'+ height)
                if(width <= height){
                    //Portrait
                    MJS.view.viewport.screenWidth=MJS.view.getBrowserWidth();
                    MJS.view.viewport.screenHeight=MJS.view.getBrowserHeight();
                }else{
                    //landscape
                    MJS.view.viewport.screenWidth=MJS.view.getBrowserWidth();
                    MJS.view.viewport.screenHeight=MJS.view.getBrowserHeight();
                }
            
            }
        }else{
            MJS.view.viewport.screenWidth=window.innerWidth;
            MJS.view.viewport.screenHeight =window.innerHeight;
        }
        
        
        
        switch(MJS.view.viewport.forceOrientation) {
            case "portrait":
                // always portrait
                if(ig.ua.mobile /*|| window.mraid*/) {
                    if(MJS.view.viewport.orientation.portrait) {
                        MJS.view.viewport.widthRatio = MJS.view.viewport.screenWidth / MJS.view.viewport.mobileWidth;
                        MJS.view.viewport.heightRatio = MJS.view.viewport.screenHeight / MJS.view.viewport.mobileHeight;    

                    }
                    else {
                        MJS.view.viewport.widthRatio = MJS.view.viewport.screenHeight / MJS.view.viewport.mobileWidth;
                        MJS.view.viewport.heightRatio = MJS.view.viewport.screenWidth / MJS.view.viewport.mobileHeight;         
                    }
                
                    gamebox.style.width = (MJS.view.viewport.mobileWidth*MJS.view.viewport.widthRatio)+"px";
                    gamebox.style.height = (MJS.view.viewport.mobileHeight*MJS.view.viewport.heightRatio)+"px";
                        
                    translation = "0px, 0px";
                    if( Math.abs(MJS.view.viewport.orientation.degree) === 90 ) {
                        translation = (MJS.view.viewport.screenWidth-parseFloat(gamebox.style.width))*0.5+"px, " +
                                        (MJS.view.viewport.screenHeight-parseFloat(gamebox.style.height))*0.5+"px";
                    }
                }
                else {
                    if(MJS.view.viewport.orientation.portrait) {
                        // always portrait
                        MJS.view.viewport.widthRatio = Math.min( MJS.view.viewport.screenHeight / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenWidth / MJS.view.viewport.desktopWidth ) ;
                        MJS.view.viewport.heightRatio = Math.min( MJS.view.viewport.screenHeight / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenWidth / MJS.view.viewport.desktopWidth ) ;
                    }
                    else {
                        // always portrait
                        MJS.view.viewport.widthRatio = Math.min( MJS.view.viewport.screenWidth / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenHeight / MJS.view.viewport.desktopWidth ) ;
                        MJS.view.viewport.heightRatio = Math.min( MJS.view.viewport.screenWidth / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenHeight / MJS.view.viewport.desktopWidth ) ;
                    }
                
                    gamebox.style.width = (MJS.view.viewport.desktopWidth*MJS.view.viewport.widthRatio)+"px";
                    gamebox.style.height = (MJS.view.viewport.desktopHeight*MJS.view.viewport.heightRatio)+"px";
        
                    translation = (MJS.view.viewport.screenWidth-parseFloat(gamebox.style.width))*0.5+ "px, 0px";
                    if( Math.abs(MJS.view.viewport.orientation.degree) === 90 ) {
                        translation = (MJS.view.viewport.screenWidth-parseFloat(gamebox.style.width))*0.5+"px, " +
                                        (MJS.view.viewport.screenHeight-parseFloat(gamebox.style.height))*0.5+"px";
                    }
                }
                break;
            
            case "landscape":
                // always landscape
            
                if(ig.ua.mobile /*|| window.mraid*/) {
                    if(MJS.view.viewport.orientation.portrait) {
                        MJS.view.viewport.widthRatio = MJS.view.viewport.screenHeight / MJS.view.viewport.mobileWidth;
                        MJS.view.viewport.heightRatio = MJS.view.viewport.screenWidth / MJS.view.viewport.mobileHeight; 
                    }
                    else {
                        MJS.view.viewport.widthRatio = MJS.view.viewport.screenWidth / MJS.view.viewport.mobileWidth;
                        MJS.view.viewport.heightRatio = MJS.view.viewport.screenHeight / MJS.view.viewport.mobileHeight;            
                    }
                
                    gamebox.style.width = (MJS.view.viewport.mobileWidth*MJS.view.viewport.widthRatio)+"px";
                    gamebox.style.height = (MJS.view.viewport.mobileHeight*MJS.view.viewport.heightRatio)+"px";

                    translation = "0px, 0px";
                    if( Math.abs(MJS.view.viewport.orientation.degree) === 90 ) {
                        translation = (MJS.view.viewport.screenWidth-parseFloat(gamebox.style.width))*0.5+"px, " +
                                        (MJS.view.viewport.screenHeight-parseFloat(gamebox.style.height))*0.5+"px";

                    }

                }
                else {
                    if(MJS.view.viewport.orientation.portrait) {
                        // always portrait
                        MJS.view.viewport.widthRatio = Math.min( MJS.view.viewport.screenWidth / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenHeight / MJS.view.viewport.desktopWidth ) ;
                        MJS.view.viewport.heightRatio = Math.min( MJS.view.viewport.screenWidth / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenHeight / MJS.view.viewport.desktopWidth ) ;
                    }
                    else {
                        // always portrait
                        MJS.view.viewport.widthRatio = Math.min( MJS.view.viewport.screenHeight / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenWidth / MJS.view.viewport.desktopWidth ) ;
                        MJS.view.viewport.heightRatio = Math.min( MJS.view.viewport.screenHeight / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenWidth / MJS.view.viewport.desktopWidth ) ;
                    }

                    gamebox.style.width = (MJS.view.viewport.desktopWidth*MJS.view.viewport.widthRatio)+"px";
                    gamebox.style.height = (MJS.view.viewport.desktopHeight*MJS.view.viewport.heightRatio)+"px";

                    translation = (MJS.view.viewport.screenWidth-parseFloat(gamebox.style.width))*0.5+ "px, 0px";
                    if( Math.abs(MJS.view.viewport.orientation.degree) === 90 ) {
                        translation = (MJS.view.viewport.screenWidth-parseFloat(gamebox.style.width))*0.5+"px, " +
                                        (MJS.view.viewport.screenHeight-parseFloat(gamebox.style.height))*0.5+"px";
                    }
                }
    
                break;
            
            default:
                // treat window.mraid as mobile
                if(ig.ua.mobile /*|| window.mraid*/) {
                    MJS.view.viewport.widthRatio = MJS.view.viewport.screenWidth / MJS.view.viewport.mobileWidth ;
                    MJS.view.viewport.heightRatio = MJS.view.viewport.screenHeight / MJS.view.viewport.mobileHeight ;
                    
                    gamebox.style.width = (MJS.view.viewport.mobileWidth*MJS.view.viewport.widthRatio)+"px";
                    gamebox.style.height = (MJS.view.viewport.mobileHeight*MJS.view.viewport.heightRatio)+"px";
            
                    translation = "0px, 0px";
        
                }
                else {
                    MJS.view.viewport.widthRatio = Math.min( MJS.view.viewport.screenHeight / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenWidth / MJS.view.viewport.desktopWidth ) ;
                    MJS.view.viewport.heightRatio = Math.min( MJS.view.viewport.screenHeight / MJS.view.viewport.desktopHeight, MJS.view.viewport.screenWidth / MJS.view.viewport.desktopWidth ) ;

                    gamebox.style.width = (MJS.view.viewport.desktopWidth*MJS.view.viewport.widthRatio)+"px";
                    gamebox.style.height = (MJS.view.viewport.desktopHeight*MJS.view.viewport.heightRatio)+"px";

                    translation = (MJS.view.viewport.screenWidth-parseFloat(gamebox.style.width))*0.5+ "px, 0px";
                }
                break;
        }
        
        if(isIphoneX){
            if(iOS){
                if(width <= height){
                    //portrait
                    translation = "0%, 0%";
                }else{
                    //landscape
                    translation = "0%, 0%";
                }
            }
        }
        
        
        // transform: translation & rotation 
        gamebox.style["-o-transform"] = "translate(" + translation + ") rotate("+MJS.view.viewport.orientation.degree+"deg)";
        gamebox.style["-moz-transform"] = "translate(" + translation + ") rotate("+MJS.view.viewport.orientation.degree+"deg)";
        gamebox.style["-ms-transform"] = "translate(" + translation + ") rotate("+MJS.view.viewport.orientation.degree+"deg)";
        gamebox.style["-webkit-transform"] = "translate(" + translation + ") rotate("+MJS.view.viewport.orientation.degree+"deg)";
        gamebox.style["transform"] = "translate(" + translation + ") rotate("+MJS.view.viewport.orientation.degree+"deg)";
        
        
        
        // on size changed
        if(ig) {
            if(ig.game) {
                for(var i = 0; i < ig.game.entities.length; i++) {
                    if( typeof(ig.game.entities[i].updateOnSizeChange) === "function" ) {
                        ig.game.entities[i].updateOnSizeChange();
                    }
                }
            }
        }
        
        // scrolling page to the top
        //window.scrollTo(0, 1);
        window.scrollTo(0, 0);
    },
    
    updateCanvasDirection: function() {
        var canvas = document.getElementById("canvas");
        
        var language = MJS.loc.getLanguage();
        switch(language) {
            case  MJS.loc.language.AR:
                // right to left 
                canvas.setAttribute("dir", "rtl");
            break;
        
            default : 
                // left to right 
                canvas.setAttribute("dir", "ltr");
            break;
        }
    },
    
    getBrowserWidth: function() {
        if(this.isiOS()){
            if(screen && screen.width){
                //Note that on chrome iphone emulator window.orientation is undefined but actual device is defined
                // report the original width size in case window.orientation is undefined
                if(window.orientation===undefined || window.orientation === null) {                     
                    if(window.innerWidth<screen.width){
                        return window.innerWidth;
                    }
                    return screen.width;
                }
                if(window.orientation%180 === 0){
                    if(window.innerWidth<screen.width){
                        return window.innerWidth;
                    }
                    return screen.width;
                }
                else
                {
                    if(window.innerWidth<screen.width){
                        return window.innerWidth;
                    }
                    return screen.width;
                }
            }
        }
        return window.innerWidth ? window.innerWidth : document.documentElement && 0 != document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body ?
        document.body.clientWidth : 0
    },

    getBrowserHeight: function() {
        if(this.isiOS()){
            if(screen && screen.height){
                if(window.orientation===undefined || window.orientation === null) { 
                    if(window.innerHeight<screen.height){
                        return window.innerHeight;
                    }                    
                    return screen.height;
                }
                if(window.orientation%180 === 0){
                    if(window.innerHeight<screen.height){
                        return window.innerHeight;
                    }                    
                    return screen.height;
                }
                else{
                    if(window.innerHeight<screen.width){
                        return window.innerHeight;
                    }                    
                    return screen.width;
                }
            }
        }
        return window.innerHeight ? window.innerHeight : document.documentElement && 0 != document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body ? document.body.clientHeight : 0
    },
    
    isiOS:function(){
        
        var iPhone = /iPhone/i.test(navigator.userAgent);
        var iPad = /iPad/i.test(navigator.userAgent);
        var iOS = iPhone || iPad;
        
        return iOS;
    },
    
    
    updateSettings: function() {
        //@TODO 
        return;
        MJS.settings.data = {};
    
        if(MJS.view.viewport && MJS.view.viewport.orientation.portrait) {
            // Portrait
            MJS.settings.data = this.mergeSettings(
                this.mergeSettings(MJS.settings.data, gameSettingsGlobal), 
                gameSettingsPortrait
            );
        }
        else if(MJS.view.viewport && MJS.view.viewport.orientation.landscape) {
            // Landscape
            MJS.settings.data = this.mergeSettings(
                this.mergeSettings(MJS.settings.data, gameSettingsGlobal), 
                gameSettingsLandscape
            );
        }
        else {
            // Default
            MJS.settings.data = this.mergeSettings(
                this.mergeSettings(MJS.settings.data, gameSettingsGlobal), 
                gameSettingsPortrait    // Portrait
            );
        }
    },
    
    
    clone:function(){
        
        var output = new ViewHelper();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
ViewHelper.prototype.constructor = ViewHelper;
