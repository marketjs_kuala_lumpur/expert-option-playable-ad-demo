window.MJS={
    sys:new SystemHelper(),
    ad:new AdHelper(),
    view:new ViewHelper(),
    math:new MathHelper(),
    game:new GameHelper(),
    event:new EventTracker(),
    settings:new SettingsHelper(),
    loc:new LocationHelper(),
    drawHelp:new DrawHelper(),
    log:new Logger(),
    TYPEOFS:{
        STR:"string",
        UNDEF:"UNDEFINED",
        NULL:null,
        NUM:"number",
        BOOL:"boolean"
    },
    CONST:{
        TRUE:"true",
        FALSE:"false",
        ON:"on",
        OFF:"off",
        ISMISSINGA:" is missing",
        ISMISSINGB:" is missing or incorrect type",
        URL:"https://www.marketjs.com",
        
        MOPUB:"mopub",
        _SELF:"_blank",
        
    }
};
