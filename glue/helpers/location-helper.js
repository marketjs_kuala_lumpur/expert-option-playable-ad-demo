
var LocationHelper = function(){
    
    
    this.text={
        prompt:{
            en:"Designed By MarketJS. \nPlease visit us if you need a HTML Web Game Developer at https://www.marketjs.com/",
            
        }
    },
    
    this.COUNTRYCODE={
        US: "us",
        KO: "ko",
        KR: "kr",
    };
    this.country =this.COUNTRYCODE.US;
    
    this.REGIONCODES={
        US:{
            
        }
    };
    
    this.LANGCODE={
        AR:"ar",
        EN:"en",
        DE:"de", 
        ES:"es",
        FR:"fr",
        FRCA :"fr-ca",
        PT:"pt",
        RU:"ru",
        IT:"it",
        PL:"pl",
        TR:"tr",
        JP:"jp",
        KO:"ko",
        DA:"da",
        FI:"fi",
        NB:"nb",
        NL:"nl",
        SV:"sv",
        ZHTW :"zh-tw",
        ZHCN :"zh-cn",
        EL:"el",
        ID:"id",
        MS:"ms",
        TH:"th",
        VI:"vi",
        CS:"cs",
        SK:"sk",
        UK:"uk",
        BG:"bg",
        HU:"hu",
        AR:"ar",
        HR:"hr",
        CA:"ca"
    };
    this.language = navigator.language;
    this.lang=navigator.language.split("-")[0];
};

LocationHelper.prototype={
    
    
    getRegion:function(){
        if(adDetails.region){
            this.region=adDetails.region;
            return this.region;
        }else{
            throw MJS.sys.EVENTS.ERROR;
        }
    },
    
    getCountry:function(){
        if (adDetails.country){
            this.country=adDetails.country.toLowerCase();
            return this.country;
        } else{
            throw MJS.sys.EVENTS.ERROR;
        }
    },
    
    getLanguage:function(){
        
        //Forced language setting
        if (adDetails.language){
            this.language = adDetails.language;
            return this.language;
        }
        else if(navigator.language){
            this.language = navigator.language;
            return this.language;
        }else{
            return "en-US";
        }
    },
    
    getMainLanguage:function(){
        return this.language.split(DELIMITERS.DASH)[0];
    },
    getCountryCode:function(){
        return this.language.split(DELIMITERS.DASH)[1];
    },
    
    clone:function(){
        
        var output = new LocationHelper();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    },
    
    getText:function(key){
        return this.text[key][this.lang]
    }
};
LocationHelper.prototype.constructor = LocationHelper;
