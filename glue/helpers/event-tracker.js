
var EventTracker = function(){
    
};

EventTracker.prototype={
   
    clone:function(){
        
        var output = new EventTracker();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
EventTracker.prototype.constructor = EventTracker;