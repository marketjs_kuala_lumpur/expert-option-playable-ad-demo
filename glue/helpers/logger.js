
var Logger = function(){
};

Logger.prototype={
    
    console:function(message,location){
        console.trace(message);
    },
    
    clone:function(){
        
        var output = new Logger();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
Logger.prototype.constructor = Logger;