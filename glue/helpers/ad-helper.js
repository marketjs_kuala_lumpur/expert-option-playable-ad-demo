
var AdHelper = function(){
    
};

AdHelper.prototype={

    openURL: function(url) {
        if(MJS.settings.getSSP()===MJS.CONST.MOPUB){
            this.openUrlMopub(url);
        }
        else{
            this.openURLNative(url);
        }
        //this.logger.log("PlayableSDK", e), this.options.mopubSlide && "mopub" === this.cfg.getSSP.call(this.cfg) ? this.openURLMopubWebView(e) : this.openURLNative(e)
    },
    openURLNative: function(url) {
        if(this.mraid){
            this.mraid.open(url);
        }
        else{
            window.open(url,MJS.CONST._SELF)
        }
        
        //this.mraid && "EXPANDABLE" !== this.creativeType ? (this.logger.log("PlayableSDK", "Calling mraid.open"), this.mraid.open(e)) : (this.logger.log("PlayableSDK", "Calling window.open"), this.window.open(e, "_self"))
    },
    openURLMopubWebView: function(url) {
        //this.logger.log("PlayableSDK", "Calling openURLMopubWebView");
        var url = "mopubnativebrowser://navigate?url=" + encodeURIComponent(MJS.settings.getDestURL());
        this.openURLNative(url);
        
        //var t = "mopubnativebrowser://navigate?url=" + encodeURIComponent(this.adInfo.destUrl),
        //    n = new a;
        //n.open("GET", this.clickURL), n.timeout = 6e3;
        //var i = this;
        /*
        n.onreadystatechange = function() {
            4 === n.readyState && (i.options.mopubSlideFallback && setTimeout(function() {
                i.openURLNative.call(i, e)
            }, 100), i.logger.log("PlayableSDK", "Opening deeplink: " + t), i.mraid ? i.mraid.open(t) : i.window.open(t))
        }, n.send()
        */
    },
    openClickUrl: function(fromElement) {

        console.log("open click url")
        this.openURL(MJS.settings.getDestURL());
        /*
        var t = this.options.clickSignal.enabled && this.options.clickSignal.destURL ? this.options.clickSignal.destURL : this.clickURL;
        if (this.logger.log("PlayableSDK", e), this.sessionStore.set("last_action", "click"), this.trackEvent.clickSignal(), this.options.telemetryDisabled) this.openURL(t);
        else {
            var n = this;
            this.trackEvent.click(e, function() {
                n.openURL(t)
            })
        }
        */
        
    },
    
    close:function(){
        if(window.mraid) {
            window.mraid.close()
        }
        else {
            window.close();
        }
    },
    
    setupCloseButton: function() {
        if(MJS.settings.preloaderStartCountdown==="on") { 
            this.initCloseButton();
        }
    },
    
    initCloseButton: function() {
        if(window.closeButtonInitialized) return;
        window.closeButtonInitialized = true;
    
        // create if not existed
        if(!window._AD_CLOSE_DIV) {
            this.createCustomCloseButtonDiv();
            this.hideCustomCloseButton();
        }
        if(!window._AD_CLOSE_COUNT_DOWN_DIV) {
            this.createCountDownButtonDiv();
        }
        
        var timeout = MJS.settings.hideCloseButtonTime;
        if(timeout > 0) {
            var interval = (MJS.settings.closeButtonTimer*1000)/timeout;
            //    console.log(interval);
            timeout = timeout.ceil();
            this.showCustomCloseButtonCountDown(timeout);

            window._CLOSE_COUNTDOWN = window.setInterval(function(){
                timeout--;

                if(timeout > 0) {
                    this.hideCustomCloseButton();
                    this.showCustomCloseButtonCountDown(timeout);
                }
                else {
                    this.hideCustomCloseButtonCountDown(timeout);
                    this.showCustomCloseButton();
                    
                }
            },interval);
        }
        else {
            this.showCustomCloseButton();
        }
    },

    createCountDownButtonDiv: function() {
        var closebuttonbox = document.getElementById("closebuttonbox");
    
        window._AD_CLOSE_COUNT_DOWN_DIV = ig.$new("div");
        window._AD_CLOSE_COUNT_DOWN_DIV.setAttribute("id","_COUNT_DOWN");
    
        var cssStyleString = "" +
            "cursor: pointer;" +
            "background-size: 30px;" +
            "background-repeat: no-repeat;" +
            "background-position: center center;" +
            "line-height: 36px;" +
            "height: 28px;" +
            "width: 28px;" +
            "position: absolute;" +
            "text-indent: -9999px;" +
            "display: block;" +
            "visibility: hidden;" +
            "border: 1px solid white;" +
            "border-radius: 15px;" +
            "line-height: 29px;" +
            "font-size: 19px;" +
            "text-indent: 0;" +
            "color: white;" +
            "z-index: 16777216;" +
        "-webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;-webkit-tap-highlight-color: rgba(0,0,0,0);-webkit-tap-highlight-color: transparent;"+
        "";
  
        // count down close button location
        switch(MJS.settings.Property3) {
            default:
            case 1:
                cssStyleString += "top: 8px;";
                cssStyleString += "right: 8px;";
            break;
            
            case 2: 
                cssStyleString += "top: 8px;";
                cssStyleString += "left: 8px;";
                break;
            
            case 3:
                cssStyleString += "bottom: 8px;";
                cssStyleString += "right: 8px;";
            break;
            
            case 4: 
                cssStyleString += "bottom: 8px;";
                cssStyleString += "left: 8px;";
            break;
        }
  
        window._AD_CLOSE_COUNT_DOWN_DIV.setAttribute("style", cssStyleString);
        this.updateCountDownButtonDiv();
        
        closebuttonbox.appendChild(window._AD_CLOSE_COUNT_DOWN_DIV);
    },
    
    createCustomCloseButtonDiv: function() {    
        var closebuttonbox = document.getElementById("closebuttonbox");
        
        window._AD_CLOSE_DIV_HITBOX= ig.$new("div");
        
        window._AD_CLOSE_DIV = ig.$new("div");
        
        window._AD_CLOSE_DIV_HITBOX.setAttribute("id","_CLOSE_AD_HITBOX");
        
        window._AD_CLOSE_DIV.setAttribute("id","_CLOSE_AD");
        var cssStyleString = "" +
            "background-size: 30px;" +
            "background-repeat: no-repeat;" +
            "background-position: center center;" +
            "background-image: url('" + spriteSheets.customCloseButton.meta.image + "');" +
            "line-height: 36px;" +
            "height: 30px;" +
            "width: 30px;" +
            "border-radius: 15px;" +
            "position: relative;" +
            "text-indent: -9999px;" +
            "z-index: 16777887;" +
            "webkit-transform: translate3d(0,0,0);" +
        "-webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;-webkit-tap-highlight-color: rgba(0,0,0,0);-webkit-tap-highlight-color: transparent;"+
            
        "";
  
        // close button location
        
        
        var hitboxString = ""+
            "padding:8px;"+
            "cursor: pointer;" +
            "display: block;" +
            "visibility: hidden;" +
            "position:absolute;" +
            "z-index: 16777888;" +
            "" +
        "-webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;-webkit-tap-highlight-color: rgba(0,0,0,0);-webkit-tap-highlight-color: transparent;"+
            "";
            
            switch(MJS.settings.data.Property3) {
                default:
                case 1:
                    hitboxString += "top: 0px;";
                    hitboxString += "right: 0px;";
                break;
            
                case 2: 
                    hitboxString += "top: 0px;";
                    hitboxString += "left: 0px;";
                    break;
            
                case 3:
                    hitboxString += "bottom: 0px;";
                    hitboxString += "right: 0px;";
                break;
            
                case 4: 
                    hitboxString += "bottom: 0px;";
                    hitboxString += "left: 0px;";
                break;          
            }
            
        window._AD_CLOSE_DIV_HITBOX.setAttribute("style",hitboxString);
        
        window._AD_CLOSE_DIV.setAttribute("style", cssStyleString);
        this.updateCustomCloseButtonDiv();
        
        
        window._AD_CLOSE_DIV_HITBOX.appendChild(window._AD_CLOSE_DIV);
        closebuttonbox.appendChild(window._AD_CLOSE_DIV_HITBOX);
        //closebuttonbox.appendChild(window._AD_CLOSE_DIV);
    },

    showCustomCloseButtonCountDown: function(countDownText) {
        countDownText = Math.max( 0, Math.ceil(countDownText) );
        
        if(countDownText && igh.settings.countDownCloseButton==="on") {
            window._AD_CLOSE_COUNT_DOWN_DIV.style.visibility = "visible";
            window._AD_CLOSE_COUNT_DOWN_DIV.innerText = countDownText;
        }
        else {
            if(!window._AD_CLOSE_COUNT_DOWN_DIV)return;
            window._AD_CLOSE_COUNT_DOWN_DIV.style.visibility = "hidden";
            window._AD_CLOSE_COUNT_DOWN_DIV.innerText = "";
        }
    },
    
    showCustomCloseButton: function() {
        // show
        window._AD_CLOSE_DIV.style.visibility = "visible";
        window._AD_CLOSE_DIV_HITBOX.style.visibility = "visible";
    
        // set function
        window._AD_CLOSE_DIV_HITBOX.onclick = function() {
            if(window.mraid) {
                window.mraid.close()
            }
            else {
                window.close();
            }
        };  // "window.location = 'mraid://close'"
    },
    
    hideCustomCloseButtonCountDown: function() {
        // clear interval
        if( window._CLOSE_COUNTDOWN ) {
            window.clearInterval( window._CLOSE_COUNTDOWN );
        }
        
        this.showCustomCloseButtonCountDown("");
        if(!window._AD_CLOSE_COUNT_DOWN_DIV)return;
        window._AD_CLOSE_COUNT_DOWN_DIV.style.visibility = "hidden";
    },
    
    hideCustomCloseButton: function() {
        if(!window._AD_CLOSE_DIV)return;
        window._AD_CLOSE_DIV.style.visibility = "hidden";
        window._AD_CLOSE_DIV_HITBOX.style.visibility = "hidden";
    },
    
    disableCloseButton: function() {
        this.hideCustomCloseButtonCountDown();
        this.hideCustomCloseButton();        
    },
    
    updateCountDownButtonDiv: function() {
        if(!window._AD_CLOSE_COUNT_DOWN_DIV) { return; }
        var backgroundColor = "";

        if(MJS.view.viewport.orientation.portrait === true) {
            if( MJS.settings.data.closeButtonBackgroundPortrait ) {
                backgroundColor = MJS.settings.data.closeButtonBackgroundPortrait;
            }
        } 
        else {
            if( MJS.settings.data.closeButtonBackgroundLandscape ) {
                backgroundColor = MJS.settings.data.closeButtonBackgroundLandscape;
            }
        }
        window._AD_CLOSE_COUNT_DOWN_DIV.style.background = backgroundColor;
    },
    
    updateCustomCloseButtonDiv: function() {
        if(!window._AD_CLOSE_DIV) { return; }
        var backgroundColor = "";

        if(MJS.view.viewport.orientation.portrait === true) {
            if( MJS.settings.data.closeButtonBackgroundPortrait ) {
                backgroundColor = MJS.settings.data.closeButtonBackgroundPortrait;
            }
        } 
        else {
            if( MJS.settings.data.closeButtonBackgroundLandscape ) {
                backgroundColor = MJS.settings.data.closeButtonBackgroundLandscape;
            }
        }
        window._AD_CLOSE_DIV.style.backgroundColor = backgroundColor;
    },
    
    
    clone:function(){
        
        var output = new AdHelper();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
AdHelper.prototype.constructor = AdHelper;
