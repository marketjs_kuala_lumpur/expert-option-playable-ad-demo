
var MathHelper = function(){
    this.ANGLE={
        RAD:"radian",
        DEG:"degree",
        PIRAD:Math.PI,
        PIDEG:180,
        PIRADTODEG:180/Math.PI,
        PIDEGTORAD:Math.PI/180
    };
};

MathHelper.prototype={
    
    radToDegree:function(rad){
        return rad * this.ANGLE.PIRADTODEG;
    },
    
    degreeToRad:function(deg){
        return deg * this.ANGLE.PIDEGTORAD;
    },
    
    clone:function(){
        
        var output = new MathHelper();

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
MathHelper.prototype.constructor = MathHelper;
