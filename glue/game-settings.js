// Game Settings
var gameSettingsGlobal = {
	"installBanner": {
		"fontFamily": "arial",
		"fontSize": 20,
		"textFillColor":"rgba(255,255,255,1)",
		"boxFillColor": "rgba(0,0,0,1)",
		"textID": 0 ,
		"text": [
			""
			,"Tutorial"
			,""		
        ]
	},
    
    //preloader:MJS.settings.get("preloader")||"on",
    //language:MJS.settings.get("language")||"en",
    
    
    /* adInfo */
    /*
    "country": PlayableSdk.cfg.get("country") || "",
    "language": PlayableSdk.getLanguageCode() || "en",
    
    "milestone" : (PlayableSdk.cfg.get("milestone") || "").split(",").map(Number),
  
	"preloader": PlayableSdk.cfg.get("preloader") || "on",
	"tutorial": PlayableSdk.cfg.get("tutorial") || "on",
	"bannerTogglePortrait": PlayableSdk.cfg.get("bannerTogglePortrait") || "on", 
	"bannerToggleLandscape": PlayableSdk.cfg.get("bannerToggleLandscape") || "on",
    "bannerClickableOnShow": PlayableSdk.cfg.get("bannerClickableOnShow") || "off",
    
    "closeButtonBackgroundPortrait": PlayableSdk.cfg.get("closeButtonBackgroundPortrait") || "rgba(0,0,0,1)", 
    "closeButtonBackgroundLandscape": PlayableSdk.cfg.get("closeButtonBackgroundLandscape") || "rgba(0,0,0,1)", 
    
	"hideCloseButtonTime": parseFloat(PlayableSdk.cfg.get("hideCloseButtonTime") || 5),
	"countDownCloseButton": PlayableSdk.cfg.get("countDownCloseButton") || "on", 
    "closeButtonTimer": parseFloat(PlayableSdk.cfg.get("closeButtonTimer") || 10),
  
    "Property1": PlayableSdk.cfg.get("Property1") || "off", 
	"Property2": PlayableSdk.cfg.get("Property2") || "off",
	"Property3": parseInt(PlayableSdk.cfg.get("Property3") || 1),
	"Property4": PlayableSdk.cfg.get("Property4") || "off", 

	"gamePlayLogo": PlayableSdk.cfg.get("gamePlayLogo") || "on",
	"endCardLogo": PlayableSdk.cfg.get("endCardLogo") || "on",

	"preloaderStartCountdown": PlayableSdk.cfg.get("preloaderStartCountdown") || "on",
	"didInteractTimeLimit": parseFloat(PlayableSdk.cfg.get("didInteractTimeLimit") || 15), 
	"didInteractTimeLimitEnabled": PlayableSdk.cfg.get("didInteractTimeLimitEnabled") || "off",	
	
    "redirectOnGameStart": parseFloat(PlayableSdk.cfg.get("redirectOnGameStart") || 0),
    "redirectOnGameEnd": parseFloat(PlayableSdk.cfg.get("redirectOnGameEnd") || 0),
    
	"disclaimerToggle": PlayableSdk.cfg.get("disclaimerToggle") || "off",
	"disclaimerFontFamily": PlayableSdk.cfg.get("disclaimerFontFamily") || "arial",
	"disclaimerFontSize": parseInt(PlayableSdk.cfg.get("disclaimerFontSize") || 10),
	"disclaimerFontColor": PlayableSdk.cfg.get("disclaimerFontColor") || "rgba(128,128,128,1)",
	"disclaimerMessageID": parseInt(PlayableSdk.cfg.get("disclaimerMessageID") || 1),
	"disclaimerMessage": ["","Disclaimer"],

	"installBanner": {
		"fontFamily": PlayableSdk.cfg.get("installBannerFontFamily") || "arial",
		"fontSize": parseFloat(PlayableSdk.cfg.get("installBannerFontSize") || 20),
		"textFillColor": PlayableSdk.cfg.get("installBannerTextFillColor") || "rgba(255,255,255,1)",
		"boxFillColor": PlayableSdk.cfg.get("installBannerBoxFillColor") || "rgba(0,0,0,1)",
		"textID": parseInt(PlayableSdk.cfg.get("installBannerTextID") || 0 ),
		"text": [
			""
			,"Tutorial"
			,""		
        ]
	},

	"infoMessageStyle": {
		"fontFamily": PlayableSdk.cfg.get("infoFontFamily") || "arial",
		"fontSize": parseFloat(PlayableSdk.cfg.get("infoFontSize") || 20),
		"fontLineSpacing": parseFloat(PlayableSdk.cfg.get("infoLineSpacing") || 3),
		"textAlign": "center",
		"textBaseline": "middle",
		"textFillColor": PlayableSdk.cfg.get("infoTextFillColor") || "rgba(255,215,11,1)",
		"textStrokeWidth": parseFloat(PlayableSdk.cfg.get("infoTextStrokeWidth") || 1),
		"textStrokeColor": PlayableSdk.cfg.get("infoTextStrokeColor") || "rgba(51,46,52,0.25)",
		"boxFillColor": PlayableSdk.cfg.get("infoBoxFillColor") || "rgba(1,1,1,0.55)",
		"boxStrokeWidth": parseFloat(PlayableSdk.cfg.get("infoBoxStrokeWidth") || 2),
		"boxStrokeColor": PlayableSdk.cfg.get("infoBoxStrokeColor") || "rgba(255,223,52,0.1)"
	},

	"endCard": {
		"header": {
			"fontFamily": PlayableSdk.cfg.get("endCardHeaderTextFontFamily") || "bowlby",
			"fontSize": parseFloat(PlayableSdk.cfg.get("endCardHeaderTextFontSize") || 28),
			"textFillColor": PlayableSdk.cfg.get("endCardHeaderTextFillColor") || "rgba(0,0,0,1)",
			"victory": {
				"toggle": PlayableSdk.cfg.get("endCardHeaderTextVictoryToggle") || "on",
				"textID": parseInt(PlayableSdk.cfg.get("endCardHeaderTextVictoryID") || 1),
				"text": [ ""
					,"YouWon"
					,"victory"
					,"InstallNow"
				],
			},
			"defeated": {
				"toggle": PlayableSdk.cfg.get("endCardHeaderTextDefeatedToggle") || "on",
				"textID": parseInt(PlayableSdk.cfg.get("endCardHeaderTextDefeatedID") || 1),
				"text": [ ""
					,"YouLost"
					,"defeat"
					,"InstallNow"
				],
			},
			"time_limit": {
				"toggle": PlayableSdk.cfg.get("endCardHeaderTextTimeLimitToggle") || "on",
				"textID": parseInt(PlayableSdk.cfg.get("endCardHeaderTextTimeLimitID") || 1),
				"text": [ ""
					,"TimesUp"
					,"GameEnd"
					,"InstallNow"
				],
			}
		},
		"upsell": {
			"fontFamily": PlayableSdk.cfg.get("endCardUpsellTextFontFamily") || "bowlby",
			"fontSize": parseFloat(PlayableSdk.cfg.get("endCardUpsellTextFontSize") || 24),
			"fontLineSpacing": parseFloat(PlayableSdk.cfg.get("endCardUpsellTextLineSpacing") || 3),
			"textFillColor": PlayableSdk.cfg.get("endCardUpsellTextFillColor") || "rgba(0,0,0,1)",
			"victory": {
				"toggle": PlayableSdk.cfg.get("endCardUpsellTextVictoryToggle") || "on",
				"textID": parseInt(PlayableSdk.cfg.get("endCardUpsellTextVictoryID") || 1),
				"text": [ ""
					,"UpsellInstall"
					,"UpsellEmpire"
					,"UpsellTrain"
					,"UpsellLead"
					,"UpsellForge"
				]
			},
			"defeated": {
				"toggle": PlayableSdk.cfg.get("endCardUpsellTextDefeatedToggle") || "on",
				"textID": parseInt(PlayableSdk.cfg.get("endCardUpsellTextDefeatedID") || 1),
				"text": [ ""
					,"UpsellInstall"
					,"UpsellEmpire"
					,"UpsellTrain"
					,"UpsellLead"
					,"UpsellForge"
				]
			},
			"time_limit": {
				"toggle": PlayableSdk.cfg.get("endCardUpsellTextTimeLimitToggle") || "on",
				"textID": parseInt(PlayableSdk.cfg.get("endCardUpsellTextTimeLimitID") || 2),
				"text": [ ""
					,"UpsellInstall"
					,"UpsellEmpire"
					,"UpsellTrain"
					,"UpsellLead"
					,"UpsellForge"
				]
			}
		},
		"install": {
			"fontFamily": PlayableSdk.cfg.get("endCardInstallButtonFontFamily") || "bowlby",
			"fontSize": parseFloat(PlayableSdk.cfg.get("endCardInstallButtonFontSize") || 20),
			"textFillColor": PlayableSdk.cfg.get("endCardInstallButtonTextFillColor") || "rgba(255,255,255,1)",
			"textID": parseInt(PlayableSdk.cfg.get("endCardInstallButtonTextID") || 1),
			"text": [ ""
				,"Install Now*"
				,"Get App*"

			],
		}
	},

	"maxPlayTime": parseFloat(PlayableSdk.cfg.get("maxPlayTime") || 0),
	*/
};

console.log(gameSettingsGlobal)

var gameSettingsPortrait = {
	"orientation": "portrait",
	"gamePlayLogoConfig": {
  		"pos":{x:250,y:600},
  		"scale":0.7,
  	},
};

var gameSettingsLandscape = {
	"orientation": "landscape",
	"gamePlayLogoConfig":{
  		"pos":{x:550,y:120},
  		"scale":0.7,
  	},
};