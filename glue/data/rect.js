var Rect = function(point,size){
    
    this.lp=point.clone();
    this.size=size.clone();
    
    this.x=this.lp.x;
    this.y=this.lp.y;
    this.w=this.size.w;
    this.h=this.size.h;
};
Rect.prototype={
    
    
    clone:function(){
        
        var output = new Rect(this.lp,this.size);

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
Rect.prototype.constructor = Rect;
