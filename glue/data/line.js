

var Line = function(p1,p2){
    this.p1=p1.clone();
    this.p2=p2.clone();
};
Line.prototype={
    
    clone:function(){
        
        var output = new Line(this.p1,this.p2);

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
Line.prototype.constructor = Line;