
var Vector3 = function(x,y,z){
    this.vallType="number";
    this.x=x;
    this.y=y;
    this.z=z;
};
Vector3.prototype={
	neg: function() {
		this.x = -this.x;
		this.y = -this.y;
		this.z = -this.z;
        return this;
	},
	add: function(v) {
		if (v instanceof Vector3) {
			this.x += v.x;
			this.y += v.y;
            this.z += v.z;
		} else if (typeof(v)=== TYPEOFS.NUMBER){
			this.x += v;
			this.y += v;
            this.z += v;
		} else {
		    throw "Error:Not a valid type" +v;
		}
		return this;
	},
	sub: function(v) {
		if (v instanceof Vector3) {
			this.x -= v.x;
			this.y -= v.y;
		} else {
			this.x -= v;
			this.y -= v;
		}
		return this;
	},
	mul: function(v) {
		if (v instanceof Vector3) {
			this.x *= v.x;
			this.y *= v.y;
		} else {
			this.x *= v;
			this.y *= v;
		}
		return this;
	},
	div: function(v) {
		if (v instanceof Vector3) {
			if(v.x != 0) this.x /= v.x;
			if(v.y != 0) this.y /= v.y;
		} else {
			if(v != 0) {
				this.x /= v;
				this.y /= v;
			}
		}
		return this;
	},
	equals: function(v) {
		return this.x == v.x && this.y == v.y;
	},
	dot: function(v) {
		return this.x * v.x + this.y * v.y;
	},
	cross: function(v) {
		return this.x * v.y - this.y * v.x;
	},
	length: function() {
		return Math.sqrt(this.dot(this));
	},
	norm: function() {
		return this.divide(this.length());
	},
	min: function() {
		return Math.min(this.x, this.y);
	},
	max: function() {
		return Math.max(this.x, this.y);
	},
	toAngles: function() {
		return -Math.atan2(-this.y, this.x);
	},
	angleTo: function(a) {
		return Math.acos(this.dot(a) / (this.length() * a.length()));
	},
	toArray: function(n) {
		return [this.x, this.y,this.z].slice(0, n || 3);
	},
	set: function(x, y) {
		this.x = x; this.y = y;
		return this;
	},
    unit: function(){
        var mag = this.length();
        //Magnitude should not be negative ever since its the product of the squares of x and y, square root.
        //If Magnitude is 0 then we have a divide by 0 error.
        if(mag > 0){
            return new Vector3(this.x/mag, this.y/mag,this.z/mag);
        }
        else
        {
            throw ("Divide by 0 error in unitVector function of vector:"+this);
        }
    },
    
    rotate:function(angle){
        var vector = this.clone();
        this.x =vector.x * Math.cos(angle) - vector.y * Math.sin(angle);
        this.y =vector.x * Math.sin(angle) + vector.y * Math.cos(angle);
        return this;
    },
    
    clone:function(){
        
        var output = new Vector3(this.x, this.y,this.z);
        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }
        return output;
    }
};
Vector3.prototype.constructor = Vector3;