
var Circle = function(c,r){
    this.x=c.x;
    this.y=c.y;
    this.r=r;
    this.d=r*2;
};
Circle.prototype={
    
    
    clone:function(){
        
        var output = new Circle(this.x, this.y,this.r);

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
Circle.prototype.constructor = Circle;
