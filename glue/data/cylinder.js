
var Cylinder = function(c,r,h){
    this.x=c.x;
    this.y=c.y;
    this.r=r;
    this.d=r*2;
    this.h=h;
};
Cylinder.prototype={
    
    
    clone:function(){
        
        var output = new Cylinder(this.x, this.y,this.r);

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
Cylinder.prototype.constructor = Cylinder;
