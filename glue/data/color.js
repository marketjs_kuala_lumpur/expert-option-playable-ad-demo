
var Color = function(r,g,b,a){
    this.r=r;
    this.g=g;
    this.b=b;
    this.a=a;
};
Color.prototype={
    
    toHex:function(){
        
    },
    toRGBA:function(){
        return "rgba("+this.r+","+this.g+","+this.b+","+this.a+")";
    },
    
    clone:function(){
        
        var output = new Color(this.r, this.g,this.b,this.a);

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
Color.prototype.constructor = Color;
