var Angle = function(angle,angleType){
    this.angleType = angleType;
    this.value=angle;
};
Angle.prototype={
    
    toDegree:function(){
        if (this.angleType === mjs.math.ANGLE.RAD){
            //Angle in radians
            return this.value*180/Math.PI;
        }else if(this.angleType === mjs.math.ANGLE.DEG){
            //Angle in deg
            return this.value;
        }else{
            //default to radians
            return this.value*180/Math.PI;
        }
    },
    toRadian:function(){
        if (this.angleType === mjs.math.ANGLE.RAD){
            //Angle in radians
            return this.value;
        }else if(this.angleType === mjs.math.ANGLE.DEG){
            //Angle in deg
            return this.value*Math.PI/180;
        }else{
            //default is radians
            return this.value;
        }
    },
    
    
    clone:function(){
        
        var output = new Angle(this.value);

        for (var prop in this)
        {
            if (this.hasOwnProperty(prop))
            {
                output[prop] = this[prop];
            }
        }

        return output;
    }
};
Angle.prototype.constructor = Angle;
