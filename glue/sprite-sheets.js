// register spirtesheets here (to generate sprites' JSON, ShoeBox -> Sprite Sheet)
var spriteSheets = {
	"customCloseButton": {
    	"frames": {
    		"button-close.png": {
    			"frame": {"x":0, "y":0, "w":30, "h":30, "ox":0, "oy":0},
    			"size": {"w":30,"h":30},
    			"scale": 1
    		}

    	},
    	"meta": {
    		"image": "media/graphics/compressed/custom-close-button.png",
    		"size": {"w": 32, "h": 32}
    	}
	},
	"catSplash": {
    	"frames": {
    		"cat-splash.png": {
    			"frame": {"x":0, "y":0, "w":218, "h":325, "ox":0, "oy":0},
    			"size": {"w":218,"h":325},
    			"scale": 1
    		}

    	},
    	"meta": {
    		"image": "media/graphics/compressed/logo/kitty.png",
    		"size": {"w": 872, "h": 650}
    	}
	},

    "gamebg": {
        "frames": {
            "gamebg.png": {
                "frame": {"x":0, "y":0, "w":500, "h":889, "ox":0, "oy":0},
                "size": {"w":500,"h":889},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/gamebg.jpg",
            "size": {"w": 502, "h": 891}
        }
    },

    "toplogo": {
        "frames": {
            "toplogo.png": {
                "frame": {"x":0, "y":0, "w":222, "h":53, "ox":0, "oy":0},
                "size": {"w":222,"h":53},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/toplogo.png",
            "size": {"w": 224, "h": 55}
        }
    },

    "tittle": {
        "frames": {
            "tittle.png": {
                "frame": {"x":0, "y":0, "w":375, "h":142, "ox":0, "oy":0},
                "size": {"w":375,"h":142},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/tittle.png",
            "size": {"w": 377, "h": 144}
        }
    },

    "installhead": {
        "frames": {
            "installhead.png": {
                "frame": {"x":0, "y":0, "w":182, "h":56, "ox":0, "oy":0},
                "size": {"w":182,"h":56},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/installhead.png",
            "size": {"w": 184, "h": 56}
        }
    },

    "installbottom": {
        "frames": {
            "installbottom.png": {
                "frame": {"x":0, "y":0, "w":540, "h":69, "ox":0, "oy":0},
                "size": {"w":540,"h":69},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/installbottom.png",
            "size": {"w": 542, "h": 71}
        }
    },

    "grid": {
        "frames": {
            "grid.png": {
                "frame": {"x":0, "y":0, "w":540, "h":530, "ox":0, "oy":0},
                "size": {"w":540,"h":530},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/grid.png",
            "size": {"w": 542, "h": 532}
        }
    },

    "panel": {
        "frames": {
            "panel.png": {
                "frame": {"x":0, "y":0, "w":540, "h":320, "ox":0, "oy":0},
                "size": {"w":540,"h":320},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/panel.png",
            "size": {"w": 542, "h": 322}
        }
    },

    "btsell": {
        "frames": {
            "btsell.png": {
                "frame": {"x":0, "y":0, "w":254, "h":104, "ox":0, "oy":0},
                "size": {"w":254,"h":104},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/btsell.png",
            "size": {"w": 256, "h": 106}
        }
    },

    "btbuy": {
        "frames": {
            "btbuy.png": {
                "frame": {"x":0, "y":0, "w":254, "h":104, "ox":0, "oy":0},
                "size": {"w":254,"h":104},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/btbuy.png",
            "size": {"w": 256, "h": 106}
        }
    },


    "appstore": {
        "frames": {
            "appstore.png": {
                "frame": {"x":0, "y":0, "w":182, "h":61, "ox":0, "oy":0},
                "size": {"w":182,"h":61},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/appstore.png",
            "size": {"w": 184, "h": 63}
        }
    },

    "playstore": {
        "frames": {
            "playstore.png": {
                "frame": {"x":0, "y":0, "w":190, "h":57, "ox":0, "oy":0},
                "size": {"w":190,"h":57},
                "scale": 1
            }

        },
        "meta": {
            "image": "media/graphics/playstore.png",
            "size": {"w": 192, "h": 59}
        }
    },
    "finger": {
        "frames": {
            "finger.png": {
                "frame": {"x":0, "y":0, "w":141, "h":146, "ox":0, "oy":0},
                "size": {"w":141,"h":146},
                "scale": 1
            }
        },
        "meta": {
            "image": "media/graphics/finger.png",
            "size": {"w": 141, "h": 148}
        }
    },

}; 