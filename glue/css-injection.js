// register your CSS here
var cssQueue = [
	'html, body { color: rgb(255, 255, 255); width: 100%; height: 100%; margin: 0px; padding: 0px; font-size: 12pt; text-align: center; font-family: arial, helvetica, sans-serif; overflow: hidden; background-color: rgb(0,0,0); }', 
	'html, body { -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; }', 
	'#canvas { position: absolute; left: 0px; right: 0px; top: 0px; margin: auto; z-index: 1; width: 1px;min-width: 100%;*width: 100%; height: 100%; transform: scale3d(1, 1, 1); }',
	'@font-face { font-family: "sans"; src: url("media/fonts/sans.woff") format("woff"); }',
	'div#closebuttonbox {background-image: url("media/graphics/closebtbg.jpg"); background-repeat:no-repeat ; background-position:center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;}'
];

// CSS injection
var sheet = (function() {
	// Create the <style> tag
	var style = document.createElement("style");

	style.setAttribute("type", "text/css");
	// Add a media (and/or media query) here if you'd like!
	// style.setAttribute("media", "screen")
	// style.setAttribute("media", "only screen and (max-width : 1024px)")

	// WebKit hack :(
	style.appendChild(document.createTextNode(""));

	// Add the <style> element to the page
	document.head.appendChild(style);

	return style.sheet;
})();

if(sheet && typeof(ig) === "undefined" || (ig && !ig.global.wm)) {
	if(cssQueue && cssQueue.length) {
	    for(var cssIndex = 0; cssIndex < cssQueue.length; cssIndex++) {
	        sheet.insertRule(cssQueue[cssIndex], sheet.cssRules.length);
	    }
	}
}