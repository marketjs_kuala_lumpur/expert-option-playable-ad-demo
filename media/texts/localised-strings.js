// register texts here, translations are provided by third-party service (Gengo & TextMaster)
var localisedStrings = {
	buyAlert:"The Price is Low! Click Buy Now",    
	buyLimit:["Open positions","limit exceeded"],
	buyFund:["Insufficient funds"],    
	sellAlert:"The price is high! Click Sell Now",        
	endAlertProfit:["Congratulations! You've earned",        
				"profit!"
			   ],
	endAlertLoss:["Oh no!","You loses your investment"],
	endAlertDraw:["You loses your investment"],
	endAlertNoAction:["Option ended. Trade now and","start earning real money."],
	endcardDesktop:[
					"Trade now",
					"and start earning",
					"real money"
					],
	endcardMobile:[
					"Install our app now",
					"and start earning",
					"real money"
					],

	inGameMessage:[
					"Keep buying!!!",
					"Keep selling to gain profit!!!"
					],


        
};