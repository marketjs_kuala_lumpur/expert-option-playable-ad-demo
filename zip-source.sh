#! /bin/bash
# Usage: sh zip-source.sh
# Will zip up deliverable sources for client
CURRENT_DIRECTORY=${PWD##*/}
ARCHIVE_NAME=$CURRENT_DIRECTORY"-source"
echo "Building archive ..."

if [ ! -f "../$ARCHIVE_NAME.zip" ]; 
then
    echo "File not found. Creating"
else
echo "File exist. Removing"
rm "../$ARCHIVE_NAME.zip"
fi

zip -r "../$ARCHIVE_NAME.zip" ./dev.html ./weltmeister.html ./encode_media.py ./build-fractional.sh ./compiler.jar ./lib ./tools ./media -x "*.DS_Store" 

echo "Done"
