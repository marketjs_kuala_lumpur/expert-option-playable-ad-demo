#! /bin/bash
#
# MarketJS Deployment System
# -----------------------------------------------------------------------
# Copyright (c) 2012 MarketJS Limited. Certain portions may come from 3rd parties and
# carry their own licensing terms and are referenced where applicable. 
# -----------------------------------------------------------------------

# Usage: sh push.sh [options]
# Example: sh push.sh -b -d (bake, then deploy)
#

NOW=$(date +"%d-%b-%Y")
CURRENT_DIRECTORY=${PWD##*/}
ARCHIVE_NAME=$CURRENT_DIRECTORY"-"$NOW

BITBUCKET_BRANCH="master"


## declare an array variable
#"glue/howler/howler.min.js"
#"glue/handlers.js"
                    
declare -a files=(  "glue/css-injection.js"
                    "glue/sprite-sheets.js"
                    "glue/data/vector2.js"
                    "glue/data/vector3.js"
                    "glue/data/color.js"
                    "glue/data/angle.js"
                    "glue/data/circle.js"
                    "glue/data/rect.js"
                    "glue/data/cylinder.js"
                    "glue/data/line.js"
                    "glue/data/file.js"
                    "glue/data/constants.js"
                    
                    "glue/helpers/event-tracker.js"
                    "glue/helpers/ad-helper.js"
                    "glue/helpers/game-helper.js"
                    "glue//helpers/math-helper.js"
                    "glue//helpers/system-helper.js"
                    "glue//helpers/location-helper.js"
                    "glue//helpers/settings-helper.js"
                    "glue/helpers/draw-helper.js"
                    "glue//helpers/view-helper.js"
                    "glue/helpers/logger.js"
                    
                    "glue/helpers/mjs.js"

                    "glue/game-settings.js"
                    "media/texts/localised-strings.js"
                    "_factory/game/game.js")
FILELIST=""
for i in "${files[@]}"
do
   echo "$i"
   
   FILELIST+="--js=$i "
done

bake (){
    echo "Pre-Bake: remove debug from main.js"
    sed -i.bak '/impact.debug.debug/d' lib/game/main.js
    
	echo ""
	echo "Baking ..."
	echo ""
	
	cd tools
	./bake.sh
	cd ..
	
	echo ""
	echo "Baking Done!"
	echo ""
    
    echo "Post-Bake: restore main.js"
    mv -f lib/game/main.js.bak lib/game/main.js
}

inject_build_timestamp(){
	echo "injecting build timestamp"
	
	echo $NOW
	sed -e 's/BUILD_TIMESTAMP.*,/BUILD_TIMESTAMP: "'"$NOW"'",/' lib/game/main.js > lib/game/main.js.tmp && mv lib/game/main.js.tmp lib/game/main.js
	
	echo "Done"
}

prep_factory_domainlock(){
	cp domainlock.js _factory/domainlock/raw.js
}

compile_test_game (){
	echo "Compiling game.js for testing ..."		
	java -jar compiler.jar \
	--warning_level=QUIET \
    $FILELIST \
    --js_output_file=dist/game.js \
	--language_in=ECMASCRIPT5	
	echo "Done!"
    	
	rm *.bak
}

prep_production (){
	echo "Zipping up media files for target language ..."
    cp templatedev.html dev.html
    
    echo "Constructing"
    CONSTRUCT=""
    for i in "${files[@]}"
    do
       #echo "$i"
       CONSTRUCT+="$i "
    done
    python injectscripts.py $CONSTRUCT
    
    
	#echo '$1:' $1
	#echo '$2:' $2
	#echo '$3:' $3
	#echo '$4:' $4
    
    #sed '/<!--INJECTJS-->/  a\
    #<script charset="utf-8" type="text/javascript" src='$i'></script>'$'\n' ./testdev.html > ./testdev2.html
    
	sh zip-media-folder.sh $1
	echo "Done ..."
	
	echo "Create basic index.html ..."
    
	cp dev.html index.html
	echo "Done ..."
		
	# Clean JS paths
    for i in "${files[@]}"
    do
       #echo "$i"
       #sed -n "#<script charset=\"utf-8\" type=\"text/javascript\" src=\"$i\"></script>#!p\ " index.html > temp && mv temp index.html
        sed -i.bak "\#$i#d" index.html
    done
    
	#sed -n '/glue\/css-injection.js/!p' index.html > temp && mv temp index.html
	#sed -n '/glue\/game-settings.js/!p' index.html > temp && mv temp index.html
	#sed -n '/glue\/sprite-sheets.js/!p' index.html > temp && mv temp index.html
	#sed -n '/glue\/handlers.js/!p' index.html > temp && mv temp index.html
    #sed -n '/glue\/helpers\/view-helper.js/!p' index.html > temp && mv temp index.html
    #sed -n '/glue\/helpers\/mjs.js/!p' index.html > temp && mv temp index.html
    
	#sed -n '/media\/texts\/localised-strings.js/!p' index.html > temp && mv temp index.html
	
	sed -n '/lib\/impact\/impact.js/!p' index.html > temp && mv temp index.html	
	sed -i.bak 's/lib\/game\/main.js/game.js/g' index.html
    
    cp index.html dist/$CURRENT_DIRECTORY.html
    
	# Remove temp files
	echo "Removing temp files ..."
	rm *.bak
	rm temp	
	echo "Done!"
	
	echo "Compiling game.js for _factory ..."		
	java -jar compiler.jar \
	--warning_level=QUIET \
	--js=game.min.js \
	--js_output_file=_factory/game/game.js \
	--language_in=ECMASCRIPT5
	echo "Done!"

	# Remove temp files
	echo "Removing game.min.js ..."
	rm game.min.js
	echo "Done!"		
}

encode_media (){
    python encode_media.py dist/game.js
    

	cp dist/game.js game.js
    
}

inline_js (){
    python inline_js.py dist/$CURRENT_DIRECTORY.html dist/game.js
    
    rm dist/game.js
}

deploy (){
	echo ""
	echo "Deploying ..."
	echo ""

	python boto-s3-upload.py -l $2 $1
	
	echo ""
	echo "Deploying Done!"
	echo ""	
}

gitpush (){
	git add --all
	git commit -m "$*"
	git push origin $BITBUCKET_BRANCH
}

report_size () {
	echo "$CURRENT_DIRECTORY Size Report"
	
	echo ""
	echo $CURRENT_DIRECTORY.html
	
	echo ""
	echo "game.js"
	du -h -s dist/game.js
	
	echo ""
	echo "Done"
}

compress () {
	echo "Building archive ..."
	
	if [ ! -f "$ARCHIVE_NAME.zip" ]; 
	then
	    echo "File not found!"
	else
	echo "File exist. Removing"
	rm "$ARCHIVE_NAME.zip"
	fi
	
	zip -r "../$ARCHIVE_NAME.zip" ./dist/$CURRENT_DIRECTORY.html -x "*.DS_Store" 
	
	echo "Done"
}

while getopts "l:bnahscr:g:" opt; do
  case $opt in
	h)
		echo "Usage: sh push.sh [option]"
		echo "Deploy Options"
		echo "\t -b \t Build all files"
		echo "\t -l \t Select language by code (en,jp,kr,zh,de,es, etc ...)"
		echo "\t -a \t Upload all files"
		echo "\t -n \t Upload new (recent) files up to 12 hrs"
		echo "\t -g \t Add, commit and push to remote repo (origin)"
		echo "Working example (copy paste directly): sh push.sh -b -l jp -a -g 'somefix'"
	  ;;
    l)
		echo "language to use:" $3
      ;;
    b)
        ##create the dist folder
        rm -rf dist
        mkdir dist
        
		inject_build_timestamp
		bake
		prep_production $3
		compile_test_game
        encode_media
        inline_js
      ;;
    n)
		deploy --new $3
      ;;
    a)
		deploy --all $3
      ;;
	s)
		pushstaging $OPTARG
	  ;; 
    g)
		gitpush $OPTARG
      ;;
    c)
        compress
      ;;
    r)
    	report_size
      ;;
    \?)
		echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
