ig.module(
	'impact.input'
)
.defines(function(){ "use strict";
	
ig.KEY = {
	'MOUSE1': -1,
	'MOUSE2': -3,
	'MWHEEL_UP': -4,
	'MWHEEL_DOWN': -5,
};


ig.Input = ig.Class.extend({
	bindings: {},
	actions: {},
	presses: {},
	locks: {},
	delayedKeyup: {},
	
	isUsingMouse: false,
	isUsingKeyboard: false,
	isUsingAccelerometer: false,
	mouse: {x: 0, y: 0},
	accel: {x: 0, y: 0, z: 0},
	
	
	initMouse: function() {
		if( this.isUsingMouse ) { return; }
		this.isUsingMouse = true;
		var mouseWheelBound = this.mousewheel.bind(this);
		ig.system.canvas.addEventListener('mousewheel', mouseWheelBound, false );
		ig.system.canvas.addEventListener('DOMMouseScroll', mouseWheelBound, false );
		
		ig.system.canvas.addEventListener('contextmenu', this.contextmenu.bind(this), false );
		ig.system.canvas.addEventListener('mousedown', this.keydown.bind(this), false );
		ig.system.canvas.addEventListener('mouseup', this.keyup.bind(this), false );
		ig.system.canvas.addEventListener('mousemove', this.mousemove.bind(this), false );
		
		if( ig.ua.touchDevice ) {
			// Standard
			ig.system.canvas.addEventListener('touchstart', this.keydown.bind(this), false );
			ig.system.canvas.addEventListener('touchend', this.keyup.bind(this), false );
			ig.system.canvas.addEventListener('touchmove', this.mousemove.bind(this), false );
			
			// MS
			ig.system.canvas.addEventListener('MSPointerDown', this.keydown.bind(this), false );
			ig.system.canvas.addEventListener('MSPointerUp', this.keyup.bind(this), false );
			ig.system.canvas.addEventListener('MSPointerMove', this.mousemove.bind(this), false );
			ig.system.canvas.style.msTouchAction = 'none';
		}
	},

		
	mousewheel: function( event ) {
		var delta = event.wheelDelta ? event.wheelDelta : (event.detail * -1);
		var code = delta > 0 ? ig.KEY.MWHEEL_UP : ig.KEY.MWHEEL_DOWN;
		var action = this.bindings[code];
		if( action ) {
			this.actions[action] = true;
			this.presses[action] = true;
			this.delayedKeyup[action] = true;
			event.stopPropagation();
			event.preventDefault();
		}
	},
	
	
	mousemove: function( event ) {		
		var internalWidth = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth;
		var scale = ig.system.scale * (internalWidth / ig.system.realWidth);
		
		var pos = {left: 0, top: 0};
		if( ig.system.canvas.getBoundingClientRect ) {
			pos = ig.system.canvas.getBoundingClientRect();
		}
		
		var ev = event.touches ? event.touches[0] : event;
		this.mouse.x = (ev.clientX - pos.left);// / scale;
		this.mouse.y = (ev.clientY - pos.top);// / scale;
	},
	
	
	contextmenu: function( event ) {
		if( this.bindings[ig.KEY.MOUSE2] ) {
			event.stopPropagation();
			event.preventDefault();
		}
	},
	
	
	keydown: function( event ) {
		var tag = event.target.tagName;
		if( tag == 'INPUT' || tag == 'textAREA' ) { return; }
		
		var code = event.type == 'keydown' 
			? event.keyCode 
			: (event.button == 2 ? ig.KEY.MOUSE2 : ig.KEY.MOUSE1);
		
		// Focus window element for mouse clicks. Prevents issues when
		// running the game in an iframe.
		if( code < 0 && !ig.ua.mobile ) {
			window.focus();
		}
		
		if( event.type == 'touchstart' || event.type == 'mousedown' ) {
			this.mousemove( event );
		}
			
		var action = this.bindings[code];
		if( action ) {
			this.actions[action] = true;
			if( !this.locks[action] ) {
				this.presses[action] = true;
				this.locks[action] = true;
			}
			event.preventDefault();
		}
	},
	
	
	keyup: function( event ) {
		var tag = event.target.tagName;
		if( tag == 'INPUT' || tag == 'textAREA' ) { return; }
		
		var code = event.type == 'keyup' 
			? event.keyCode 
			: (event.button == 2 ? ig.KEY.MOUSE2 : ig.KEY.MOUSE1);
		
		var action = this.bindings[code];
		if( action ) {
			this.delayedKeyup[action] = true;
			event.preventDefault();
		}
	},
	
	
	
	bind: function( key, action ) {
		if( key < 0 ) { this.initMouse(); }
		else if( key > 0 ) { this.initKeyboard(); }
		this.bindings[key] = action;
	},
	
	
	bindTouch: function( selector, action ) {
		var element = ig.$( selector );
		
		var that = this;
		element.addEventListener('touchstart', function(ev) {that.touchStart( ev, action );}, false);
		element.addEventListener('touchend', function(ev) {that.touchEnd( ev, action );}, false);
		element.addEventListener('MSPointerDown', function(ev) {that.touchStart( ev, action );}, false);
		element.addEventListener('MSPointerUp', function(ev) {that.touchEnd( ev, action );}, false);
	},
	
	
	unbind: function( key ) {
		var action = this.bindings[key];
		this.delayedKeyup[action] = true;
		
		this.bindings[key] = null;
	},
	
	
	unbindAll: function() {
		this.bindings = {};
		this.actions = {};
		this.presses = {};
		this.locks = {};
		this.delayedKeyup = {};
	},
	
	
	state: function( action ) {
		return this.actions[action];
	},
	
	
	pressed: function( action ) {
		return this.presses[action];
	},
	
	released: function( action ) {
		return !!this.delayedKeyup[action];
	},
		
	clearPressed: function() {
		for( var action in this.delayedKeyup ) {
			this.actions[action] = false;
			this.locks[action] = false;
		}
		this.delayedKeyup = {};
		this.presses = {};
	},
	
	touchStart: function( event, action ) {
		this.actions[action] = true;
		this.presses[action] = true;
		
		event.stopPropagation();
		event.preventDefault();
		return false;
	},
	
	
	touchEnd: function( event, action ) {
		this.delayedKeyup[action] = true;
		event.stopPropagation();
		event.preventDefault();
		return false;
	}
});

});