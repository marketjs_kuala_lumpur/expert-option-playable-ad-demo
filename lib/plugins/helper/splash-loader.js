ig.module('plugins.helper.splash-loader')
.requires(
	'impact.loader',
	'impact.animation',
	
	// entity
	'game.entities.ui.banner'
)
.defines(function() {
	ig.SplashLoader = ig.Loader.extend({
        _drawStatus: 0,
        status: 0,
        
		endTime: {
            MRAID: 250,
            NON_MRAID: 250
        },
        endThreshold: 0.999,
        isEnded: false,
        
        gamebg:{
            spriteImage: new ig.Image(spriteSheets.gamebg.meta.image),
            spriteID:"gamebg.png",
            spriteSheet:"gamebg"
        },

        tittle:{
            spriteImage: new ig.Image(spriteSheets.tittle.meta.image),
            spriteID:"tittle.png",
            spriteSheet:"tittle"
        },
        
        customFonts: [
            "bowlby"
        ],
        waitIdle:17,
        delay:0,
                
		init:function(gameClass,resources){
			this.parent(gameClass,resources);
                        
            var head = document.getElementsByTagName('head').item(0),
            js = document.createElement('script'),
            s = 'mraid.js';
            js.setAttribute('type', 'text/javascript');
            js.setAttribute('src', s);
            head.appendChild(js);
            
            this.preloadFonts();
		},
		
        preloadFonts: function() {
            var context = ig.system.context;
            
			context.fillStyle = "#000000";
            
            if(this.customFonts && this.customFonts.length) {
                for(var fontIndex = 0; fontIndex < this.customFonts.length; fontIndex++) {
                    context.font = "18px " + this.customFonts[fontIndex];
                    context.fillText("preload bowlby font", 0, 0);
                }
            }			
        },
        
        end:function(){
            if(this.isEnded) { return; } 
            if(MJS.settings.data.preloader === "on")
            {
                if(this._drawStatus >= this.endThreshold) {
                    this._drawStatus = 1;
                    var pauseInterval = (window.mraid)?this.endTime.MRAID:this.endTime.NON_MRAID;
                    
                    if(pauseInterval>0) {
                        window.setTimeout(
                            function() {
                                this.endTime.MRAID = 0;
                                this.endTime.NON_MRAID = 0;
                            }.bind(this),
                            pauseInterval
                        );
                    }	
                    else {
                        this.isEnded = true;
                    }
                
                }
            }
            else
            {

                if(this.delay > this.waitIdle){
                    this.isEnded = true;
                }

                // this.isEnded = true;
            }
            
            if(this.isEnded === true) {
                // stop drawing, and set game
                this.parent();
                				
				// Analytics
				// Game Loaded
				////PlayableSdk.track.gameLoaded();	
            }
		},
        
        updateStatus: function() {
            this.delay +=1;
			this._drawStatus += (this.status - this._drawStatus)/12;
			if(this._drawStatus >= this.endThreshold) { this.end(); }
        },
        //delay:0,
        readyOnce:false,
        preloadUseCustomClose:function(){
            //this.delay+=ig.system.tick
            if(typeof(mraid) !== null && typeof(mraid) !== "undefined")
            {
                var bool = window.MJS.settings.getBool("disableCustomClose")
                if(bool)
                {
                    if(mraid.getState() === 'loading'){
                        if(!this.readyOnce)
                        {
                            mraid.addEventListener('ready', this.preloadUseCustomClose);
                            mraid.addEventListener('ready', window.MJS.view.orientationHandler());
                            this.readyOnce=true
                        }
                    }
                    else{
                        try
                        {
                            mraid.useCustomClose(true)
                        }
                        catch(err)
                        {
                            console.log("Mraid missing")
                        }
                    }
                    
                }
            }
            else
            {
                //console.log("mraid undefined")
            }
        },
        
		
		draw: function() {
            window.MJS.view.orientationHandler()
            //console.log("test")
            
            // fix for double close button in MoPub
            this.preloadUseCustomClose();
                        
			// black background
			this.drawBackground();
			
            // update draw status
			this.updateStatus();

            this.drawLoadingBar();
            
            if(MJS.settings.data.preloader === "on")
            {
                //console.log("preloader");
                /*
    			if( this.logo.spriteImage.loaded ) {
    				this.drawLogo();
                    this.drawLoadingBar();
    			}
    			else {
    				this.drawConnectingInfo();
    			}
                */
			    this.drawConnectingInfo();
            }
            else {
                
            }
		},
        
        drawBackground: function() {
            var context = ig.system.context;
			
                if (MJS.view.viewport.orientation.landscape) {
                    var fr = spriteSheets[this.gamebg.spriteSheet].frames[this.gamebg.spriteID].frame;
                    context.drawImage(
                        this.gamebg.spriteImage.data,
                        0,
                        300,
                        fr.w,
                        400,
                        0,
                        0,
                        ig.system.width,
                        ig.system.height
                    );                
 
                }else{
                   var fr = spriteSheets[this.gamebg.spriteSheet].frames[this.gamebg.spriteID].frame;
                    context.drawImage(
                        this.gamebg.spriteImage.data,
                        0,
                        0,
                        fr.w,
                        fr.h,
                        0,
                        0,
                        ig.system.width,
                        ig.system.height
                    );                
                }    

                var fr = spriteSheets[this.tittle.spriteSheet].frames[this.tittle.spriteID].frame;
                context.drawImage(
                    this.tittle.spriteImage.data,
                    0,
                    0,
                    fr.w,
                    fr.h,
                    ig.system.width/2-fr.w/2,
                    ig.system.height/2-fr.h/2,
                    fr.w,
                    fr.h
                );                

        },
        
        drawConnectingInfo: function() {
            var context = ig.system.context,
                pos = {
                    x: 0.5*ig.system.width,
                    y: 0
                };
                            
			context.fillStyle = "#FFFFFF";
			context.textAlign = "center";
			context.textBaseline = "top"
			
			context.font = "12px arial";
            context.fillText("Connecting to Game Server...", pos.x, pos.y);
        },
        
        drawLogo: function() {
            var context = ig.system.context;
            context.fillStyle="rgba(255,0,0,1)";
            context.fillRect(0,0,50,50);
        },
        
        drawLoadingBar: function() {
            var context = ig.system.context,
                pos = {
                    x: 0.2*ig.system.width,
                    y: 0.70*ig.system.height
                },
                size = {
                    x: 0.6*ig.system.width,
                    y: 20
                };

            context.strokeStyle = 'rgba(255,255,255,1)';
            context.fillStyle = 'rgba(11,18,83,1)';
            context.lineWidth=2;
            this.roundRect(context, pos.x, pos.y, size.x, size.y, 10, true, true);
            context.fillStyle = 'rgba(55,136,229,1)';
            this.roundRect(context, pos.x, pos.y, size.x*this._drawStatus, size.y, 10, true, false);
                
   //          // fill background - placeholder
			// context.fillStyle = '#30251d';
			// context.fillRect( pos.x, pos.y, size.x, size.y );
	
   //          // fill foreground - status
			// context.fillStyle = '#ff5703';
			// context.fillRect( pos.x, pos.y, size.x*this._drawStatus, size.y );
	
   //          // stroke foregound - frame
			// context.strokeStyle = '#ff5703';
			// context.strokeRect( pos.x, pos.y, size.x, size.y );
        },

        roundRect: function(ctx, x, y, width, height, radius, fill, stroke) {
            if (typeof stroke == "undefined" ) {
                stroke = true;
            }
            if (typeof radius === "undefined") {
                radius = 5;
            }
            ctx.beginPath();
            ctx.moveTo(x + radius, y);
            ctx.lineTo(x + width - radius, y);
            ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
            ctx.lineTo(x + width, y + height - radius);
            ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
            ctx.lineTo(x + radius, y + height);
            ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
            ctx.lineTo(x, y + radius);
            ctx.quadraticCurveTo(x, y, x + radius, y);
            ctx.closePath();
            if (stroke) {
                ctx.stroke();
            }
            if (fill) {
                ctx.fill();
            }
        },        


	});
});