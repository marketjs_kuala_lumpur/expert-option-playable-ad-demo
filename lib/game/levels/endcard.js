ig.module( 'game.levels.endcard' )
.requires( 'impact.image','game.entities.endcard-control','game.entities.ui.pointer' )
.defines(function(){
LevelEndCard=/*JSON[*/{
	"entities": [
		{
			"type": "EntityEndcardControl",
			"x": 0,
			"y": 0
		},
		{
			"type": "EntityPointer",
			"x": 0,
			"y": 0
		}
	],
	"layer": []
}/*]JSON*/;
});