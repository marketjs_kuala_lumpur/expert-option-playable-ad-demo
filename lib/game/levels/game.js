ig.module( 'game.levels.game' )
.requires( 'impact.image','game.entities.game-control','game.entities.ui.pointer' )
.defines(function(){
LevelGame=/*JSON[*/{
	"entities": [
		{
			"type": "EntityGameControl",
			"x": -50,
			"y": -50
		},
		{
			"type": "EntityPointer",
			"x": 0,
			"y": 0
		}
	],
	"layer": []
}/*]JSON*/;
});