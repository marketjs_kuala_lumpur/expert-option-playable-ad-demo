ig.module('game.entities.game-control')
.requires(
    'impact.entity',    
	'game.entities.ui.banner',
	'game.entities.ui.finger',
	'game.entities.ui.hud',
	'game.entities.buttons.button-install-now',
	'game.entities.buttons.button-sell',
	'game.entities.buttons.button-buy'
	
)
.defines(function () {

    EntityGameControl = ig.Entity.extend({
    	zIndex: 0,
        //Add a clickable layer for this cat @TODO
        
        gamebg:{
            spriteImage: new ig.Image(spriteSheets.gamebg.meta.image),
            spriteID:"gamebg.png",
            spriteSheet:"gamebg"
        },
        toplogo:{
            spriteImage: new ig.Image(spriteSheets.toplogo.meta.image),
            spriteID:"toplogo.png",
            spriteSheet:"toplogo"
        },

        grid:{
            spriteImage: new ig.Image(spriteSheets.grid.meta.image),
            spriteID:"grid.png",
            spriteSheet:"grid"
        },

        panel:{
            spriteImage: new ig.Image(spriteSheets.panel.meta.image),
            spriteID:"panel.png",
            spriteSheet:"panel"
        },
        goldtext:'00:00:00',
        transitionTime:1,
        transitionTime2:0.5,
        transitionTime3:6,
        balanceText:10000,
        priceText:0,
        priceLength:1350,
        priceLow:1010,
        priceConst:0,
        investText:0,
        investText2:0,
        investCount:0,
        path:[],        
        lastPoint:null,
        valY:{min:80,max:570},
        centerY:245,
        pointlength:0,
        pageCount:0,
        pauseDot:false,
        offsetX:0,
        offsetFixposX:-25,
        lastPrice:0,
        buyStatus:false,
        sellStatus:false,
        buyAlpha:1,
        sellAlpha:1,
        endGameStatus:false,
        endGameAlpa:1,
        timeLimit:0,
        maxTimeLimit:17,
        offsetPosText:0,
        trend:'down',
        trendPoint:[
	        			{
	        				trend:'up',
	        				time:2,
	        				start:4,	
	        				length:75
	        			},
	        			{
	        				trend:'down',
	        				time:10,	
	        				start:14,
	        				length:425	
	        			},
        		    ],
        graphMessage:[
	        			{
	        				time:6,
	        				status:'buy',
	        			},
	        			{
	        				time:14,	
	        				status:'sell'
	        			},
        		    ],
        checkPoint:[],
        labelPoint:[],
        ingameStatus:false,
        ingameText:null,
        ingameAlpha:1,
        gatePos:{x1:778,y1:70,x2:778,y2:580},
        graphlength:0,
        profitPercent:0.80,
        highestInvest:0,
        screen:'P',
		init: function (x, y, settings) {
            this.parent(x, y, settings);
			
			if(!ig.global.wm) {
	            this.goldTimer=new ig.Timer();
	            this.pathTimer=new ig.Timer();
	            // this.alertTimer=new ig.Timer();

				// mraid close button
				if(MJS.settings.preloaderStartCountdown !== "on") { MJS.ad.initCloseButton(); }
				
				// top banner
				// this.banner = ig.game.spawnEntity(EntityBanner,0,0, {control: this});
                
            	if(ig.ua.mobile){
            		var caption='Install Now';
            	}else{
            		var caption='Trade Now';		            		
            	}


				this.installnow2 = ig.game.spawnEntity(EntityButtonInstallNow,0,ig.system.height-70, 
																					{parrent: this,
																					  boxPos:{x:0,y:ig.system.height-70},
																					  boxSize:{x:ig.system.width,y:70},
																					  updown:1,
																					  caption:'',
																					  endcard:false,
																					  div_layer_name:'install-now-button2',
																					});
                this.installnow2.updateOnOrientationChange();
                this.installnow2.reset();


				this.installnow = ig.game.spawnEntity(EntityButtonInstallNow,300,8, {parrent: this,
																					  boxPos:{x:300,y:8},
																					  boxSize:{x:170,y:50},
																					  updown:0,
																					  caption:caption,
																					  endcard:false,
																					  div_layer_name:'install-now-button',
																					  });
                this.installnow.updateOnOrientationChange();
                this.installnow.reset();


				this.btsell = ig.game.spawnEntity(EntityButtonSell,140,830,{parrent: this,zIndex:this.zIndex+150});
				this.btbuy = ig.game.spawnEntity(EntityButtonBuy,400,830,{parrent: this,zIndex:this.zIndex+150});


				// this.dot = ig.game.spawnEntity(EntityDot,0,0,{parrent: this,zIndex:this.zIndex+40});
				this.goBig();
                // timer
				ig.game.setupTimer(this);
				
                
                // redirection
                if(MJS.settings.redirectOnGameStart > 0) {
                    this.redirectToAppStore("Game start");
                }
                
                this.constOrientation=400/490;

                this.updateOnOrientationChange();
                this.initPrice();

				ig.game.sortEntitiesDeferred();
			}
        },
        ready:function(){
			ig.game.sortEntitiesDeferred();
        },
        initPrice:function(){
        	//570-80

        	var h=this.valY.max-this.valY.min;
        	var centerH=this.valY.min+h/2;
	        var offsetH=-50+Math.floor(Math.random() * 100); 
	        var restY=centerH+offsetH;
	        var firstPoint={x:30,y:restY};
	        this.lastPoint = firstPoint;
	        this.priceConst=(this.priceLength-this.priceLow)/h;
	        this.priceText=(h-(restY-this.valY.min))*this.priceConst+this.priceLow;
	        this.lastPrice=this.priceText;

	        this.path.push({x:25,y:restY});

	        this.path.push({x:30,y:restY});
			this.pointlength +=1;

			// console.log(this.priceText);

        },
        goLandscape:function(){
        	var h=this.valY.max-this.valY.min;
        	var centerH=this.valY.min+h/2;
	        this.priceConst=(this.priceLength-this.priceLow)/h;

			for(var i=0;i<=this.path.length-1;i++){
				var tempY=this.path[i].y;
				this.path[i].y=tempY*this.constOrientation;
			}	

			for(var i=0;i<=this.checkPoint.length-1;i++){
				var tempY=this.checkPoint[i].y;
				this.checkPoint[i].y=tempY*this.constOrientation;
			}	

			for(var i=0;i<=this.labelPoint.length-1;i++){
				var tempY=this.labelPoint[i].y;
				this.labelPoint[i].y=tempY*this.constOrientation;
			}	



			if(this.lastPoint){
				var tempY=this.lastPoint.y;
				this.lastPoint.y=tempY*this.constOrientation;
			}
			this.offsetX=0;
        },
        goPortrait:function(){
        	var h=this.valY.max-this.valY.min;
        	var centerH=this.valY.min+h/2;
	        this.priceConst=(this.priceLength-this.priceLow)/h;

			for(var i=0;i<=this.path.length-1;i++){
				var tempY=this.path[i].y;
				this.path[i].y=tempY/this.constOrientation;
			}	

			for(var i=0;i<=this.checkPoint.length-1;i++){
				var tempY=this.checkPoint[i].y;
				this.checkPoint[i].y=tempY/this.constOrientation;
			}	

			for(var i=0;i<=this.labelPoint.length-1;i++){
				var tempY=this.labelPoint[i].y;
				this.labelPoint[i].y=tempY/this.constOrientation;
			}	

			if(this.lastPoint){
				var tempY=this.lastPoint.y;
				this.lastPoint.y=tempY/this.constOrientation;
			}

	        if(this.pointlength>=19){
				this.offsetX=400;
				this.pageCount=1;
	        }
        },
		updateOnOrientationChange: function() {
            // console.log("game control orientation change");            
                if (MJS.view.viewport.orientation.landscape) {
                	this.screen='L';

				    this.installnow2.boxPos={x:0,y:ig.system.height+10};
                	this.installnow2.hide();
	                this.installnow2.updateOnOrientationChange();
	                this.installnow2.reset();


 				    this.installnow.boxPos={x:ig.system.width/2+20,y:4};
				    this.installnow.boxSize={x:170,y:40};
	                this.installnow.updateOnOrientationChange();
	                this.installnow.reset();

                    this.valY={min:60,max:460};
			        this.gatePos={x1:778,y1:50,x2:778,y2:455};

					this.btsell.posx = 640;
					this.btsell.posy = ig.system.height-70;					
					this.btsell.sizex= 150;
					this.btsell.sizey= 70;

					this.btbuy.posx = 800;
					this.btbuy.posy = ig.system.height-70;					
					this.btbuy.sizex= 150;
					this.btbuy.sizey= 70;
					
					this.goLandscape();

                }else{
                	this.screen='P';
				    this.installnow2.boxPos={x:0,y:ig.system.height-70};
				    this.installnow2.boxSize={x:ig.system.width,y:70};
                	this.installnow2.show();
	                this.installnow2.updateOnOrientationChange();
	                this.installnow2.reset();

 				    this.installnow.boxPos={x:300,y:8};
				    this.installnow.boxSize={x:170,y:50};
	                this.installnow.updateOnOrientationChange();
	                this.installnow.reset();

                    this.valY={min:80,max:570};
			        this.gatePos={x1:778,y1:70,x2:778,y2:580};


					this.btsell.posx = 25;
					this.btsell.posy = 785;					
					this.btsell.sizex= 254;
					this.btsell.sizey= 104;


					this.btbuy.posx = 285;
					this.btbuy.posy = 785;					
					this.btbuy.sizex= 254;
					this.btbuy.sizey= 104;

					this.goPortrait();

                }

        },		

		update: function() {
			this.parent();			
			this.updateGoldIndex();
	        this.priceText=((this.valY.max-this.valY.min)-(this.lastPoint.y-this.valY.min))*this.priceConst+this.priceLow;
	        this.investText = (this.investCount * this.priceText);
	        if(this.investText>this.highestInvest){
	        	this.highestInvest=this.investText;
	        }
		},
        updateGoldIndex:function(){
        	if(this.endGameStatus) return;


            if(this.pathTimer){
                if(this.pathTimer.delta() > this.transitionTime2 && !this.pauseDot && !this.endGameStatus){
                    this.pathTimer.reset();
					this.addNewPoint();
                }
            }

            if(this.goldTimer){
                if(this.goldTimer.delta() > this.transitionTime && !this.pauseDot){
                    this.goldTimer.reset();
				
		            var seconds = this.maxTimeLimit-this.timeLimit-1;
		            var min = Math.floor(seconds/60);
		            var sec = seconds % 60;

		            if(min<10){
		                var strMin='0'+min;
		            }else{
		                var strMin=min;
		            }

		            if(sec<10){
		                var strSec='0'+sec;
		            }else{
		                var strSec=sec;
		            }
		            this.goldtext = '00:'+strMin+':'+strSec;

		            if(!this.pauseDot){
						this.timeLimit +=1;
					}else{
						return;
					}

					for(var x=0;x<=this.trendPoint.length-1;x++){
						if(this.timeLimit==this.trendPoint[x].time){

							this.graphlength=this.trendPoint[x].length;

							if(ig.game.guidedExperience=='on'){
								this.pauseDot = true;
							}

							if(this.trendPoint[x].trend=='up'){
					        	this.btbuy.disabled=false;
								this.btbuy.startFinger();
								this.alertDown();
							}else{
								this.btbuy.fingerStatus=false;
					        	this.btbuy.disabled=true;

				        		this.btsell.disabled=false;
								this.btsell.startFinger();
								this.alertUp();
							}
						}

						if(this.timeLimit==this.trendPoint[x].start){
							this.trend=this.trendPoint[x].trend;
						}
					}


					for(var x=0;x<=this.graphMessage.length-1;x++){
						if(this.timeLimit==this.graphMessage[x].time-1){
								this.btbuy.fingerStatus=false;
								this.btsell.fingerStatus=false;

						}

						if(this.timeLimit==this.graphMessage[x].time){
							this.alertInGameMessage(x,this.graphMessage[x].status);
						}
					}


					if((this.timeLimit>=this.maxTimeLimit || this.graphlength>=800) && !this.endGameStatus ){
						this.endgameValue();
					}
                }
            }
        },
        endgameValue:function(){
			this.endGameStatus=true;
			this.goldTimer=null;
			this.pathTimer=null;

			this.btsell.fingerStatus=false;
        	this.btsell.disabled=true;

            this.goldtext = '00:00:00';

			if(((this.investText2*this.profitPercent) + this.investText2)>=0){
				// var txt=(this.balanceText+this.investText)-10000;
				var txt=(this.investText2*this.profitPercent) + this.investText2;

				if(txt==0){
					this.endGameText1=localisedStrings.endAlertNoAction[0];
					this.endGameText2=localisedStrings.endAlertNoAction[1];					
				}else{
					this.endGameText1=localisedStrings.endAlertProfit[0];
					this.endGameText2=this.currency(txt.toFixed(0))+' '+localisedStrings.endAlertProfit[1];					
				}

				this.offsetPosText=0;
			}else if(((this.investText2*this.profitPercent) + this.investText2)<0){
				// var txt=10000-(this.balanceText+this.investText);
				var txt=(this.investText2*this.profitPercent) + this.investText2;
				this.endGameText1=localisedStrings.endAlertLoss[0];
				this.endGameText2=localisedStrings.endAlertLoss[1];
				// this.endGameText2=localisedStrings.endAlertLoss[1]+' '+this.currency(txt.toFixed(0));
				this.offsetPosText=0;
			}else{
				this.endGameText1=localisedStrings.endAlertDraw[0];							
				this.endGameText2='';
				this.offsetPosText=20;
			}

			// console.log('endgame');
	        this.tween({endGameAlpha:0},3,{easing: ig.Tween.Easing.Sinusoidal.EaseIn, onComplete:function(){
	        	this.endGameAlpha=1;
	        	this.installnow.hide();
	        	this.installnow2.hide();
				ig.game.director.loadLevel(1);
	        }.bind(this)}).start();

        },

        alertInGameMessage:function(idx,status){
        	this.ingameStatus=true;
        	this.ingameText=localisedStrings.inGameMessage[idx];
        	this.ingameAlpha=1;
        	
        	if(status=='buy'){
        		this.btbuy.startFinger();
        	}else{
        		this.btsell.startFinger();
        	}

	        this.tween({ingameAlpha:0},2,{easing: ig.Tween.Easing.Sinusoidal.EaseIn, onComplete:function(){
	        	this.ingameStatus=false;
	        	this.ingameAlpha=1;
	        	this.ingameText='';
	        }.bind(this)}).start();

        },
        alertDown:function(){
        	this.buyStatus=true;
			if(ig.game.guidedExperience=='on'){

			}else{
		        this.tween({buyAlpha:0},2,{easing: ig.Tween.Easing.Sinusoidal.EaseIn, onComplete:function(){
		        	this.buyStatus=false;
		        	this.buyAlpha=1;
		        }.bind(this)}).start();
			}

        },
        alertDownGuided:function(){
			this.pauseDot = false;
	        this.tween({buyAlpha:0},1,{easing: ig.Tween.Easing.Sinusoidal.EaseIn, onComplete:function(){
	        	this.buyStatus=false;
	        	this.buyAlpha=1;
	        }.bind(this)}).start();        	
        },
        alertUp:function(){
        	this.sellStatus=true;
			if(ig.game.guidedExperience=='on'){

			}else{
		        this.tween({sellAlpha:0},2,{easing: ig.Tween.Easing.Sinusoidal.EaseIn, onComplete:function(){
		        	this.sellStatus=false;
		        	this.sellAlpha=1;
		        }.bind(this)}).start();
			}
        },
        alertUpGuided:function(){

			this.pauseDot = false;
	        this.tween({sellAlpha:0},1,{easing: ig.Tween.Easing.Sinusoidal.EaseIn, onComplete:function(){
	        	this.sellStatus=false;
	        	this.sellAlpha=1;
	        }.bind(this)}).start();
        },
        yPoint:function(lastPoint){
	        
	        if(this.trend=='down'){
		        var offsety=-15+Math.floor(Math.random() * 65); 
	        		return (lastPoint+offsety);
	        }else{
		        var offsety=15-(Math.floor(Math.random() * 65)); 
	        		return (lastPoint+offsety);
	        }

        },
        addNewPoint:function(){

            var newx=this.lastPoint.x+25;
	        var newy=this.yPoint(this.lastPoint.y);


	        if(newx>this.gatePos.x1+25){
	        	newx = this.lastPoint.x;
	        	newy = this.lastPoint.y;
	        	// this.endgameValue();
	        	// console.log('end1');
	        }

	        // console.log(this.graphlength);
	        this.graphlength +=25;

	        if(newy>this.valY.max) newy=this.valY.max;
	        if(newy<this.valY.min) newy=this.valY.min;

	        var newPoint={x:newx,y:newy};
            this.path.push(newPoint);
			this.pointlength +=1;
	        this.tween({lastPoint:{x:newx,y:newy}},0.4,{easing: ig.Tween.Easing.Sinusoidal.EaseOut, onComplete:function(){
	        }.bind(this)}).start();

	        if(this.pointlength==19 && this.screen=='P'){
	        	this.pauseDot = true;
	        	this.pageCount +=1;
	        	this.moveGraph();
	        }
        },			
        moveGraph:function(){
	        this.tween({offsetX:(this.pageCount*400)},1.5,{easing: ig.Tween.Easing.Sinusoidal.EaseOut, onComplete:function(){
	        	this.pauseDot=false;

	        	// this.pointlength =0;
	        	// if(this.pageCount>1){
		        // 	var temp=this.path;
		        // 	this.path=[];
		        // 	for(var i=temp.length-40;i<temp.length-1;i++){
		        // 		var newx=temp[i].x-this.offsetX;
		        // 		var newy=temp[i].y;
		        // 		this.path.push({x:newx,y:newy});
		        // 	}
		        // 	this.lastPoint={x:this.lastPoint.x-this.offsetX,y:this.lastPoint.y};
		        // 	this.offsetX=0;
		        // 	this.pageCount=0;
		        // }
	        }.bind(this)}).start();
        },
		draw:function()
		{
			this.parent();
			if(!ig.global.wm) {
				this.drawBackground();
				this.drawGrid();
				this.drawTopBanner();
				this.drawGate();
				this.drawPath();
				this.drawHorizontalLine();
				this.drawCheckPoint();
				if(this.buyStatus){
					this.drawBuyStatus();
				}
				if(this.sellStatus){
					this.drawSellStatus();
				}
				if(this.endGameStatus){
					this.drawEndGameStatus();
				}
				if(this.ingameStatus){
					this.drawIngameMessage();
				}

				if(this.screen=='P'){
					this.drawPanel();
				}else{
					this.drawPanelLanscape();
				}
				this.drawGoldText();
				this.drawGoldTextTop();
				this.drawBalanceText();
				this.drawInvestText();
				this.drawPriceText();


			}
		},
		drawEndGameStatus:function(){
            var context = ig.system.context;

            	// context.globalAlpha=this.endGameAlpha;
            	context.globalAlpha=1;
				context.lineWidth=2;
				context.fillStyle = "rgba(30,40,60,1)";
				context.strokeStyle = "rgba(255,255,255,1)";

				if(this.screen=='P'){				
	        		ig.game.roundRect(context, ig.system.width/2-250, ig.system.height/2-230, 500, 120, 4, true, true);
				}else{
	        		ig.game.roundRect(context, ig.system.width/2-250, ig.system.height/2-80, 500, 120, 4, true, true);
				}            

				context.fillStyle = "rgba(255,255,255,1)";
				context.font = "30px sans,arial";
				context.textAlign = "center";				

				if(this.screen=='P'){				
					context.fillText(this.endGameText1,ig.system.width/2, ig.system.height/2-185+this.offsetPosText);
				}else{
					context.fillText(this.endGameText1,ig.system.width/2, ig.system.height/2-35+this.offsetPosText);
				}

				if((this.investText2+this.balanceText)>=0){
					context.fillStyle = "rgba(80,255,60,1)";
				}else{
					context.fillStyle = "rgba(249,173,41,1)";					
				}

				if(this.screen=='P'){				
					context.fillText(this.endGameText2,ig.system.width/2, ig.system.height/2-140);
				}else{
					context.fillText(this.endGameText2,ig.system.width/2, ig.system.height/2+10);
				}
            	context.globalAlpha=1;

		},
		drawIngameMessage:function(){
            var context = ig.system.context;

            	context.globalAlpha=this.ingameAlpha;
				context.lineWidth=2;
				context.fillStyle = "rgba(30,40,60,1)";
				context.strokeStyle = "rgba(255,255,255,1)";
				if(this.screen=='P'){				
		        	ig.game.roundRect(context, ig.system.width/2-250, ig.system.height/2-230, 500, 90, 4, true, true);
            	}else{
		        	ig.game.roundRect(context, ig.system.width/2-250, ig.system.height/2-80, 500, 90, 4, true, true);
            	}	
            	
				context.fillStyle = "rgba(255,255,255,1)";
				context.font = "34px sans,arial";
				context.textAlign = "center";				
				if(this.screen=='P'){				
					context.fillText(this.ingameText,ig.system.width/2, ig.system.height/2-175);
            	}else{
					context.fillText(this.ingameText,ig.system.width/2, ig.system.height/2-25);
            	}	

            	context.globalAlpha=1;

		},
		drawBuyStatus:function(){
            var context = ig.system.context;

            	context.globalAlpha=this.buyAlpha;
				context.lineWidth=2;
				context.fillStyle = "rgba(30,40,60,1)";
				context.strokeStyle = "rgba(255,255,255,1)";
				
				if(this.screen=='P'){				
	        		ig.game.roundRect(context, ig.system.width/2-250, ig.system.height/2-230, 500, 90, 4, true, true);
            	}else{
	        		ig.game.roundRect(context, ig.system.width/2-250, ig.system.height/2-80, 500, 90, 4, true, true);
            	}	

				context.fillStyle = "rgba(255,255,255,1)";
				context.font = "30px sans,arial";
				context.textAlign = "center";				
				if(this.screen=='P'){				
					context.fillText(localisedStrings.buyAlert,ig.system.width/2, ig.system.height/2-175);
            	}else{
					context.fillText(localisedStrings.buyAlert,ig.system.width/2, ig.system.height/2-25);
            	}	

            	context.globalAlpha=1;

		},
		drawSellStatus:function(){
            var context = ig.system.context;

            	context.globalAlpha=this.sellAlpha;
				context.lineWidth=2;
				context.fillStyle = "rgba(30,40,60,1)";
				context.strokeStyle = "rgba(255,255,255,1)";
				if(this.screen=='P'){				
		        	ig.game.roundRect(context, ig.system.width/2-250, ig.system.height/2-230, 500, 90, 4, true, true);
		        }else{
		        	ig.game.roundRect(context, ig.system.width/2-250, ig.system.height/2-80, 500, 90, 4, true, true);
		        }
				context.fillStyle = "rgba(255,255,255,1)";
				context.font = "30px sans,arial";
				context.textAlign = "center";				
				if(this.screen=='P'){				
					context.fillText(localisedStrings.sellAlert,ig.system.width/2, ig.system.height/2-175);
		        }else{
					context.fillText(localisedStrings.sellAlert,ig.system.width/2, ig.system.height/2-25);
		        }

            	context.globalAlpha=1;
		},
		currency:function(val){
			var	number_string = val.toString(),
				a 	= number_string.length % 3,
				b 	= number_string.substr(a).match(/\d{3}/g),
				result 	= number_string.substr(0, a);
					
				if (b) {
					separator = a ? '.' : '';
					result += separator + b.join('.');
				}				
			return '$'+result;	
		},
		drawPriceText:function(){
            var context = ig.system.context;
				context.fillStyle = "rgba(255,255,255,1)";
				context.textAlign = "center";				
				if(this.screen=='P'){
					context.font = "24px sans,arial";
					context.fillText(this.currency(this.priceText.toFixed(0)),400,745);
				}else{
					context.font = "22px sans,arial";
					context.fillText(this.currency(this.priceText.toFixed(0)),535,ig.system.height-25);
				}
		},

		drawInvestText:function(){
            var context = ig.system.context;
				context.fillStyle = "rgba(255,255,255,1)";
				context.textAlign = "center";				
				if(this.screen=='P'){
					context.font = "24px sans,arial";
					context.fillText(this.currency(this.investText2.toFixed(0)),140,745);
				}else{
					context.font = "22px sans,arial";
					context.fillText(this.currency(this.investText2.toFixed(0)),385,ig.system.height-25);
				}
		},

		drawBalanceText:function(){
            var context = ig.system.context;
				context.fillStyle = "rgba(241,194,53,1)";
				context.textAlign = "center";

				if(this.screen=='P'){
					context.font = "24px sans,arial";
					context.fillText(this.currency(this.balanceText.toFixed(0)),400,655);
				}else{
					context.font = "22px sans,arial";
					context.fillText(this.currency(this.balanceText.toFixed(0)),235,ig.system.height-25);
				}

		},
		drawGoldText:function(){
            var context = ig.system.context;
				context.fillStyle = "rgba(255,255,255,1)";
				context.textAlign = "center";				
				if(this.screen=='P'){
					context.font = "24px sans,arial";
					context.fillText(this.goldtext,140,655);
				}else{
					context.font = "22px sans,arial";
					context.fillText(this.goldtext,85,ig.system.height-25);					
				}
		},

		drawGoldTextTop:function(){
            var context = ig.system.context;
				context.fillStyle = "rgba(255,255,255,1)";
				context.textAlign = "center";				
				context.font = "22px sans,arial";
				if(this.screen=='P'){
					context.fillText(ig.game.trade+' : '+this.investCount+' '+ig.game.unit,ig.system.width/2,ig.system.height/2+90);
				}else{
					context.fillText(ig.game.trade+' : '+this.investCount+' '+ig.game.unit,ig.system.width/2,ig.system.height-95);					
				}
		},

        drawBackground: function() {
            var context = ig.system.context;
			if(this.screen=='L'){
	            var fr = spriteSheets[this.gamebg.spriteSheet].frames[this.gamebg.spriteID].frame;
	            context.drawImage(
	                this.gamebg.spriteImage.data,
	                0,
	                300,
	                fr.w,
	                400,
	                0,
	                0,
	                ig.system.width,
	                ig.system.height
	            );      

			}else{

	            var fr = spriteSheets[this.gamebg.spriteSheet].frames[this.gamebg.spriteID].frame;
	            context.drawImage(
	                this.gamebg.spriteImage.data,
	                0,
	                0,
	                fr.w,
	                fr.h,
	                0,
	                -50,
	                ig.system.width,
	                ig.system.height
	            );      
            }          
        },
        drawTopBanner:function(){
            var context = ig.system.context;
            
			context.fillStyle = "rgba(46,56,79,1)";

			if(this.screen=='P'){
				context.fillRect(0,0,ig.system.width,70);		
				var scale=1;		
				var left=60;
			}else{
				context.fillRect(0,0,ig.system.width,50);
				var scale=0.7;		
				var left=ig.system.width/2-180;
			}

            var fr = spriteSheets[this.toplogo.spriteSheet].frames[this.toplogo.spriteID].frame;
            context.drawImage(
                this.toplogo.spriteImage.data,
                0,
                0,
                fr.w,
                fr.h,
                left,
                8,
                fr.w*scale,
                fr.h*scale
            );                

        },

        drawGrid:function(){
            var context = ig.system.context;            
            context.globalAlpha=0.6;
            var fr = spriteSheets[this.grid.spriteSheet].frames[this.grid.spriteID].frame;

            context.drawImage(
                this.grid.spriteImage.data,
				0, 0, 
				fr.w, 
				fr.h,
				0, 
				48,
				fr.w,
				fr.h
            );

            context.drawImage(
                this.grid.spriteImage.data,
				0, 0, 
				fr.w, 
				fr.h,
				fr.w, 
				48,
				fr.w,
				fr.h
            );
            context.globalAlpha=1;
   
        },
		drawPanelLanscape:function(){

            var context = ig.system.context;            
			context.fillStyle = "rgba(34,40,61,1)";
			context.fillRect(0,ig.system.height-85,ig.system.width,85);		

			context.fillStyle = "rgba(103,111,139,1)";
			context.textAlign = "left";				
			context.font = "16px arial";
			context.fillText('Time Left',20,ig.system.height-60);

			context.fillStyle = "rgba(28,35,55,1)";
	        ig.game.roundRect(context, 20, ig.system.height-50,130,35, 5, true, false);


			context.fillStyle = "rgba(103,111,139,1)";
			context.textAlign = "left";				
			context.font = "16px arial";
			context.fillText('Balance',170,ig.system.height-60);

			context.fillStyle = "rgba(28,35,55,1)";
	        ig.game.roundRect(context, 170, ig.system.height-50,130,35, 5, true, false);


			context.fillStyle = "rgba(103,111,139,1)";
			context.textAlign = "left";				
			context.font = "16px arial";
			context.fillText('Investment',320,ig.system.height-60);

			context.fillStyle = "rgba(28,35,55,1)";
	        ig.game.roundRect(context, 320, ig.system.height-50,130,35, 5, true, false);

			context.fillStyle = "rgba(103,111,139,1)";
			context.textAlign = "left";				
			context.font = "16px arial";
			context.fillText('Current Price',470,ig.system.height-60);

			context.fillStyle = "rgba(28,35,55,1)";
	        ig.game.roundRect(context, 470, ig.system.height-50,130,35, 5, true, false);

		},
        drawPanel:function(){
            var context = ig.system.context;            
            var fr = spriteSheets[this.panel.spriteSheet].frames[this.panel.spriteID].frame;
            context.drawImage(
                this.panel.spriteImage.data,
				0, 0, 
				fr.w, 
				fr.h,
				0, 
				ig.system.height-fr.h-60,
				fr.w,
				fr.h
            );   
        },   
        checkOverlap:function(x,y,i,status){
			var res=false;
			if(this.labelPoint[i-1]){
				var offset=0;
				for(var j=1;j<=i;j++){

					var x1=this.labelPoint[j-1].x;
					var y1=this.labelPoint[j-1].y;
					var x2=x1+50;
					var y2=y1+20;


					if(((x>=x1 && x<=x2) && (y>=y1 && y<=y2)) ||
					  ((x>=x1 && x<=x2) && (y+20>=y1 && y+20<=y2)))	
					{
						res=true;
						offset +=30;
					}
				}
			}

			if(res){
				if(status==0){
					return {x:x,y:y-offset,price:this.currency(this.priceText.toFixed(0))};
				}else{
					return {x:x,y:y+offset,price:this.currency(this.priceText.toFixed(0))};
				}
			}else{
				if(status==0){
					return {x:x,y:y,price:this.currency(this.priceText.toFixed(0))};
				}else{
					return {x:x,y:y,price:this.currency(this.priceText.toFixed(0))};
				}
			}


        },

        createBuyPoint:function(){
			this.checkPoint.push({x:this.lastPoint.x,y:this.lastPoint.y,status:'buy'});

			var posx=this.lastPoint.x;
			var posy=this.lastPoint.y;
			var i=this.checkPoint.length-1;
			var newspot=this.checkOverlap(posx,posy,i,0);
			this.labelPoint.push(newspot);

        },
        cerateSellPoint:function(){
			this.checkPoint.push({x:this.lastPoint.x,y:this.lastPoint.y,status:'sell'});

			var posx=this.lastPoint.x;
			var posy=this.lastPoint.y;
			var i=this.checkPoint.length-1;
			var newspot=this.checkOverlap(posx,posy,i,1);
			this.labelPoint.push(newspot);

        },
        drawCheckPoint:function(){
            var context = ig.system.context;            


			for(var i=0;i<this.checkPoint.length;i++){

				var posx=this.checkPoint[i].x-this.offsetX+this.offsetFixposX;
				var posy=this.checkPoint[i].y;

				if(this.checkPoint[i].status=='buy'){
					context.strokeStyle="rgba(60,205,30,1)";

					var resX=this.labelPoint[i].x-25;	
					var resY=this.labelPoint[i].y-40;	
					if(resY<this.valY.min) resY=this.valY.min;	

					context.lineWidth=2;
		            context.beginPath();
					context.moveTo(posx,posy);
					context.lineTo(resX+25-this.offsetX+this.offsetFixposX,resY);
		            context.stroke();
		            context.closePath();

				}else{
					context.strokeStyle="rgba(215,60,99,1)";					

					var resX=this.labelPoint[i].x-25;	
					var resY=this.labelPoint[i].y+40;	
					if(resY>this.valY.max) resY=this.valY.max;	

					context.lineWidth=2;
		            context.beginPath();
					context.moveTo(posx,posy);
					context.lineTo(resX+25-this.offsetX+this.offsetFixposX,resY);
		            context.stroke();
		            context.closePath();
				}
			}



			for(var i=0;i<this.checkPoint.length;i++){

				if(this.checkPoint[i].status=='buy'){
					context.strokeStyle="rgba(60,205,30,1)";
				}else{
					context.strokeStyle="rgba(215,60,99,1)";					
				}

				var posx=this.checkPoint[i].x-this.offsetX+this.offsetFixposX;
				var posy=this.checkPoint[i].y;

				context.lineWidth=5;
				context.beginPath();
				context.arc( 
					posx, 
					posy,
					3, 0, 2 * Math.PI, false
				);
				context.stroke();
				context.closePath();			




				if(this.checkPoint[i].status=='buy'){


					context.lineWidth=1;
					context.strokeStyle="rgba(60,205,30,1)";
					context.beginPath();
					context.moveTo(
						0, 
						posy
					);						

					context.lineTo(
						ig.system.width, 
						posy
					);
					context.stroke();
					context.closePath();	


					context.fillStyle="rgba(60,205,30,1)";

					var resX=this.labelPoint[i].x-25;	
					var resY=this.labelPoint[i].y-40;	
					if(resY<this.valY.min) resY=this.valY.min;	


					context.lineWidth=2;
					context.strokeStyle="rgba(255,255,255,1)";
			        ig.game.roundRect(context, resX-this.offsetX+this.offsetFixposX, resY, 50, 20, 5, true, true);


					context.fillStyle = "rgba(255,255,255,1)";
					context.textAlign = "center";				
					context.font = "12px sans,arial";
					context.fillText(this.labelPoint[i].price,resX-this.offsetX+this.offsetFixposX+25,resY+15);

				}else{


					context.lineWidth=1;
					context.strokeStyle="rgba(215,60,99,1)";
					context.beginPath();
					context.moveTo(
						0, 
						posy
					);						

					context.lineTo(
						ig.system.width, 
						posy
					);
					context.stroke();
					context.closePath();	


					context.fillStyle="rgba(215,60,99,1)";			

					var resX=this.labelPoint[i].x-25;	
					var resY=this.labelPoint[i].y+40;	
					if(resY>this.valY.max) resY=this.valY.max;	

					context.lineWidth=2;
					context.strokeStyle="rgba(255,255,255,1)";
			        ig.game.roundRect(context, resX-this.offsetX+this.offsetFixposX, resY, 50, 20, 5, true, true);
					context.fillStyle = "rgba(255,255,255,1)";
					context.textAlign = "center";				
					context.font = "12px sans,arial";
					context.fillText(this.labelPoint[i].price,resX-this.offsetX+this.offsetFixposX+25,resY+15);
				}
			}

        },    
    	drawPath:function(){

            var context = ig.system.context;            

			for(var i=1;i<this.path.length-1 && this.path[i];i++){


                var gradient1=ig.system.context.createLinearGradient(this.path[i-1].x-this.offsetX+this.offsetFixposX,this.valY.min,this.path[i].x-this.offsetX+this.offsetFixposX, this.valY.max+50);
                    gradient1.addColorStop(0,"rgba(23,125,174,0.6)");
                    gradient1.addColorStop(1,"rgba(23,125,174,0)");


					context.fillStyle = gradient1;
		            context.beginPath();
					context.moveTo(this.path[i-1].x-this.offsetX+this.offsetFixposX,this.path[i-1].y);
					context.lineTo(this.path[i].x-this.offsetX+this.offsetFixposX, this.path[i].y);
					context.lineTo(this.path[i].x-this.offsetX+this.offsetFixposX, this.valY.max+10);
					context.lineTo(this.path[i-1].x-this.offsetX+this.offsetFixposX, this.valY.max+10);
					context.lineTo(this.path[i-1].x-this.offsetX+this.offsetFixposX,this.path[i-1].y);
		            context.fill();
		            context.closePath();

					context.lineWidth=3;
					context.strokeStyle="rgba(23,125,174,1)";
					context.beginPath();
					context.moveTo(
						this.path[i-1].x-this.offsetX+this.offsetFixposX, 
						this.path[i-1].y
					);						

					context.lineTo(
						this.path[i].x-this.offsetX+this.offsetFixposX, 
						this.path[i].y
					);
					context.stroke();
					context.closePath();										

			}

			if(i>1){

                var gradient2=ig.system.context.createLinearGradient(this.path[i-1].x-this.offsetX+this.offsetFixposX,this.valY.min,this.path[i].x-this.offsetX+this.offsetFixposX, this.valY.max+50);
                    gradient2.addColorStop(0,"rgba(23,125,174,0.6)");
                    gradient2.addColorStop(1,"rgba(23,125,174,0)");

				context.fillStyle = gradient2;
	            context.beginPath();
				context.moveTo(this.path[this.path.length-2].x-this.offsetX+this.offsetFixposX,this.path[this.path.length-2].y);
				context.lineTo(this.lastPoint.x-this.offsetX+this.offsetFixposX, this.lastPoint.y);
				context.lineTo(this.lastPoint.x-this.offsetX+this.offsetFixposX, this.valY.max+10);
				context.lineTo(this.path[this.path.length-2].x-this.offsetX+this.offsetFixposX, this.valY.max+10);
				context.lineTo(this.path[this.path.length-2].x-this.offsetX+this.offsetFixposX,this.path[this.path.length-2].y);
	            context.fill();
	            context.closePath();


				context.lineWidth=3;
				context.strokeStyle="rgba(23,125,174,1)";
				context.beginPath();
				context.moveTo(
					this.path[this.path.length-2].x-this.offsetX+this.offsetFixposX, 
					this.path[this.path.length-2].y
				);						

				context.lineTo(
					this.lastPoint.x-this.offsetX+this.offsetFixposX, 
					this.lastPoint.y
				);
				context.stroke();
				context.closePath();										


				var context =ig.system.context; 
				context.strokeStyle="rgba(255,255,255,0.2)";
				context.lineWidth=this.line;
				context.beginPath();
				context.arc( 
					this.lastPoint.x-this.offsetX+this.offsetFixposX, 
					this.lastPoint.y,
					this.radius2, 0, 2 * Math.PI, false
				);
				context.stroke();
				context.closePath();			


				context.fillStyle = "rgba(40,107,206,1)";
				context.fillRect(this.lastPoint.x-this.offsetX+this.offsetFixposX-4,this.lastPoint.y-6,8,8);
				context.strokeStyle="rgba(255,255,255,1)";
				context.lineWidth=4;
				context.beginPath();
				context.arc( 
					this.lastPoint.x-this.offsetX+this.offsetFixposX, 
					this.lastPoint.y,
					5, 0, 2 * Math.PI, false
				);
				context.stroke();
				context.closePath();			


			}

    	},
		drawHorizontalLine:function(){
            var context = ig.system.context;            

				context.lineWidth=1;
				context.strokeStyle="rgba(23,125,174,1)";
				context.fillStyle = "rgba(55,136,229,0.5)";
		        ig.game.roundRect(context, ig.system.width-60, this.lastPoint.y-10, 55, 20, 10, true, true);

				context.lineWidth=1;
				context.strokeStyle="rgba(23,125,174,1)";
				context.beginPath();
				context.moveTo(
					0, 
					this.lastPoint.y
				);						

				context.lineTo(
					ig.system.width-60, 
					this.lastPoint.y
				);
				context.stroke();
				context.closePath();	

				context.fillStyle = "rgba(255,255,255,1)";
				context.textAlign = "center";				
				context.font = "12px sans,arial";
				context.fillText(this.currency(this.priceText.toFixed(0)),ig.system.width-33,this.lastPoint.y+5);

		},
    	drawGate:function(){
            var context = ig.system.context;            

                var gradient2=ig.system.context.createLinearGradient(this.gatePos.x1-this.offsetX+this.offsetFixposX,this.gatePos.y1,ig.system.width, this.gatePos.y1);
                    gradient2.addColorStop(0,"rgba(119,121,57,0.2)");
                    gradient2.addColorStop(1,"rgba(119,121,57,0)");

				context.fillStyle = gradient2;
	            context.beginPath();
				context.moveTo(this.gatePos.x1-this.offsetX+this.offsetFixposX-1,this.gatePos.y1);
				context.lineTo(ig.system.width, this.gatePos.y1);
				context.lineTo(ig.system.width, this.gatePos.y2);
				context.lineTo(this.gatePos.x2-this.offsetX+this.offsetFixposX-1, this.gatePos.y2);
				context.lineTo(this.gatePos.x1-this.offsetX+this.offsetFixposX-1,this.gatePos.y1);
	            context.fill();
	            context.closePath();



				context.lineWidth=2;
				context.strokeStyle="rgba(119,121,57,1)";
				context.beginPath();
				context.moveTo(
					this.gatePos.x1-this.offsetX+this.offsetFixposX, 
					this.gatePos.y1
				);						

				context.lineTo(
					this.gatePos.x2-this.offsetX+this.offsetFixposX, 
					this.gatePos.y2
				);
				context.stroke();
				context.closePath();	



				context.fillStyle = "rgba(119,121,57,1)";
				context.textAlign = "center";				
				context.font = "14px sans,arial";

				var x=this.gatePos.x1-this.offsetX+this.offsetFixposX-10;
				var y=this.gatePos.y1+30

				context.save();
				context.translate(x, y);
				context.rotate(-Math.PI / 2);

				context.fillText('Finish',0,5);

				context.restore();

    	},
    	line:4,
    	radius2:5,
		goBig:function(){
			  if(this.endGameStatus) return;
	          this.tween({radius2:10,line:15},0.7,{easing: ig.Tween.Easing.Linear.EaseNone, onComplete:function(){
	          		this.goSmall();
	          }.bind(this)}).start();
		},
		goSmall:function(){
			  if(this.endGameStatus) return;
	          this.tween({radius2:5,line:4},0.7,{easing: ig.Tween.Easing.Linear.EaseNone, onComplete:function(){
	                setTimeout(function(){ 
		          		this.goBig();
	                }.bind(this), 800);
	          }.bind(this)}).start();
		},


    });

});