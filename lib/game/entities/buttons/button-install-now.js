ig.module('game.entities.buttons.button-install-now')
.requires(
	'game.entities.buttons.button'
)
.defines(function() {
	EntityButtonInstallNow = EntityButton.extend({
		spriteImage: false,
		spriteSheet: false,
		spriteID: false,
		text: "InstallNow",
		
		type: ig.Entity.TYPE.B,
		gravityFactor: 0,
		alpha: 1,
		offset: {x: 0, y: 0},
		scale: {x: 1, y: 1},
		anchor: {x: 0.5, y: 0.5},
		visible: true,

        installhead:{
            spriteImage: new ig.Image(spriteSheets.installhead.meta.image),
            spriteID:"installhead.png",
            spriteSheet:"installhead"
        },

        installbottom:{
            spriteImage: new ig.Image(spriteSheets.installbottom.meta.image),
            spriteID:"installbottom.png",
            spriteSheet:"installbottom"
        },
        appstore:{
            spriteImage: new ig.Image(spriteSheets.appstore.meta.image),
            spriteID:"appstore.png",
            spriteSheet:"appstore"
        },

        playstore:{
            spriteImage: new ig.Image(spriteSheets.playstore.meta.image),
            spriteID:"playstore.png",
            spriteSheet:"playstore"
        },
        zIndex:50,
        updown:0,
		init: function(x,y,settings) {
			this.parent(x,y,settings);			
			ig.game.sortEntitiesDeferred();
		},

		reset: function(x,y,settings) {
			this.parent(x,y,settings);		
			this.updateSize();
			this.updateEntityBox();
			
			if(!this.clickableDiv) {
				this.createClickableDivLayer();
			}
		},
		
		createClickableDivLayer: function() {
			this.clickableDiv = ig.game.spawnEntity(
				EntityClickableDivLayer, 
				this.pos.x, this.pos.y, 
				{
                    cssZIndex: this.zIndex+10,
					size: { x: this.size.x, y: this.size.y },
					div_layer_name: this.div_layer_name,
					onClickFunction: this.released.bind(this)
				}
			);
			ig.game.sortEntitiesDeferred();
		},
		
		updateSize: function() {
			var size = this.getSize();
			
			this._SIZE = {
				x: size.x,
				y: size.y
			};
		},

		updateOnSizeChange: function() {
			if(this.clickableDiv) {
				this.clickableDiv.pos = {
					x: this.pos.x,
					y: this.pos.y
				};
				this.clickableDiv.size = {
					x: this.size.x,
					y: this.size.y
				};
				this.clickableDiv.updateCSS();
			}
		},

		updateOnOrientationChange: function() {
			// this function is fired once whenever orientation changed (portrait <=> landscape)
			// so you can re-locate or re-size here
			this.parent();
			
			this._POS.x = this.boxPos.x+0.5*this.boxSize.x;
			this._POS.y = this.boxPos.y+0.5*this.boxSize.y;
			
			this.updateEntityBox();
			this.updateOnSizeChange();
		},
		
		show: function() {
			this.visible = true;
			if(this.clickableDiv) {
				this.clickableDiv.show();
			}
		},
		
		hide: function() {
			this.visible = false;
			if(this.clickableDiv) {
				this.clickableDiv.hide();
			}
		},

		clicked: function() {
			this.parent();
		},
		
		clicking: function() {
			this.parent();
		},

        released: function() {
            this.parent();
            MJS.ad.openClickUrl();
        },
		
		releasedOutside: function() {
			this.parent();
		},
		
		leave: function() {
			this.parent();
		},
		
		over: function() {
			this.parent();
		},
		
		update: function() {
			this.parent();
		},
		
		getSize: function() {
			return {x: this.boxSize.x, y: this.boxSize.y};
		},
		
		
	   /**
	    * Draws a rounded rectangle using the current state of the canvas. 
	    * If you omit the last three params, it will draw a rectangle 
	    * outline with a 5 pixel border radius 
	    * @param {CanvasRenderingContext2D} ctx
	    * @param {Number} x The top left x coordinate
	    * @param {Number} y The top left y coordinate 
	    * @param {Number} width The width of the rectangle 
	    * @param {Number} height The height of the rectangle
	    * @param {Number} radius The corner radius. Defaults to 5;
	    * @param {Boolean} fill Whether to fill the rectangle. Defaults to false.
	    * @param {Boolean} stroke Whether to stroke the rectangle. Defaults to true.
	    */
	   roundRect: function(ctx, x, y, width, height, radius, fill, stroke) {
			if (typeof stroke == "undefined" ) {
			  stroke = true;
			}
			if (typeof radius === "undefined") {
			  radius = 5;
			}
			ctx.beginPath();
			ctx.moveTo(x + radius, y);
			ctx.lineTo(x + width - radius, y);
			ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
			ctx.lineTo(x + width, y + height - radius);
			ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
			ctx.lineTo(x + radius, y + height);
			ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
			ctx.lineTo(x, y + radius);
			ctx.quadraticCurveTo(x, y, x + radius, y);
			ctx.closePath();
			if (stroke) {
			  ctx.stroke();
			}
			if (fill) {
			  ctx.fill();
			}        
	   },
		
		drawBackground: function() {
			var context = ig.system.context;
			
			if(this.updown==0){

				if(this.endcard){

						if(ig.ua.mobile){
							if(ig.ua.iOS){
					            var fr = spriteSheets[this.appstore.spriteSheet].frames[this.appstore.spriteID].frame;
					            context.drawImage(
					                this.appstore.spriteImage.data,
									fr.x, fr.y, 
									fr.w, fr.h,
									this.pos.x, 
									this.pos.y,
									this.size.x,
									this.size.y
					            );
							}else{
					            var fr = spriteSheets[this.playstore.spriteSheet].frames[this.playstore.spriteID].frame;
					            context.drawImage(
					                this.playstore.spriteImage.data,
									fr.x, fr.y, 
									fr.w, fr.h,
									this.pos.x, 
									this.pos.y,
									this.size.x,
									this.size.y
					            );
							}

						}else{
				            var fr = spriteSheets[this.installhead.spriteSheet].frames[this.installhead.spriteID].frame;
				            context.drawImage(
				                this.installhead.spriteImage.data,
								fr.x, fr.y, 
								fr.w, fr.h,
								this.pos.x, 
								this.pos.y,
								this.size.x,
								this.size.y
				            );
				            var scale=this.size.x/fr.w;
							context.fillStyle = "rgba(255,255,255,1)";
							context.font = (scale*26)+"px sans,arial";
							context.textAlign = "center";				
							context.fillText(this.caption, this.pos.x+this.size.x/2,this.pos.y+this.size.y/2+(scale*10));

						}
				}else{


			            var fr = spriteSheets[this.installhead.spriteSheet].frames[this.installhead.spriteID].frame;
			            context.drawImage(
			                this.installhead.spriteImage.data,
							fr.x, fr.y, 
							fr.w, fr.h,
							this.pos.x, 
							this.pos.y,
							this.size.x,
							this.size.y
			            );
			            var scale=this.size.x/fr.w;
						context.fillStyle = "rgba(255,255,255,1)";
						context.font = (scale*26)+"px sans,arial";
						context.textAlign = "center";				
						context.fillText(this.caption, this.pos.x+this.size.x/2,this.pos.y+this.size.y/2+(scale*10));					
				}




            }else if(this.updown==1){
	            var fr = spriteSheets[this.installbottom.spriteSheet].frames[this.installbottom.spriteID].frame;
	            context.drawImage(
	                this.installbottom.spriteImage.data,
					fr.x, fr.y, 
					fr.w, fr.h,
					this.pos.x, 
					this.pos.y,
					this.size.x,
					this.size.y
	            );

            }                

		},
		

		draw: function() {
			this.parent();
			
			if(this.visible) {
				this.drawBackground();
			}
			
		}
	});
	
});