ig.module('game.entities.buttons.button-sell')
.requires(
	'game.entities.buttons.button'
)
.defines(function() {
	EntityButtonSell = EntityButton.extend({		
		type: ig.Entity.TYPE.B,
		gravityFactor: 0,
		alpha: 1,
		size: {x: 254, y: 104},
		offset: {x: 0, y: 0},
		scale: {x: 0.91, y: 0.8},
		anchor: {x: 0.5, y: 0.5},
		locked: false,
        btsell:{
            spriteImage: new ig.Image(spriteSheets.btsell.meta.image),
            spriteID:"btsell.png",
            spriteSheet:"btsell"
        },
        finger:{
            spriteImage: new ig.Image(spriteSheets.finger.meta.image),
            spriteID:"finger.png",
            spriteSheet:"finger"
        },
        offsetY:0,
        fingerStatus:false,
        fingerScale:0.6,
        fingerTime:3,
        disabled:false,
        buylimitStatus:false,
        buylimitAlpha:1,
        line1:'',
        line2:'',
        line1Offset:0,
        status1:true,
        status2:true,
		init: function(x,y,settings) {
			this.parent(x,y,settings);			
			ig.game.sortEntitiesDeferred();

			this.posx=this.pos.x;
            this.posy=this.pos.y;

            this.sizex=this.size.x;
            this.sizey=this.size.y;

            setTimeout(function(){ 
				if(ig.game.guidedExperience=='on'){
					this.disabled=true;
				}else{
					this.disabled=false;
				}
            }.bind(this), 100);

		},
		startFinger:function(){
			this.fingerStatus=true;
			if(ig.game.guidedExperience!='on'){
	          this.tween({fingerTime:3},4,{easing: ig.Tween.Easing.Linear.EaseNone, onComplete:function(){
	          		this.fingerStatus=false;
	          }.bind(this)}).start();
	        }  
			this.goBig();
		},
		updateOnOrientationChange: function() {
			this.parent();			
		},
		clicked: function() {
			this.parent();
			if(this.disabled) return;
  		    if(this.parrent.endGameStatus) return;
			// if(this.parrent.investText<this.parrent.priceText) return;
			if(this.parrent.balanceText<this.parrent.priceText) return;
			this.offsetY=5;			
		},		
		clicking: function() {
			this.parent();
			if(this.disabled) return;
  		    if(this.parrent.endGameStatus) return;
			// if(this.parrent.investText<this.parrent.priceText) return;
			if(this.parrent.balanceText<this.parrent.priceText) return;
			this.offsetY=5;			
		},
		released: function() {
			this.parent();
			this.offsetY=0;			
			if(this.disabled) return;

			if(this.parrent.balanceText<this.parrent.priceText && this.status2){
				this.fingerStatus=false;
				this.status2 = false;
				this.buylimitStatus=true;
	        	this.buylimitAlpha=1;
				this.line1=localisedStrings.buyFund[0];
				this.line2="";
				this.line1Offset=10;
		        this.tween({buylimitAlpha:0},3,{easing: ig.Tween.Easing.Sinusoidal.EaseIn, onComplete:function(){
		        	this.buylimitStatus=false;
		        	this.buylimitAlpha=1;
		        }.bind(this)}).start();
				return;	
			} 
			if(this.parrent.balanceText<this.parrent.priceText) return;


  		    if(this.parrent.endGameStatus) return;
			// if(this.parrent.investText<this.parrent.priceText) return;

			// this.parrent.balanceText +=this.parrent.priceText;
			// this.parrent.investCount -=1;

			this.parrent.balanceText -=this.parrent.priceText;

			this.parrent.investText2 +=this.parrent.priceText;

			this.parrent.investCount +=1;

			this.fingerStatus=false;
			this.parrent.cerateSellPoint();
			if(ig.game.guidedExperience=='on'){
				this.parrent.alertUpGuided();
			}
			// this.parrent.checkPoint.push({x:this.parrent.lastPoint.x,y:this.parrent.lastPoint.y,status:'sell'});
			// console.log(this.parrent.checkPoint);
		},		
		leave:function(){
			this.offsetY=0;
		},
		update: function() {
			this.parent();
            this.pos.x=this.posx;
            this.pos.y=this.posy;

            this.size.x=this.sizex;
            this.size.y=this.sizey;
		},		
		goBig:function(){
			  if(!this.fingerStatus) return;
	          this.tween({fingerScale:1.2},0.3,{easing: ig.Tween.Easing.Linear.EaseNone, onComplete:function(){
	          		this.goSmall();
	          }.bind(this)}).start();
		},	
		goSmall:function(){
  			  if(!this.fingerStatus) return;
	          this.tween({fingerScale:0.6},0.3,{easing: ig.Tween.Easing.Linear.EaseNone, onComplete:function(){
	          		this.goBig();
	          }.bind(this)}).start();
		},
		drawSprite: function() {
			var context = ig.system.context;
            var fr = spriteSheets[this.btsell.spriteSheet].frames[this.btsell.spriteID].frame;
            context.drawImage(
                this.btsell.spriteImage.data,
				0, 
				0, 
				fr.w, 
				fr.h,
				this.pos.x, 
				this.pos.y+this.offsetY,
				this.size.x*this.scale.x,
				this.size.y*this.scale.y
            );   
		},		
		draw: function() {
			this.parent();			
			this.drawSprite();
			if(this.fingerStatus){
				this.drawFinger();			
			}			
			if(this.buylimitStatus){
				this.drawBuyLimit();
			}
		},
		drawBuyLimit:function(){
            var context = ig.system.context;

            	context.globalAlpha=this.buylimitAlpha;
				context.lineWidth=2;
				context.fillStyle = "rgba(30,40,60,1)";
				context.strokeStyle = "rgba(255,255,255,1)";
	            if(this.parrent.screen=='P'){
		        	ig.game.roundRect(context, 15, ig.system.height/2+40, 200, 50, 4, true, true);
	        	}else{
	        		ig.game.roundRect(context, ig.system.width-280, ig.system.height-160, 200, 50, 4, true, true);
	        	}

				context.fillStyle = "rgba(255,70,70,1)";
				context.font = "18px sans,arial";
				context.textAlign = "center";	
							
	            if(this.parrent.screen=='P'){
					context.fillText(this.line1,115, ig.system.height/2+63+this.line1Offset);
					context.fillText(this.line2,115, ig.system.height/2+82);
	        	}else{
					context.fillText(this.line1,ig.system.width-180, ig.system.height-137+this.line1Offset);
					context.fillText(this.line2,ig.system.width-180, ig.system.height-118);
	        	}

            	context.globalAlpha=1;			
		},

		drawFinger:function(){
			var context = ig.system.context;
            var fr = spriteSheets[this.finger.spriteSheet].frames[this.finger.spriteID].frame;
            if(this.parrent.screen=='P'){
            	var yOffset=fr.h*this.fingerScale/2;
            }else{
            	var yOffset=fr.h*this.fingerScale;
            }
            context.drawImage(
                this.finger.spriteImage.data,
				0, 
				0, 
				fr.w, 
				fr.h,
				this.pos.x+this.size.x/2-(fr.w*this.fingerScale/2), 
				this.pos.y+this.size.y/2-yOffset+50,
				fr.w*this.fingerScale,
				fr.h*this.fingerScale
            );   			
		},

	});
	
});