ig.module('game.entities.buttons.button')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityButton = ig.Entity.extend({
        zIndex: 2500,
		type: ig.Entity.TYPE.B,
		gravityFactor: 0,
		alpha: 1,
		offset: {x: 0, y: 0},
		scale: {x: 1, y: 1},
		anchor: {x: 0.5, y: 0.5},
		spriteSheet: "gameScreen",
		spriteID: "button-black",

		init: function(x,y,settings) {
			this.parent(x,y,settings);
			
			if(!ig.global.wm)
			{
				this.reset();
				this.updateEntityBox();
			
				ig.game.sortEntitiesDeferred();
			}
		},
		
		updateOnOrientationChange: function() {
			this._POS = {
				x: this.pos.x,
				y: this.pos.y
			};
		
			this._SIZE = {
				x: this.size.x,
				y: this.size.y
			};
		},
		
		reset: function() {
			if(!ig.global.wm)
			{
				this.updateOnOrientationChange();
			}
		},

		updateEntityBox: function() {			
			this.size.x = this.scale.x*this._SIZE.x;
			this.size.y = this.scale.y*this._SIZE.y;
			
			this.pos.x = this._POS.x-this.anchor.x*this.size.x;
			this.pos.y = this._POS.y-this.anchor.y*this.size.y;
		},
		

		clicked: function() {
			
		},
		
		clicking: function() {
			
		},

		released: function() {
			
		},
		
		releasedOutside: function() {
			
		},
		
		leave: function() {
			
		},
		
		over: function() {
			
		},
		
		update: function() {
			this.parent();
			
			if(!ig.global.wm)
			{
				this.updateEntityBox();
			}
		},
		

		draw: function() {
			this.parent();
		}
	});
	
});