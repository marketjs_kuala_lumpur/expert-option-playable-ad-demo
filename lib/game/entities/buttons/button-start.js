ig.module('game.entities.buttons.button-start')
.requires(
	'game.entities.buttons.button'
)
.defines(function() {
	EntityButtonStart = EntityButton.extend({
		//spriteImage: new ig.Image(spriteSheets["gameScreen"].meta.image),
		spriteSheet: "gameScreen",
		spriteID: "button-black.png",
		text: "Tutorial",
		
		type: ig.Entity.TYPE.B,
		gravityFactor: 0,
		alpha: 1,
		offset: {x: 0, y: 0},
		scale: {x: 1, y: 1},
		anchor: {x: 0.5, y: 0.5},
		locked: false,

		init: function(x,y,settings) {
			this.parent(x,y,settings);
			
			ig.game.sortEntitiesDeferred();
		},

		updateOnOrientationChange: function() {
			// this function is fired once whenever orientation changed (portrait <=> landscape)
			// so you can re-locate or re-size here
			this.parent();
			
			switch(igh.viewport.forceOrientation) {
				case "portrait":
					this._POS = {
						x: 0.5*ig.system.width,
						y: 0.5*ig.system.height
					};
					break;
				case "landscape":
					this._POS = {
						x: 0.25*ig.system.width,
						y: 0.55*ig.system.height
					};
					break;
				default:
					// serve both orientation
					if(igh.viewport.orientation.portrait) {
						// portrait
						this._POS = {
							x: 0.5*ig.system.width,
							y: 0.5*ig.system.height
						};
					}
					else {
						// landscape (not portrait)
						this._POS = {
							x: 0.25*ig.system.width,
							y: 0.55*ig.system.height
						};
					}
				break;
			}
			
		},

		clicked: function() {
			this.parent();
			
			this.control.startGame();
			ig.game.triggerUserAction();
            
			this.control.buttons.start = null;
			this.kill();
		},
		
		clicking: function() {
			this.parent();
		},

		released: function() {
			this.parent();
		},
		
		releasedOutside: function() {
			this.parent();
		},
		
		leave: function() {
			this.parent();
		},
		
		over: function() {
			this.parent();
		},
		
		update: function() {
			this.parent();
		},
		
		drawSprite: function() {
			var context = ig.system.context;
			/*
            var frame = spriteSheets[this.spriteSheet].frames[this.spriteID].frame;
			
			context.drawImage(
				this.spriteImage.data,
				frame.x, frame.y, 
				frame.w, frame.h,
				this.pos.x, 
				this.pos.y,
				this.size.x,
				this.size.y
			);
            */
		},
		
		drawText: function() {
			var context = ig.system.context;
			if(this.text) {
				context.fillStyle = "rgba(255,255,255,"+this.alpha+")";
				context.textAlign = "center";
				context.textBaseline = "middle";
				context.globalAlpha = 1;		
				
				// different font size, for other languages
				var maxFontHeight = this.size.y*0.9;
				context.font=maxFontHeight+"px bowlby";
				var textBox = context.measureText(localisedStrings[this.text][igh.settings.language]);
				
				if(textBox.width >= 0.8*this.size.x) {
					maxFontHeight *= this.size.x/textBox.width*0.8;
				}

				context.font = (maxFontHeight).floor() + "px bowlby";
				context.fillText(
					localisedStrings[this.text][igh.settings.language],
					this.pos.x + 0.5*this.size.x,
					this.pos.y + 0.535*this.size.y
				);
			}
		},

		draw: function() {
			this.parent();
			
			this.drawSprite();
			this.drawText();
			
		}
	});
	
});