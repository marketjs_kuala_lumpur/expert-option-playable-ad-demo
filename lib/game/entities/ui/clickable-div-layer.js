ig.module(
	'game.entities.ui.clickable-div-layer'
)
.requires(
	'impact.entity'
)
.defines(function(){
	EntityClickableDivLayer = ig.Entity.extend({
        zIndex: 8888,
		type: ig.Entity.TYPE.B,
		pos: {x: 0, y: 0},
		size: {x: 0, y: 0},
		lastSize: {x: 0, y: 0},
		
		div_layer_name: "install-now",
		onClickFunction: null,
		ignorePause: true,

		cssZIndex: 3,
		
		init:function(x,y,settings) {
			this.parent(x,y,settings);
            
            this.createClickableLayer();
		},

		createClickableLayer:function() {
			this.elem = document.getElementById(this.div_layer_name);
            
			if( !this.elem || typeof(this.elem) === "undefined" ){
				this.createClickableOutboundLayer();	
			}
            
			this.setupEvent();
			this.updateCSS();
			this.show();
		},

		idle: function() {
			if(ig.game.pointer.hoveringItem === this) {
				ig.game.pointer.hoveringItem = false;
			}
			if(ig.game.pointer.firstClick === this) {
				ig.game.pointer.firstClick = false;
			}
			this.isClicking = false;
		},
		
		clicking: function() {
			return this.idle();
		},

		clicked: function() {
			return this.idle();
		},

		released: function() {
			this.onClickFunction();
			
			return this.idle();
		},

		createClickableOutboundLayer:function() {
			this.elem = ig.$new("div");
			this.elem.setAttribute("id",this.div_layer_name);
			
			// append to document
			document.getElementById("gamebox").appendChild(this.elem);
		},
        
        kill: function() {
            this.parent();
            
			// remove from document
			document.getElementById("gamebox").removeChild(this.elem);
        },
		
		setupEvent: function() {
			this.elem.onclick = this.onClickFunction.bind(this);
			this.elem.onmousemove = ig.input.mousemove.bind(ig.input);
		
			if( ig.ua.touchDevice ) {
				// Standard
				this.elem.ontouchmove = ig.input.mousemove.bind(ig.input);
			
				// MS
				this.elem.onmspointermove = ig.input.mousemove.bind(ig.input);
				this.elem.style.msTouchAction = 'none';
			}
		},
		
		show: function() {
			if(this.elem) {
				this.elem.style.visibility = "visible";
			}
		},
		
		hide: function() {
			if(this.elem) {
				this.elem.style.visibility = "hidden";
			}
		},
		
		updateCSS: function() {
			if(this.elem) {
				var divleft = ( this.pos.x * MJS.view.viewport.widthRatio - parseFloat(ig.system.canvas.offsetLeft) ) + "px",
					divtop = ( this.pos.y * MJS.view.viewport.heightRatio - parseFloat(ig.system.canvas.offsetTop) ) + "px",
					divwidth = ( this.size.x * MJS.view.viewport.widthRatio ) + "px",
					divheight = ( this.size.y * MJS.view.viewport.heightRatio ) + "px";
				
                
                
				var cssString = "" +
					"float: " + "left" + ";" +
					"position: " + "absolute" + ";" +
					"left: " + divleft + ";" +
					"top: " + divtop + ";" +
					"width: " + divwidth + ";" +
					"height: " + divheight + ";" +
					"z-index: " + this.cssZIndex + "; "+
                    "-webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;-webkit-tap-highlight-color: rgba(0,0,0,0);-webkit-tap-highlight-color: transparent;";
			
				this.elem.setAttribute("style",cssString);		
			}
		}
	});
});