ig.module('game.entities.ui.banner')
.requires(
    'impact.entity',
    'game.entities.ui.clickable-div-layer'
)
.defines(function () {

    EntityBanner = ig.Entity.extend({
        zIndex: 99999,
		type: ig.Entity.TYPE.B,
		gravityFactor: 0,
		scale: {x: 1, y: 1},
		alpha: 1,
		anchor: {x: 0.5, y: 0.5},
		locked: false,
		showText: false,
		ignorePause: true,
		text: "",
		displayText: "",
        size: {
            x: 480,
            y: 66
        },
		
		init: function (x, y, settings) {
            this.parent(x, y, settings);
			
			if(!ig.global.wm)
			{
				this.reset(x,y,settings);
                this.createClickableDivLayer();
				this.spawnButton();
			}
        },
        
		createClickableDivLayer: function() {
			this.clickableDiv = ig.game.spawnEntity(
				EntityClickableDivLayer, 
				0, 0, 
				{
                    cssZIndex: 99,
					size: { x: ig.system.width, y: this.size.y },
					div_layer_name: "install-now-banner",
					onClickFunction: this.released.bind(this)
				}
			);
		},
		
        getDisplayText: function() {
            return this.displayText = localisedStrings[this.text][igh.settings.language];
        },
    
		changeText: function(text) {
			if(text in localisedStrings) {
				this.text = text;
				this.getDisplayText();
				this.showText = true;
			}
			else {
				this.text = '';
				this.displayText = '';
				this.showText = false;
			}
		},
		
		spawnButton: function() {
            /*
			this.buttonInstallNow = ig.game.spawnEntity(
				EntityButtonInstallNow,
				this.pos.x + 0.5*this.size.x,
				this.pos.y + 0.5*this.size.y,
				{
					banner: this,
					control: this.control,
					zIndex: this.zIndex+1
				}
			);
			this.buttonInstallNow.hide();
            */
		},
        
        kill: function() {
            if(this.clickableDiv) {
                this.clickableDiv.kill();
            }
            if(this.buttonInstallNow) {
                this.buttonInstallNow.kill();
            }
            this.parent();
        },
		
		reset: function(x, y, settings) {			
            
			this.setupEvent();
			this.updateOnOrientationChange();

			// update text
			var textIndex = MJS.settings.get("installBanner","textID");
			var text = MJS.settings.get("installBanner","text")[textIndex] || "";
			this.changeText(text);
		},
		
        setupEvent: function() {
            if(this.clickableDiv) {
    			this.clickableDiv.onclick = this.released.bind(this);
    			this.clickableDiv.addEventListener('mousemove', function(event) {
    				ig.input.mousemove.bind(ig.input);
    			}.bind(this), false );
		
    			if( ig.ua.touchDevice ) {
    				// Standard
    				this.clickableDiv.addEventListener('touchmove', function(event) {
    					ig.input.mousemove.bind(ig.input);
    				}.bind(this), false );
		
    				// MS
    				this.clickableDiv.addEventListener('MSPointerMove', function(event) {
    					ig.input.mousemove.bind(ig.input);
    				}.bind(this), false );
    				this.clickableDiv.style.msTouchAction = 'none';
    			}
            }
        },
        
		hide: function() {
			if(this.div) {
				this.div.style.visibility = "hidden";
			}
			this.visible = false;
		},
		
		show: function() {
			if(this.div) {
				this.div.style.visibility = "visible";
			}
			this.visible = true;
		},
        
		updateOnSizeChange: function() {
			if(this.clickableDiv) {
				this.clickableDiv.pos = {
					x: this.pos.x,
					y: this.pos.y
				};
				this.clickableDiv.size = {
					x: this.size.x,
					y: this.size.y
				};
				this.clickableDiv.updateCSS();
			}
		},

		updateOnOrientationChange: function() {
			this.pos.x = this.pos.x;
			this.pos.y = this.pos.y
			
			this.size.x = ig.system.width;
			this.size.y = this.size.y;
      
			// show/hide banner
			if(MJS.settings.data.bannerTogglePortrait === "on" && MJS.view.viewport.orientation.portrait || 
                MJS.settings.data.bannerToggleLandscape === "on" && MJS.view.viewport.orientation.landscape
            ) {
				this.show();
			}
			else {
				this.hide();
			}
			this.updateOnSizeChange();
		},
		
		released: function() {
            if(this.visible) {
                if(igh.settings.bannerClickableOnShow === "on")
                {
                	// Analytics
                	// Click to App Store
                    PlayableSdk.openClickUrl("toolbar");
                }
                else if(this.buttonInstallNow && this.buttonInstallNow.visible === true)
                {
                	// Analytics
                	// Click to App Store
                    PlayableSdk.openClickUrl("toolbar");
                }
			}
			
			this.isClicking = false;
		},
		
        getOptimumFontSize: function(container, fontSize, fontFamily, text) {
            var optimumFontSize = fontSize || 21, minimumFontSize = 6;
      
            var context = ig.system.context;
			var textBox = context.measureText(text);
			
			// different font size, for other languages
			context.font=optimumFontSize+"px "+fontFamily;
			var textBox = context.measureText(text);
      
            if(optimumFontSize > minimumFontSize && (textBox.width > container.w || optimumFontSize > container.h)) {
                optimumFontSize -= 0.25;
                return this.getOptimumFontSize(container, optimumFontSize, fontFamily, text);
            }
            if(optimumFontSize < minimumFontSize) {
                optimumFontSize = minimumFontSize;
            }
      
            return optimumFontSize;
        },
	  
		draw:function()
		{
			this.parent();
			if(!ig.global.wm) {

				if(this.visible) {
					ig.system.context.fillStyle = igh.settings.installBanner.boxFillColor;
					ig.system.context.fillRect(this.pos.x, this.pos.y, this.size.x, this.size.y);
				}	
				
				/*
					CHANGED: text is presented on the 'Install Now' button spot in the Tutorial with {banner: false}
					https://fractionalmedia.atlassian.net/browse/WAVE-12
					Text should be presented with or without the Tool bar during the Tutorial. Otherwise user can't really understand what's going on in front of him.
				*/
				if(this.showText && this.getDisplayText() ) {
                    var text = this.getDisplayText(), 
                    container = { w: this.size.x*0.725, h: this.size.y*0.9};
                    var currentFontSize = igh.settings.installBanner.fontSize;
                    currentFontSize = this.getOptimumFontSize(container, currentFontSize, igh.settings.installBanner.fontFamily, text);
          
					ig.system.context.font = currentFontSize+"px "+igh.settings.installBanner.fontFamily;
					ig.system.context.fillStyle = igh.settings.installBanner.textFillColor;
					ig.system.context.textAlign = "center";
					ig.system.context.textBaseline = "middle";
					ig.system.context.fillText(text, this.pos.x+0.5*this.size.x, this.pos.y+0.5*this.size.y);
				}
			}
		}
	});
});