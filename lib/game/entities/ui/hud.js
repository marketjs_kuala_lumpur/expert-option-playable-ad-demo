ig.module('game.entities.ui.hud')
.requires(
    'impact.entity'
)
.defines(function () {

    EntityHud = ig.Entity.extend({
        zIndex: 1000,
		gravityFactor: 0,
		
		appNameLogo: {
			//spriteImage: new ig.Image(spriteSheets["app-name-logo"].meta.image),
			spriteSheet: "app-name-logo", 
			spriteID: "gow-logo-flat.png"
		},
		
		init: function (x, y, settings) {
            this.parent(x, y, settings);
			
			if(!ig.global.wm)
			{
				this.reset(x,y,settings);
			}
        },
		
		update: function() {
			this.parent();
		},
	  
		draw:function()
		{
			this.parent();
			
			if(!ig.global.wm) {
				this.drawAppNameLogo();
			}
		},
		
		drawAppNameLogo: function() {
			if(MJS.settings.data.gamePlayLogo == "on")
			{
				igh.drawSprite(
					ig.system.context,
					this.appNameLogo.spriteImage,
					this.appNameLogo.spriteSheet,
					this.appNameLogo.spriteID,
					MJS.settings.data.gamePlayLogoConfig.pos.x,
					MJS.settings.data.gamePlayLogoConfig.pos.y,
					{
						scale: {
							x: MJS.settings.data.gamePlayLogoConfig.scale,
							y: MJS.settings.data.gamePlayLogoConfig.scale
						}
					}
				);
			}
		}
	});
});