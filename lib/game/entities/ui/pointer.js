ig.module('game.entities.ui.pointer')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityPointer = ig.Entity.extend({
        zIndex: 16777216,
		type:ig.Entity.TYPE.A,
        checkAgainst: ig.Entity.TYPE.B,
	
        isFirstPressed: false,
        isPressed: false,
        isReleased: false,
        isHovering: false,
	    
		hoveringItem: null,
        objectArray: [],

        clickedObjectList: [],
		clickCount: 0,
        
		ignorePause: true,
		
		init: function(x,y,settings) {
			if(!ig.global.wm) {
				while( (ig.game.getEntitiesByType(EntityPointer).length) ) { 
                    ig.game.getEntitiesByType(EntityPointer)[0].kill();
				}
                
				this.parent(x,y,settings);
				ig.game.pointer = this;
				this.control = ig.game.getEntitiesByType(EntityGameControl)[0];
				this.size = {x: 11, y: 11};
			}
		},

		updateOnOrientationChange: function() {
			ig.input.delayedKeyup['click'] = true;
			ig.input.clearPressed();
			
			this.update();
			
			clickedObjectList = [];
            this.isFirstPressed = false;
			this.isPressed = false;
		},
		
        check: function( other ) {

            this.objectArray.push(other);
        },
		
        clickObject: function(targetobject){
            // if first pressed
            if(this.isFirstPressed){
                if(typeof(targetobject.clicked) == 'function'){
                    targetobject.clicked();
                    this.addToClickedObjectList(targetobject);
                    this.firstClick = targetobject;
                }
            }
            // if pressed
            if(this.isPressed && !this.isReleased){
                if(typeof(targetobject.clicking) == 'function'){
                    targetobject.clicking();
                }
            }
            // if released
            if(this.isReleased){
                if(typeof(targetobject.released) == 'function'){
                    targetobject.released();
                    this.removeFromClickedObjectList(targetobject);
                    this.firstClick = null;
                }
            }
        },
		
		getTransformedCoords: function(coords){
			// let mraid takeover
			if(MJS.view.viewport.forceOrientation) {
				if(window.mraid) {
					if( window.mraid.getVersion() ) {
						if( parseFloat(window.mraid.getVersion(), 10) >= 2 ) {
							MJS.view.viewport.orientation.degree = 0;
							return {
								x: ig.input.mouse.x/MJS.view.viewport.widthRatio - this.size.x/2 + ig.game.screen.x,
								y: ig.input.mouse.y/MJS.view.viewport.heightRatio - this.size.y/2 + ig.game.screen.y
							};
						}
					}
				}
			}
			
			
			
			var translation = {x: 0.5*ig.system.width, y: 0.5*ig.system.height};
			
			// translate ---> rotate ---> inverse-translate
			var x = -translation.y + coords.x/MJS.view.viewport.heightRatio + ig.game.screen.x;
			var y = -translation.x + coords.y/MJS.view.viewport.widthRatio + ig.game.screen.y;
			
			// invert angle and compute angle in radian
			var radian = (MJS.view.viewport.orientation.degree).toRad();
			
			var cos = Math.cos(-1*radian);
			var sin = Math.sin(-1*radian);

			var newX = x*cos - y*sin + translation.x - 0.5*this.size.x; 
			var newY = x*sin + y*cos + translation.y - 0.5*this.size.y;
			
			return {
		        x : newX,
		        y : newY
		    };
		},
		
		updatePosition: function() {
			var dx = ig.input.mouse.x/MJS.view.viewport.widthRatio, dy = ig.input.mouse.y/MJS.view.viewport.heightRatio;
			if(MJS.view.viewport.orientation.landscape) {
				dx = ig.input.mouse.x/MJS.view.viewport.heightRatio;
				dy = ig.input.mouse.y/MJS.view.viewport.widthRatio;
			}
			
			if(Math.abs(MJS.view.viewport.orientation.degree) === 90) {
				// transformed
				this.pos = this.getTransformedCoords(ig.input.mouse);
			}
			else {
				// original
				this.pos.x = ig.input.mouse.x/MJS.view.viewport.widthRatio - this.size.x/2 + ig.game.screen.x;
		        this.pos.y = ig.input.mouse.y/MJS.view.viewport.heightRatio - this.size.y/2 + ig.game.screen.y;
			}
		},
			
	    update: function() {
			this.updatePosition();
			
			this.parent();
			
            var targetObject = null ;
            var highestIndex = -1 ;

            // Find top Entity
            for(a = this.objectArray.length -1 ; a > -1 ; a --){
                if(this.objectArray[a].zIndex > highestIndex){
                    highestIndex = this.objectArray[a].zIndex;
                    targetObject = this.objectArray[a] ;
                }
            }

            if(targetObject != null){
                // Entity found within pointer area

                if(this.hoveringItem != null){
                    // If there was a hovering item registered from before:
                    // If hovering over a different Entity from before
                    if(this.hoveringItem != targetObject){
                        // Do leave() for previously hovered over Entity
                        if(typeof(this.hoveringItem.leave) == 'function'){
                            this.hoveringItem.leave();
                        }

                        // Do over() for current hovered over Entity
                        if(typeof(targetObject.over) == 'function'){
                            targetObject.over();
                        }
                    }

                }else{
                    // If no hovering item registered from before:
                    // Do over() for current hovered over Entity
                    if(typeof(targetObject.over) == 'function'){
                        targetObject.over();
                    }
                }

                this.hoveringItem = targetObject;
                this.clickObject(targetObject);

                // Clear array of captured objects
                this.objectArray = [];

            }else{
                // No entities found within pointer area

                // Do leave() for previously hovered over Entity
                if(this.hoveringItem != null && typeof(this.hoveringItem.leave) == 'function'){
                    this.hoveringItem.leave();
                    this.hoveringItem = null;
                }

                if(this.isReleased){
                    // do releasedOutside() function for previous entities where clicked() is executed
                    for(var i=0; i<this.clickedObjectList.length; i++){
                        var targetobject = this.clickedObjectList[i];
                        if(typeof(targetobject.releasedOutside) == 'function'){
                            targetobject.releasedOutside();
                        }
                    }
                    this.clickedObjectList = [];
                }
            }

            // Only check for the click once per frame, instead of
            // for each entity it touches in the 'check' function
            this.isFirstPressed = ig.input.pressed('click');
            this.isReleased = ig.input.released('click');
            this.isPressed = ig.input.state('click');
			
			if(this.isFirstPressed) {
				if(!this.clickCount) {
					this.clickCount = 0;
				}
				this.clickCount++;
			}
		},
		
        addToClickedObjectList: function(targetObject){
            this.clickedObjectList.push(targetObject);
        },

        removeFromClickedObjectList: function(targetObject){
            var temp = [];
            for(var i=0; i<this.clickedObjectList.length; i++){
                var obj = this.clickedObjectList[i];
                if(obj != targetObject){
                    temp.push(obj);
                }
            }
            this.clickedObjectList = temp;
        },
	});
});