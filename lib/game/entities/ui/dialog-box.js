ig.module('game.entities.ui.dialog-box')
.requires(
	'impact.entity',
	'game.entities.ui.clickable-div-layer'
)
.defines(function() {
	EntityDialogBox = ig.Entity.extend({
        zIndex: 10000,
		type: ig.Entity.TYPE.B,
        
		gravityFactor: 0,
		alpha: 1,
		scale: {x: 0.6, y: 0.6},
		anchor: {x: 0.5, y: 0.5},
		locked: false,
        
        background: {
            spriteImage: null,
		    spriteSheet: null,
		    spriteID: null
        },
		ignorePause: true,
        
        bufferCanvas: null,
        bufferContext: null,
		
		init: function(x,y,settings) {
			this.parent(x,y,settings);
			
            
            this.initBufferCard();
			this._POS = {
				x: this.pos.x,
				y: this.pos.y
			};
			this._COORDINATE = {
				x: this.pos.x/ig.system.width,
				y: this.pos.y/ig.system.height
			};
			this.pos = {
				x: this._POS.x-this.anchor.x*this.size.x,
				y: this._POS.y-this.anchor.y*this.size.y
			}
			
			ig.game.sortEntitiesDeferred();
		},
        
        initBufferCard: function() {
            if( this.background &&
                this.background.spriteImage &&
                this.background.spriteSheet &&
                this.background.spriteID
            ) {
                this.bufferCanvas = ig.$new("canvas");
                this.bufferCanvas.width = spriteSheets[this.background.spriteSheet].frames[this.background.spriteID].frame.w;
                this.bufferCanvas.height = spriteSheets[this.background.spriteSheet].frames[this.background.spriteID].frame.h;
            
                this.bufferContext = this.bufferCanvas.getContext('2d');
            }
            
        },
			
		idle: function() {
			if(ig.game.pointer.hoveringItem === this) {
				ig.game.pointer.hoveringItem = false;
			}
			if(ig.game.pointer.firstClick === this) {
				ig.game.pointer.firstClick = false;
			}
			this.isClicking = false;
		},
		
		clicking: function() {
            if(this.locked) return this.idle();
		},
		
		clicked: function() {
            if(this.locked) return;
			this.isClicking = true;
		},
		
		released: function() {
            if(this.locked) return;
			
			this.isClicking = false;
		},
    
        getOptimumFontSize: function(container, fontSize, fontFamily, text) {
            var optimumFontSize = fontSize || 21, minimumFontSize = 6;
      
            var context = ig.system.context;
			var textBox = context.measureText(text);
			
			// different font size, for other languages
			context.font=optimumFontSize+"px "+fontFamily;
			var textBox = context.measureText(text);
      
            if(optimumFontSize > minimumFontSize && (textBox.width > container.w || optimumFontSize > container.h)) {
                optimumFontSize -= 0.25;
                return this.getOptimumFontSize(container, optimumFontSize, fontFamily, text);
            }
            if(optimumFontSize < minimumFontSize) {
                optimumFontSize = minimumFontSize;
            }
      
            return optimumFontSize;
        },
        
        kill: function() {
            this.parent();
            
            this.bufferCanvas = null;
            this.bufferContext = null;
        },
        
		updateOnOrientationChange: function() {
			this.stopTweens(false);
		},
        
		draw: function() {
            this.parent();
            
            this.drawBuffer();
            this.drawBufferCanvas();
		},
		
		drawText: function(context, text, x, y, fontHeight, fillStyle, fontFamily, textAlignment, textBaseline) {
			if(!textAlignment) textAlignment = "center";
			if(!textBaseline) textBaseline = "middle";
			
			// draw text
			context.font = fontHeight+"px" + " " + fontFamily;
			context.textAlign = textAlignment;
			context.textBaseline = textBaseline;
			context.fillStyle = fillStyle;
			context.fillText(
				text,
				x, y
			);
		},
		
		drawDialog: function(context, textArray, x, y, fontHeight, lineSpacing, fillStyle, fontFamily, textAlignment, textBaseline, fontWeight) {
			// draw wall of texts
			var font = null;
			if(fontWeight) {
				font = fontWeight + " " + fontHeight+"px" + " " + fontFamily;
			}
			else {
				font = fontHeight+"px" + " " + fontFamily;
			}
			context.font = font;
			context.textAlign = textAlignment;
			context.textBaseline = textBaseline;
			context.fillStyle = fillStyle;
			
			for(var textIndex = 0; textIndex < textArray.length; textIndex++) {
				context.fillText(
					textArray[textIndex],
					x,
					y + textIndex*(fontHeight+lineSpacing)
				);
			}
		},
        
        drawBuffer: function() {
            if( this.bufferCanvas && this.bufferContext ) {
                this.bufferContext.clearRect(0, 0, this.bufferCanvas.width, this.bufferCanvas.height);
                
                
                this.drawBackround();
            }
        },
        
        drawBufferCanvas: function() {
			var context = ig.system.context;
            
            if( this.bufferCanvas && this.bufferContext ) {
                context.drawImage(
                    this.bufferCanvas,
                    0,0,
                    this.bufferCanvas.width,
                    this.bufferCanvas.height,
                    this.pos.x,
                    this.pos.y,
                    this.size.x,this.size.y
                );
            }
        },
        
        drawBackround: function() {
            if( this.background.spriteImage &&
                this.background.spriteSheet &&
                this.background.spriteID
            ) {
                igh.drawSprite(
                    this.bufferContext,
                    this.background.spriteImage,
                    this.background.spriteSheet,
                    this.background.spriteID,
                    0.5*this.bufferCanvas.width,
                    0.5*this.bufferCanvas.height
                );
            }

        },
        
        
	});

});