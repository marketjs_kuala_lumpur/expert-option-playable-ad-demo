ig.module('game.entities.ui.finger')
.requires(
	'impact.entity'
)
.defines(function() {
	
	EntityFinger = ig.Entity.extend({
        zIndex: 5000,
        finger: {
		    //spriteImage: new ig.Image(spriteSheets["gameScreen"].meta.image),
		    spriteSheet: "gameScreen",
		    spriteID: "tutorial-hand.png",
        },
        
		target: null,
		scale: {x: 1, y: 1},
		alpha: 0,
		angle: 0,
		
		init: function(x, y, settings) {
			this.parent(x,y,settings);
			
			this.reset();
            this.startTween();
            
			ig.game.sortEntitiesDeferred();
		},
		
		reset: function() {
            var frame = spriteSheets[this.finger.spriteSheet].frames[this.finger.spriteID].frame;
            
			this.size = {
				x: this.scale.x*frame.w,
				y: this.scale.y*frame.h
			};
			this._POS = {
				x: this.pos.x,
				y: this.pos.y
			};
		},
		
		updateOnOrientationChange: function() {
			this.stopTween();
			this.alpha = 0.01;
			
			if(this.target) {
				this._POS = {
					x: this.target._POS.x,
					y: this.target._POS.y
				};
                this.pos.x = this._POS.x;
                this.pos.y = this._POS.y;
                
				this.startTween();
			}
		},
		
		stopTween: function() {
			if(this.tweens && this.tweens.length) {
				for(var tweenIndex = 0; tweenIndex < this.tweens.length; tweenIndex++) {
					if(this.tweens[tweenIndex]) {
                        this.tweens[tweenIndex].stop();
                    }
                }
			}
		},
		
		startTween: function() {
			var tweenStart = this.tween(
				{
					pos: {
						x: this._POS.x,
						y: this._POS.y
					},
					alpha: 1
				},
				0.01,
				{
					delay: 0.5
				}
			);
			var tweenGo1 = this.tween(
				{
					pos: {
						x: this._POS.x,
						y: this._POS.y
					}
				},
				0.3
			);
			var tweenGo2 = this.tween(
				{
					pos: {
						x: this._POS.x,
						y: this._POS.y-9
					}
				},
				0.25
			);
			var tweenGo3 = this.tween(
				{
					pos: {
						x: this._POS.x,
						y: this._POS.y
					}
				},
				0.125
			);
			var tweenEnd = this.tween(
				{
					alpha: 0.01
				},
				0.25
			);
			
			var tweenDelay1 = this.tween({}, 0.25);
			var tweenDelay2 = this.tween({}, 0.25);
			var tweenDelay3 = this.tween({}, 0.75);
			
			tweenStart.chain(tweenDelay1);
			tweenDelay1.chain(tweenGo1);
			tweenGo1.chain(tweenGo2);
			tweenGo2.chain(tweenGo3);
			tweenGo3.chain(tweenDelay2);
			tweenDelay2.chain(tweenEnd);
			tweenEnd.chain(tweenDelay3);
			tweenDelay3.chain(tweenStart);
			
			tweenStart.start();
		},
		
		update: function() {
			this.parent();
		},
		
		draw: function() {
            this.parent();
            
            this.drawFinger();
		},
        
        drawFinger: function() {
            var context = ig.system.context;
            
            if( this.finger &&
                this.finger.spriteImage &&
                this.finger.spriteSheet &&
                this.finger.spriteID 
            ) {
                igh.drawSprite(
                    context,
                    this.finger.spriteImage,
                    this.finger.spriteSheet,
                    this.finger.spriteID,
                    this.pos.x,
                    this.pos.y,
                    {
                        pivot: {
                            x: 0.02, 
                            y: 0.15
                        },
                        flip: {
                            x: true
                        }
                    }
                );
            }
        }
	});
});