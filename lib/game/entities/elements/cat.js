ig.module('game.entities.elements.cat')
.requires(
    'impact.entity'
)
.defines(function () {

    EntityCat = ig.Entity.extend({
    	animSheet: new ig.AnimationSheet( 'media/graphics/raw/logo/kitty.png', 218, 325 ),
		size: { x: 218, y: 325 },
        
		init: function (x, y, settings) {
            this.parent(x, y, settings);
			MJS.log.console("KITTY","game.entities.elements.cat.LINE13");
			if(!ig.global.wm) {
                this.addAnim( 'idle', 0.05, [0,1,2,3,4,5,6,7] );
			}
        },
        
        update: function() {
            this.parent();
            
			if(!ig.global.wm) {
                this.updatePos();
                this.updateAnimAngle();
			}
        },
        
        updatePos: function() {
            
    
        },
        
        updateAnimAngle: function() {
            
        },
		
        draw: function() {
            this.parent();
            
			if(!ig.global.wm) {
                
			}
        }
    
    });

});