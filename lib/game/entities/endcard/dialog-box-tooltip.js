ig.module('game.entities.endcard.dialog-box-tooltip')
.requires(
	'game.entities.ui.dialog-box'
)
.defines(function() {
	EntityDialogBoxTooltip = EntityDialogBox.extend({
        zIndex: 5000,
		background: {
            //spriteImage: new ig.Image(spriteSheets["gameScreen"].meta.image),
		    spriteSheet: "gameScreen",
		    spriteID: "tooltip.png"
        },
		scale: {
			x: 1,
			y: 1
		},
		alpha: 1,
		dialog: "",
		icon: {
            //spriteImage: new ig.Image(spriteSheets["gameScreen"].meta.image),
		    spriteSheet: "gameScreen",
		    spriteID: "tooltip.png"
		},
		ignorePause: false,
		shouldClose: false,
		
		init: function(x,y,settings) {
			this.parent(x,y,settings);
			this._scale = {
				x: this.scale.x,
				y: this.scale.y
			}
			this.updateOnOrientationChange();
		},
		
		updateOnOrientationChange: function() {
			this.parent();
			
            var tooltipArray = ig.game.getEntitiesByType(EntityDialogBoxTooltip);
			if(tooltipArray.length) {
				for(var tooltipIndex = 0; tooltipIndex < tooltipArray.length; tooltipIndex++) {
					if(tooltipArray[tooltipIndex] !== this) tooltipArray[tooltipIndex].endTween();
				}
			}
			
			switch(igh.viewport.forceOrientation) {
				case "portrait":
					this.pos.x = this._originPos.portrait.x;
					this.pos.y = this._originPos.portrait.y;
					break;
				case "landscape":
					this.pos.x = this._originPos.landscape.x;
					this.pos.y = this._originPos.landscape.y;
					break;
				default:
					// serve both orientation
					if(igh.viewport.orientation.portrait) {
						this.pos.x = this._originPos.portrait.x;
						this.pos.y = this._originPos.portrait.y;
					}
					else {
						this.pos.x = this._originPos.landscape.x;
						this.pos.y = this._originPos.landscape.y;
					}
				break;
			}
			
			
			var spriteSize = {
				x: spriteSheets[this.background.spriteSheet].frames[this.background.spriteID].frame.w,
				y: spriteSheets[this.background.spriteSheet].frames[this.background.spriteID].frame.h
			};
			
			this.scale = {
				x: this._scale.x*Math.min(ig.system.width/spriteSize.x, ig.system.height/spriteSize.y),
				y: this._scale.y*Math.min(ig.system.width/spriteSize.x, ig.system.height/spriteSize.y)
			};
			this.size.x = spriteSize.x*this.scale.x;
			this.size.y = spriteSize.y*this.scale.y;
			
			// this.kill();
			
			this._END_POS = {
				x: this.pos.x-0.5*this.size.x,
				y: this.pos.y-0.5*this.size.y
			};
			if(igh.viewport.orientation.portrait) {
				this._START_POS = {
					x: this._END_POS.x,
					y: this._END_POS.y-222
				}
			}
			else {
				this._START_POS = {
					x: this._END_POS.x,
					y: this._END_POS.y+522
				}
			}
			this.pos.x = this._START_POS.x;
			this.pos.y = this._START_POS.y;
						
			if(this.shouldClose) this.endTween();
			else this.startTween();
		},
		
		autoClose: function(time) {
			this.tween({},time,{
				onComplete: function() {
					this.endTween();
				}.bind(this)
			}).start();
		},
		
		clicked: function() {
			this.parent();
			if(!this.locked) {
				this.endTween();
				this.locked = true;
				this.type = ig.Entity.TYPE.NONE;
				return this.idle();
			}
		},
		
		startTween: function() {
			this.tween(
				{
					pos: {
						y: this._END_POS.y
					},
					alpha: 1
				},
				0.25,
				{
					delay: 0.25,
					easing: ig.Tween.Easing.Back.EaseOut,
					onComplete: function() {
						
					}.bind(this)
				}
			).start();
		},
		
		endTween: function() {
			this.shouldClose = true;
			
			this.tween(
				{
					pos: {
						y: this._START_POS.y
					},
					alpha: 0.01
				},
				0.5,
				{
					delay: 0.125,
					easing: ig.Tween.Easing.Back.EaseOut,
					onComplete: function() {
						this.idle();
						
						if(this.control.tooltip === this) this.control.tooltip = null;
						this.kill();
					}.bind(this)
				}
			).start();
		},
		draw: function() {
            var context = ig.system.context;
            
			context.globalAlpha = this.alpha.limit(0,1);
			this.parent();
			
			ig.system.context.globalAlpha = 1;
		},
        
        drawBuffer: function() {
            this.parent();
            
            this.drawIcon();
			this.drawMessage();
        },
        
        drawIcon: function() {
			if( this.icon && 
                this.icon.spriteImage && 
                this.icon.sPriteSheet && 
                this.icon.spriteID
            ) {
				igh.drawSprite(
                    this.bufferContext,
                    this.icon.spriteImage,
                    this.icon.spriteSheet, 
                    this.icon.spriteID, 
                    0*this.bufferCanvas.width, 
                    0*this.bufferCanvas.height
                );
			}
        },
        
        drawMessage: function() {
			if(this.dialog) {
                var dialogMessage = localisedStrings[this.dialog][igh.settings.language].split("\n"),
                    pos = {
                        x: 0.3*this.bufferCanvas.width,
                        y: 0.3*this.bufferCanvas.height
                    },
                    fontSize = 27,
                    lineSpacing = 2,
                    fontFamily = "arial",
                    textAlign = "left",
                    textBaseline = "middle",
                    fontWeight = "bold";
                    
                
    			this.drawDialog(
                    this.bufferContext,
                    dialogMessage,
    				pos.x-0.5,
    				pos.y-0.5,
    				fontSize,
    				lineSpacing,
    				"rgba(128,128,128,"+this.alpha+")",
    				fontFamily, textAlign, textBaseline, fontWeight
    			);
    			this.drawDialog(
                    this.bufferContext,
    				dialogMessage,
    				pos.x,
    				pos.y,
    				fontSize,
    				lineSpacing,
    				"rgba(79,63,45,"+this.alpha+")",
    				fontFamily, textAlign, textBaseline, fontWeight
    			);
			}
            
        }
	});
});