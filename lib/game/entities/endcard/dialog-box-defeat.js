ig.module('game.entities.endcard.dialog-box-defeat')
.requires(
	'game.entities.ui.dialog-box'
)
.defines(function() {	
	EntityDialogBoxDefeat = EntityDialogBox.extend({
		background: {
            //spriteImage: new ig.Image(spriteSheets["gameScreen"].meta.image),
		    spriteSheet: "gameScreen",
		    spriteID: "banner-defeat.png",
        },
        alpha: 0.01,
		
		init: function(x,y,settings) {
			this.parent(x,y,settings);
			
			this._SIZE = {
				x: spriteSheets[this.background.spriteSheet].frames[this.background.spriteID].frame.w,
				y: spriteSheets[this.background.spriteSheet].frames[this.background.spriteID].frame.h
			};
			this.tween(
				{
					alpha: 1,
					pos: {
						x: this._POS.x-this.anchor.x*this._SIZE.x,
						y: this._POS.y-this.anchor.y*this._SIZE.y
					},
					size: {
						x: this._SIZE.x,
						y: this._SIZE.y
					}
				},
				0.75,
				{
					delay: 0.25,
					easing: ig.Tween.Easing.Quadratic.EaseInOut,
					onComplete: function() {
						this.spawnUpsell();
					}.bind(this)
				}
			).start();
		},
		
		spawnUpsell: function() {
			if(!this.upsell) {
				this.tween(
					{
					},
					0.25,
					{
						delay: 0.25,
						onComplete: function() {
							this.upsell = ig.game.spawnEntity(
								EntityDialogBoxUpsell, 
								0.5*ig.system.width, 
								0.5*ig.system.height,
								{
									control: this.control,
									defeat: this
								} 
							);
						}.bind(this)
					}
				).start();
			}
		},
		
		updateOnOrientationChange: function() {
			this.parent();
			
			this.pos = {
				x: this._COORDINATE.x*ig.system.width - this.anchor.x*this.size.x,
				y: this._COORDINATE.y*ig.system.height - this.anchor.y*this.size.y
			};
			this.spawnUpsell();
			if(this.upsell) this.upsell.alpha = 1;
		},
		
		draw: function() {
            var context = ig.system.context;
            
			context.globalAlpha = this.alpha.limit(0,1);
			this.parent();
			
			// draw text
			this.drawText(
                context, 
				localisedStrings["defeat"][igh.settings.language],
				this.pos.x + 0.693*this.size.x,
				this.pos.y + 0.535*this.size.y,
				this.size.y*0.27,
				"rgba(255,255,255,"+this.alpha+")",
				"bowlby", "center", "middle"
			);

			context.globalAlpha = 1;
		}
		
	});
});