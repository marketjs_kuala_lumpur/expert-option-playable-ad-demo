ig.module('game.entities.endcard.dialog-box-upsell')
.requires(
	'game.entities.ui.dialog-box'
)
.defines(function() {
	EntityDialogBoxUpsell = EntityDialogBox.extend({
        zIndex: 20000,
        
        background: {
            //spriteImage: new ig.Image(spriteSheets["backgrounds"].meta.image),
		    spriteSheet: "backgrounds",
		    spriteID: "upsell.jpg"
        },
		appNameLogo: {
			//spriteImage: new ig.Image(spriteSheets["app-name-logo"].meta.image),
			spriteSheet: "app-name-logo", 
			spriteID: "gow-logo-flat.png",
			anchor: {
				x: 0.65,
				y: 0.85
			}
		},
		ratingBadge: {
			//spriteImage: new ig.Image(spriteSheets["rating-badge"].meta.image),
			spriteSheet: "rating-badge", 
			spriteID: "",
			taiwanSpriteID: "ratingbadge_age12_tw.png",
            russiaSpriteID: "ratingbadge_age12_ru.png",
			anchor: {
				x: 1,
				y: 1
			}
		},
		
		scale: {
			x: 0.9,
			y: 0.9
		},

		alpha: 0.01,
		init: function(x,y,settings) {
			this.parent(x,y,settings);
			this.updateOnOrientationChange();
			
            this.updateDialog();
      
			if(this.defeat) {
				this.tween(
					{
						defeat: {
							alpha: 0.01
						}
					},
					0.5,
					{
						delay: 0.5,
						easing: ig.Tween.Easing.Quadratic.EaseInOut,
						onComplete: function() {			
							if(this.defeat) {
								this.defeat.kill();
								this.defeat = null;
							}
						}.bind(this)
					}
				).start();
			}
			
			this.startTween();
			
			ig.game.sortEntitiesDeferred();
		},
    
        updateDialog: function() {
      
            this.headerConfig = igh.settings.endCard.header;
            this.upsellConfig = igh.settings.endCard.upsell;
            this.installConfig = igh.settings.endCard.install;
      
            if(this.headerConfig) {
                if(ig.game.gameStatus in this.headerConfig) {
                    if( this.headerConfig[ig.game.gameStatus].toggle === "on" ) {
                        var textID = this.headerConfig[ig.game.gameStatus].textID;
                        this.headerText = this.headerConfig[ig.game.gameStatus].text[textID];
                    }
                }
            }
      
            if(this.upsellConfig) {
                if(ig.game.gameStatus in this.upsellConfig) {
                    if( this.upsellConfig[ig.game.gameStatus].toggle === "on" ) {
                        var textID = this.upsellConfig[ig.game.gameStatus].textID;
                        this.upsellText = this.upsellConfig[ig.game.gameStatus].text[textID];
                    }
                }
            }
      
            if(this.installConfig) {
                var textID = this.installConfig.textID;
                this.installText = this.installConfig.text[textID];

            }
        },
		
        startTween: function() {
			
            this.tween(
                {
                    alpha: 1
                },
                0.25,
                {
                    delay: 0.25,
                    easing: ig.Tween.Easing.Quadratic.EaseInOut,
                    onComplete: function() {
                        this.createClickableDivLayer();
                    
                        ig.game.sortEntities();
                        ig.game.paused = true;
                    }.bind(this)
                }
            ).start();
        },
		
        createClickableDivLayer: function() {
            this.clickableDiv = ig.game.spawnEntity(
                EntityClickableDivLayer, 
                0, 0, 
                {
                    cssZIndex: 99,
                    size: { x: ig.system.width, y: ig.system.height },
                    div_layer_name: "install-now-end-card",
                    onClickFunction: this.released.bind(this)
                }
			);
		},
		
		released: function() {			
			// Analytics
			// Click to App Store
			PlayableSdk.openClickUrl( ig.game.gameStatus );
		},
		
		clicked: function() {
			
		},
		
		updateOnOrientationChange: function() {
			this.parent();
			
			switch(igh.viewport.forceOrientation) {
				case "portrait":
					this.scale = {
						x: 0.9,
						y: 0.9
					};
                    break;
				case "landscape":
					this.scale = {
						x: 0.7,
						y: 0.7
					};
					break;
				default:
					// serve both orientation
					if(igh.viewport.orientation.portrait) {
						this.scale = {
							x: 0.9,
							y: 0.9
						};
					}
					else {
						this.scale = {
							x: 0.7,
							y: 0.7
						};
					}
				break;
			}
			
			
			this.size = {
				x: spriteSheets[this.background.spriteSheet].frames[this.background.spriteID].frame.w,
				y: spriteSheets[this.background.spriteSheet].frames[this.background.spriteID].frame.h
			};
			this.scale = {
				x: this.scale.x*Math.min(ig.system.width/this.size.x, ig.system.height/this.size.y),
				y: this.scale.y*Math.min(ig.system.width/this.size.x, ig.system.height/this.size.y)
			};
			if(this.scale.x < 1) this.size.x *= this.scale.x;
			if(this.scale.y < 1) this.size.y *= this.scale.y;
			
			this.pos = {
				x: this._COORDINATE.x*ig.system.width - this.anchor.x*this.size.x,
				y: this._COORDINATE.y*ig.system.height - this.anchor.y*this.size.y
			};
			
			
		},
		
		updateOnSizeChange: function() {
			// update div
			if(this.clickableDiv) {
				this.clickableDiv.size = {x: ig.system.width, y: ig.system.height};
				this.clickableDiv.updateCSS();
			}
		},
		
		draw: function() {
            var context = ig.system.context;
            
			context.globalAlpha = this.alpha.limit(0,1);
			context.fillStyle = "rgba(0,0,0,"+this.alpha.limit(0,0.7)+")";
			context.fillRect(0,0,ig.system.width,ig.system.height);
			this.parent();
			
            // header
            this.drawHeader();
            this.drawContent();
            this.drawInstallText();
			this.drawAppNameLogo();
			this.drawRatingBadge();
			this.drawDisclaimer();
			
			context.globalAlpha = 1;
		},
        
        drawHeader: function() {
            var context = ig.system.context;
            
            if(this.headerText && this.headerText in localisedStrings) {
                var headerText = localisedStrings[this.headerText][igh.settings.language];
                var currentInfoFontSize = this.getOptimumFontSize(
                    {w: this.size.x*0.66, h: this.size.y*0.05},
                    this.scale.y*this.headerConfig.fontSize,
                    this.headerConfig.fontFamily,
                    headerText
                );
        
                this.drawText(
                    context,
                    headerText,
                    this.pos.x + 0.57*this.size.x,
                    this.pos.y + 0.13*this.size.y,
                    currentInfoFontSize,
                    this.headerConfig.textFillColor,
                    this.headerConfig.fontFamily, "center", "middle"
                );
        
            }
        },
        
        drawContent: function() {
            var context = ig.system.context;
            
            if(this.upsellText && this.upsellText in localisedStrings) {
        
                var upsellText = localisedStrings[this.upsellText][igh.settings.language];
                this.drawDialog(
                    context,
                    upsellText.split("\n"),
                    this.pos.x + 0.57*this.size.x,
                    this.pos.y + 0.19*this.size.y,
                    this.scale.y*this.upsellConfig.fontSize,
                    this.scale.y*this.upsellConfig.fontLineSpacing,
                    this.upsellConfig.textFillColor,
                    this.upsellConfig.fontFamily, "center", "middle"
                );
        
            }
        },
        
        drawInstallText: function() {
            var context = ig.system.context;
      
			if(this.installText && this.installText in localisedStrings) {

                var installText = localisedStrings[this.installText][igh.settings.language];
                var currentInfoFontSize = this.getOptimumFontSize(
                    {w: this.size.x*0.35, h: this.size.y*0.05},
                    this.scale.y*this.installConfig.fontSize,
                    this.installConfig.fontFamily,
                    installText
                );
        
                this.drawText(
                    context,
                	installText,
                	this.pos.x + 0.6431*this.size.x,
                	this.pos.y + 0.661*this.size.y,
                	currentInfoFontSize,
                	this.installConfig.textFillColor,
                	this.installConfig.fontFamily, "center", "middle"
                );
            }
        },
		
		drawAppNameLogo: function() {
			if(igh.settings.endCardLogo == "on")
			{
				igh.drawSprite(
					ig.system.context,
					this.appNameLogo.spriteImage,
					this.appNameLogo.spriteSheet,
					this.appNameLogo.spriteID,
					this.pos.x + this.appNameLogo.anchor.x*this.size.x,
					this.pos.y + this.appNameLogo.anchor.y*this.size.y,
					{
						scale: ig.copy(this.scale),
						alpha: this.alpha
					}
				);
			}
		},
		
		drawRatingBadge: function() {
			var context = ig.system.context;
			
			/* Taiwan Rating Badge */
			if(
				(igh.settings.country).toUpperCase() === "TW" ||
				PlayableSdk.cfg.getCountry().toUpperCase() === "TW"
			) {
				this.ratingBadge.spriteID = this.ratingBadge.taiwanSpriteID;
			}
			if(
				(igh.settings.country).toUpperCase() === "RU" ||
				PlayableSdk.cfg.getCountry().toUpperCase() === "RU"
			) {
				this.ratingBadge.spriteID = this.ratingBadge.russiaSpriteID;
			}
			
			if(this.ratingBadge.spriteID) {
			    igh.drawSprite(
					ig.system.context,
			        this.ratingBadge.spriteImage,
			        this.ratingBadge.spriteSheet,
			        this.ratingBadge.spriteID,
			        this.pos.x+this.ratingBadge.anchor.x*this.size.x, 
			        this.pos.y+this.ratingBadge.anchor.y*this.size.y,
			        {
			            pivot: {
			                x: 1, y: 1
			            },
			            alpha: this.alpha,
			            scale: ig.copy(this.scale)
			        }
			    );
			}
		},
        
        drawDisclaimer: function() {
			// Disclaimer if Enabled is true and not empty
			if( igh.settings.disclaimerToggle === "on" )
			{
	            var messageTextID = igh.settings.disclaimerMessageID;
	            if(messageTextID)
	            {
	                var messageText = localisedStrings[igh.settings.disclaimerMessage[messageTextID]][igh.settings.language],
	                messageTextFont = igh.settings.disclaimerFontFamily,
	                messageTextSize = igh.settings.disclaimerFontSize,
					messageTextColor = igh.settings.disclaimerFontColor;
	            }
				
	            if(messageText) {
                    ig.system.context.globalAlpha = this.alpha;
	                this.drawDialog( 
						ig.system.context,
	                    messageText.split("\n"),
	                    0.5*ig.system.width,
	                    0.97*ig.system.height,
	                    messageTextSize,
	                    2,
	                    messageTextColor,
	                    messageTextFont, "center", "middle"
	                );
					ig.system.context.globalAlpha = 1;
	            }
			}
        }
        
	});
});