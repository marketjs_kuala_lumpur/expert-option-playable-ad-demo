ig.module('game.entities.endcard-control')
.requires(
    'impact.entity'    
)
.defines(function () {

    EntityEndcardControl = ig.Entity.extend({
    	zIndex: 10,
        
        gamebg:{
            spriteImage: new ig.Image(spriteSheets.gamebg.meta.image),
            spriteID:"gamebg.png",
            spriteSheet:"gamebg"
        },
        toplogo:{
            spriteImage: new ig.Image(spriteSheets.toplogo.meta.image),
            spriteID:"toplogo.png",
            spriteSheet:"toplogo"
        },
        tittle:{
            spriteImage: new ig.Image(spriteSheets.tittle.meta.image),
            spriteID:"tittle.png",
            spriteSheet:"tittle"
        },


        endCardSize:0,
        textSize:0,
		init: function (x, y, settings) {
            this.parent(x, y, settings);
			
			if(!ig.global.wm) {

				if(ig.ua.mobile){
					this.encardtext1=localisedStrings.endcardMobile[0];
					this.encardtext2=localisedStrings.endcardMobile[1];
					this.encardtext3=localisedStrings.endcardMobile[2];
				}else{
					this.encardtext1=localisedStrings.endcardDesktop[0];
					this.encardtext2=localisedStrings.endcardDesktop[1];
					this.encardtext3=localisedStrings.endcardDesktop[2];
				}

	            this.tween({endCardSize:1},0.3,{easing: ig.Tween.Easing.Back.EaseOut, onComplete:function(){
		            this.tween({textSize:40},0.3,{easing: ig.Tween.Easing.Back.EaseOut, onComplete:function(){
		            	if(ig.ua.mobile){
		            		var caption='Install Now';
		            	}else{
		            		var caption='Trade Now';		            		
		            	}

                        if (MJS.view.viewport.orientation.landscape) {
                            this.screen='L';
                            var boxPos={x:ig.system.width/2-150,y:ig.system.height/2+130};
                        }else{
                            this.screen='P';
                            var boxPos={x:ig.system.width/2-150,y:ig.system.height/2+180};
                        }


						this.installnow = ig.game.spawnEntity(EntityButtonInstallNow,100,10, {parrent: this,
																							  boxPos:boxPos,
																							  boxSize:{x:300,y:90},
																							  updown:0,
																							  caption:caption,
																							  endcard:true,
                                                                                              div_layer_name:'install-now-button',
																							  });		        
		            }.bind(this)}).start();      	        
	            }.bind(this)}).start();      
	           
                // this.updateOnOrientationChange();                
				ig.game.sortEntitiesDeferred();
			}
        },
		updateOnOrientationChange: function() {
            if (MJS.view.viewport.orientation.landscape) {
                this.screen='L';

                this.installnow.boxPos={x:ig.system.width/2-150,y:ig.system.height/2+130};
                this.installnow.boxSize={x:300,y:90};
                this.installnow.updateOnOrientationChange();
                this.installnow.reset();

            }else{
                this.screen='P';

                this.installnow.boxPos={x:ig.system.width/2-150,y:ig.system.height/2+180};
                this.installnow.boxSize={x:300,y:90};
                this.installnow.updateOnOrientationChange();
                this.installnow.reset();

            }
        },		
		update: function() {
			this.parent();			
		},        
   		draw:function(){
			this.parent();
			this.drawBackground();
		},
        drawBackground: function() {
            var context = ig.system.context;
			
            if (MJS.view.viewport.orientation.landscape) {
                var fr = spriteSheets[this.gamebg.spriteSheet].frames[this.gamebg.spriteID].frame;
                context.drawImage(
                    this.gamebg.spriteImage.data,
                    0,
                    300,
                    fr.w,
                    400,
                    0,
                    0,
                    ig.system.width,
                    ig.system.height
                );                
            }else{
                var fr = spriteSheets[this.gamebg.spriteSheet].frames[this.gamebg.spriteID].frame;
                context.drawImage(
                    this.gamebg.spriteImage.data,
                    0,
                    0,
                    fr.w,
                    fr.h,
                    0,
                    0,
                    ig.system.width,
                    ig.system.height
                );                
            }   

            var fr = spriteSheets[this.tittle.spriteSheet].frames[this.tittle.spriteID].frame;
            context.drawImage(
                this.tittle.spriteImage.data,
                0,
                0,
                fr.w,
                fr.h,
                ig.system.width/2-fr.w*this.endCardSize/2,
                ig.system.height/2-fr.h*this.endCardSize/2-170,
                fr.w*this.endCardSize,
                fr.h*this.endCardSize
            );                

            this.drawText();

        },
    	drawText:function(){

            var context = ig.system.context;

            if(this.textSize>0){
				context.fillStyle = "rgba(255,255,255,1)";
				context.font = this.textSize+"px sans,arial";
				context.textAlign = "center";		
                if(this.screen=='P'){		
    				context.fillText(this.encardtext1,ig.system.width/2, ig.system.height/2);
    				context.fillText(this.encardtext2,ig.system.width/2, ig.system.height/2+50);
    				context.fillText(this.encardtext3,ig.system.width/2, ig.system.height/2+100);
                }else{
                    context.fillText(this.encardtext1,ig.system.width/2, ig.system.height/2-30);
                    context.fillText(this.encardtext2,ig.system.width/2, ig.system.height/2+20);
                    context.fillText(this.encardtext3,ig.system.width/2, ig.system.height/2+70);                    
                }
			}
    	},
    });

});