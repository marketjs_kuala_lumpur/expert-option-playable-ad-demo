ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	
    //'impact.debug.debug',
    
	//plugins
	'plugins.helper.director',
	'plugins.helper.tween',
	'plugins.helper.splash-loader',
	//levels
	'game.levels.game',
	'game.levels.endcard'
)
.defines(function(){
	this.START_OBFUSCATION;
	this.FRAMEBREAKER;
	MyGame = ig.Game.extend({
        BUILD_TIMESTAMP: "13-Jan-2020",
		drawCloseButton: false,
		userAction : 0,
		trade:null,
		guidedExperience:null,
		init: function() {
            
			// Initialize your game here; bind keys etc.
			this.setupControls();
			this.setupDirector();
			
			MJS.view.orientationHandler();
			
			// let mraid v2 takes over orientation handling
			if(MJS.view.viewport.forceOrientation) {
				if(window.mraid) {
					if( window.mraid.getVersion() ) {
						if( parseFloat(window.mraid.getVersion(), 10) >= 2 ) {
							// use mraid handler instead of ours
							orientationHandler = function() {
								// make sure the pointer isn't rotated
								MJS.view.viewport.orientation.degree = 0;
							};
							sizeHandler = function() { };
						}
					}
				}
			}

			ig.game.trade=adDetails.settings.trade;
			ig.game.unit=adDetails.settings.unit;
			ig.game.guidedExperience=adDetails.settings.guidedExperience;
			// if(ig.ua.mobile){
			// 	if(ig.ua.iOS){
			// 		MJS.CONST.URL=adDetails.settings.itunesUrl;
			// 	}else{
			// 		MJS.CONST.URL=adDetails.settings.playStoreUrl;
			// 	}
			// }else{
			// 	MJS.CONST.URL=adDetails.settings.desktopUrl;
			// }

			
		},
		
        roundRect: function(ctx, x, y, width, height, radius, fill, stroke) {
            if (typeof stroke == "undefined" ) {
                stroke = true;
            }
            if (typeof radius === "undefined") {
                radius = 5;
            }
            ctx.beginPath();
            ctx.moveTo(x + radius, y);
            ctx.lineTo(x + width - radius, y);
            ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
            ctx.lineTo(x + width, y + height - radius);
            ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
            ctx.lineTo(x + radius, y + height);
            ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
            ctx.lineTo(x, y + radius);
            ctx.quadraticCurveTo(x, y, x + radius, y);
            ctx.closePath();
            if (stroke) {
                ctx.stroke();
            }
            if (fill) {
                ctx.fill();
            }
        },        
        
		setupTimer: function(controller) {
			window.totalSeconds = 0;
			window.secondsInterval = window.setInterval(function(){
				window.totalSeconds++;
				
				if(MJS.settings.data.didInteractTimeLimitEnabled==="on") {
					if(window.totalSeconds === MJS.settings.data.didInteractTimeLimit) {
						if(ig.game.pointer.clickCount <= 0) {
							controller.endGame();
						}
					}
				}	
        
		        if(MJS.settings.data.milestone && MJS.settings.data.milestone.length) {
					var time = (window.totalSeconds);
	
					if(MJS.settings.data.milestone.indexOf(time) >-1 ) {
						// Analytics
						// Milestone
						//PlayableSdk.track.milestone(time, "time", "");
					}	
				}
        
			}.bind(this),1000);
		},
		
		setupControls:function(){
			ig.input.unbindAll();

			// Mouse
			ig.input.initMouse();			
			ig.input.bind(ig.KEY.MOUSE1, 'click');
			
		},
		
		setupDirector:function()
		{			
			this.director = new ig.Director(this,[
				LevelGame,LevelEndCard
			]);										
			
			this.director.loadLevel(this.director.currentLevel);
		},
		
        checkCustomClose:function(){
            if(typeof(mraid) !== null && typeof(mraid) !== "undefined")
            {
                var bool = window.MJS.settings.getBool("disableCustomClose")
                if(bool)
                {
                    try
                    {
                        mraid.useCustomClose(true)
                    }
                    catch(err)
                    {
                        console.log("Mraid missing")
                    }
                }
            }
        },
        
		update: function() {
			// Framerate Tracking
			//PlayableSdk.updateFps();
			this.checkCustomClose();
            if( this.paused ) {
                // only update some of the entities when paused:
                for( var i = 0; i < this.entities.length; i++ ) {
                    if( this.entities[i].ignorePause ) {
                        this.entities[i].update();
                    }
                }
                this.checkEntities();
                // sort entities?
                if( this._doSortEntities || this.autoSort ) {
                    this.sortEntities();
                    this._doSortEntities = false;
                }
            }
            else {
                // Update all entities and backgroundMaps
                this.parent();
            }
		},
        
        drawFPS: function() {
            var fps = 0;
            if(ig.debug) {
                fps = Math.round(1000/ig.debug.debugTickAvg);
        
                ig.system.context.font = "24px arial";
                ig.system.context.fillStyle = "rgba(255,255,255,1)";
                ig.system.context.textAlign = 'left';
                ig.system.context.textBaseline = 'bottom';
                ig.system.context.fillText('FPS: ' + fps, 0.15*ig.system.width, 0.55*ig.system.height);
            }
			
		},

       	dctf: function() {
            this.COPYRIGHT;
        },	

		draw: function() {
			// Draw all entities and backgroundMaps
			this.parent();			
			// debug
			ig.game.drawFPS();

			this.dctf();
		}
		
	});
    
	// start game
	ig.gameStarted = false;
	ig.startGame = function() {
		
        console.log("start")
        MJS.view.initViewport();
		MJS.view.updateViewport();
		MJS.view.setupPage();
		
		if(MJS.view.viewport.forceOrientation) {
			if(window.mraid) {
				if( window.mraid.getVersion() ) {
					if( parseFloat(window.mraid.getVersion(), 10) >= 2 ) {
						// lock orientation
						window.mraid.setOrientationProperties (
							{
								"allowOrientationChange": false, 
								"forceOrientation" : MJS.view.viewport.forceOrientation	// portrait, landscape
							} 
						);
					}
					window.mraid.expand();
				}
			}
		}
        
		if(!ig.gameStarted) {
            
			// Start the Game with 60fps, a resolution, scale factor, loader
            if(ig.ua.mobile) {
                ig.main( '#canvas', MyGame, 60, MJS.view.viewport.mobileWidth, MJS.view.viewport.mobileHeight, 1,ig.SplashLoader );
            }
			else {
                ig.main( '#canvas', MyGame, 60, MJS.view.viewport.desktopWidth, MJS.view.viewport.desktopHeight, 1,ig.SplashLoader );
            }
			ig.gameStarted = true;
			
            //MJS.view.reorient();
            
            MJS.view.orientationHandler();
		}
	};
	MJS.settings.updateSettings();
    MJS.game.startGame(ig.startGame,25);
    
    //ig.MJSSDK.start(ig.startGame);
	this.DOMAINLOCK_BREAKOUT_ATTEMPT;
	this.END_OBFUSCATION;

});
