# README
### What is this repository for? ###

* An Ad Games template based on MarketJS's ImpactJS template, specifically designed for fullscreen [MRAID](http://www.iab.com/guidelines/mobile-rich-media-ad-interface-definitions-mraid) environment while supporting Non-MRAID environment.
* The build must only consist of a single minified game javascript file (game.js) and two index.html files for Google Adx and MoPub ad servers.
* CSS rules and @font-face custom fonts are injected from css-injection.js, since we are not using Cascading Style Sheet (CSS).
* Game assets are encoded into BASE64 strings and stored inside game.js for production.
* Since the ad game should be light-weighted, the game.js file size must not exceed 1 MB if possible.


### How do I get set up? ###
* All assets (images & fonts) must be encoded into BASE64 strings, due to [Same-origin policy](https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy) 
* Custom @font-face can be injected from css-injection.js via insertRule function. You will have to register by adding custom font CSS string into the cssQueue array: 
```javascript
var cssQueue = [
	// ...
    '@font-face { font-family: "bowlby"; src: url("media/fonts/bowlby.woff") format("woff"); }'
];
```

* To trigger preload of @font-face, you will have to add the custom font-family name into customFonts array from splash-loader.js
```javascript
        customFonts: [
            "bowlby"
        ],
```

* We will have to pack our graphical assets nicely into spritesheet, you can do this using [ShoeBox](https://renderhjs.net/shoebox/)
* Animation frame sheet can be packed with ShoeBox > Animation > Frame Sheet.
* All other images can be packed using ShoeBox > Sprites > Sprite Sheet.
* A standard Shoebox configuration for packing Sprite Sheet can be found in tools/spritesheet/sprite-sheet-settings.sbx
* We can register SpriteSheet into sprite-sheet.js
```javascript
// register spirtesheets here (to generate sprites' JSON, ShoeBox -> Sprite Sheet)
var spriteSheets = {
// ...

	"gameScreen": {
    	"frames": {
    		"banner-defeat.png": {
    			"frame": {"x":0, "y":194, "w":455, "h":137, "ox":0, "oy":0},
    			"size": {"w":455,"h":137},
    			"scale": 1
    		},
    		"button-black.png": {
    			"frame": {"x":0, "y":427, "w":160, "h":56, "ox":0, "oy":0},
    			"size": {"w":160,"h":56},
    			"scale": 1
    		},
    		"tooltip.png": {
    			"frame": {"x":0, "y":0, "w":590, "h":192, "ox":5, "oy":4},
    			"size": {"w":597,"h":196},
    			"scale": 1
    		},
    		"tutorial-hand.png": {
    			"frame": {"x":0, "y":333, "w":200, "h":92, "ox":0, "oy":0},
    			"size": {"w":200,"h":92},
    			"scale": 1
    		}

    	},
    	"meta": {
    		"image": "media/graphics/compressed/game-screen.png",
    		"size": {"w": 592, "h": 485}
    	}
    },
// ...

};
```

* By default, the template allow ad game to be re-oriented (Portrait <==> Landscape)
* To set lock screen, simply change the forceOrientation variable in handlers.js
* The screen size can be set from portraitWidth, portraitHeight, landscapeWidth & landscapeHeight as well. The default resolution is 500x700 or 700x500.
```javascript
window.igh = {
    // configurable
    portraitWidth: 500, 
    portraitHeight: 700, 
    landscapeWidth: 700, 
    landscapeHeight: 500,
    
    forceOrientation: "",	// can be "portrait", "landscape" or ""/false/null
    
// ...
```
Changed to ViewHelper inside MJS.view

* If the ad game is designed to be played in both orientation, 
you can re-locate or re-size the entities from **updateOnOrientationChange** function:
```javascript
updateOnOrientationChange: function() {
			// this function is fired once whenever orientation changed (portrait <=> landscape)
			// so you can re-locate or re-size here
			this.parent();
			
			switch(igh.viewport.forceOrientation) {
				case "portrait":
					this._POS = {
						x: 0.5*ig.system.width,
						y: 0.5*ig.system.height
					};
					break;
				case "landscape":
					this._POS = {
						x: 0.25*ig.system.width,
						y: 0.55*ig.system.height
					};
					break;
				default:
					// serve both orientation
					if(igh.viewport.orientation.portrait) {
						// portrait
						this._POS = {
							x: 0.5*ig.system.width,
							y: 0.5*ig.system.height
						};
					}
					else {
						// landscape (not portrait)
						this._POS = {
							x: 0.25*ig.system.width,
							y: 0.55*ig.system.height
						};
					}
				break;
			}
			
		},
// ...
```
