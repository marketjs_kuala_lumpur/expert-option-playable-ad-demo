#! /bin/bash
#
# MarketJS Deployment System
# -----------------------------------------------------------------------
# Copyright (c) 2012 MarketJS Limited. Certain portions may come from 3rd parties and
# carry their own licensing terms and are referenced where applicable. 
# -----------------------------------------------------------------------

# Usage: sh build-fractional.sh [options]
# Example: sh build-fractional.sh -b -c -r
#

NOW=$(date +"%d-%b-%Y")
CURRENT_DIRECTORY=${PWD##*/}
ARCHIVE_NAME=$CURRENT_DIRECTORY"-"$NOW"-build"

usage () {
	echo "Usage: sh build-fractional.sh [option]"
	echo "Options"
	echo "\t -b \t Build files"
	echo "\t -c \t Compress deliverable files into zip"
	echo "\t -r \t Report files size"
	echo "Working example: sh build-fractional.sh -b -c -r"
}

bake (){
    echo "Pre-Bake: remove debug from main.js"
    sed -i.bak '/impact.debug.debug/d' lib/game/main.js
    
	echo ""
	echo "Baking ..."
	echo ""
	
	cd tools
	./bake.sh
	cd ..
	
	echo ""
	echo "Baking Done!"
	echo ""
    
    echo "Post-Bake: restore main.js"
    mv -f lib/game/main.js.bak lib/game/main.js
}

inject_build_timestamp(){
	echo "injecting build timestamp"
	
	echo $NOW
	sed -e 's/BUILD_TIMESTAMP.*,/BUILD_TIMESTAMP: "'"$NOW"'",/' lib/game/main.js > lib/game/main.js.tmp && mv lib/game/main.js.tmp lib/game/main.js
	
	echo "Done"
}

prep_production (){
	echo "Create basic .html ..."
	cp dev.html $CURRENT_DIRECTORY"_adx".html
	cp dev.html $CURRENT_DIRECTORY"_mopub".html
	echo "Done ..."
	
	echo "Cleaning up paths ..."
	
	# Clean JS paths
	sed -n '/glue\/playable_sdk.min.js/!p' $CURRENT_DIRECTORY"_adx".html > temp && mv temp $CURRENT_DIRECTORY"_adx".html
	sed -n '/glue\/css-injection.js/!p' $CURRENT_DIRECTORY"_adx".html > temp && mv temp $CURRENT_DIRECTORY"_adx".html
	sed -n '/glue\/game-settings.js/!p' $CURRENT_DIRECTORY"_adx".html > temp && mv temp $CURRENT_DIRECTORY"_adx".html
	sed -n '/glue\/sprite-sheets.js/!p' $CURRENT_DIRECTORY"_adx".html > temp && mv temp $CURRENT_DIRECTORY"_adx".html
	sed -n '/glue\/handlers.js/!p' $CURRENT_DIRECTORY"_adx".html > temp && mv temp $CURRENT_DIRECTORY"_adx".html
	sed -n '/media\/texts\/localised-strings.js/!p' $CURRENT_DIRECTORY"_adx".html > temp && mv temp $CURRENT_DIRECTORY"_adx".html
	sed -n '/lib\/impact\/impact.js/!p' $CURRENT_DIRECTORY"_adx".html > temp && mv temp $CURRENT_DIRECTORY"_adx".html	
	sed -i.bak 's/lib\/game\/main.js/game.js/g' $CURRENT_DIRECTORY"_adx".html
	
	# Clean JS paths
	sed -n '/glue\/playable_sdk.min.js/!p' $CURRENT_DIRECTORY"_mopub".html > temp && mv temp $CURRENT_DIRECTORY"_mopub".html
	sed -n '/glue\/css-injection.js/!p' $CURRENT_DIRECTORY"_mopub".html > temp && mv temp $CURRENT_DIRECTORY"_mopub".html
	sed -n '/glue\/game-settings.js/!p' $CURRENT_DIRECTORY"_mopub".html > temp && mv temp $CURRENT_DIRECTORY"_mopub".html
	sed -n '/glue\/sprite-sheets.js/!p' $CURRENT_DIRECTORY"_mopub".html > temp && mv temp $CURRENT_DIRECTORY"_mopub".html
	sed -n '/glue\/handlers.js/!p' $CURRENT_DIRECTORY"_mopub".html > temp && mv temp $CURRENT_DIRECTORY"_mopub".html
	sed -n '/media\/texts\/localised-strings.js/!p' $CURRENT_DIRECTORY"_mopub".html > temp && mv temp $CURRENT_DIRECTORY"_mopub".html
	sed -n '/lib\/impact\/impact.js/!p' $CURRENT_DIRECTORY"_mopub".html > temp && mv temp $CURRENT_DIRECTORY"_mopub".html	
	sed -i.bak 's/lib\/game\/main.js/game.js/g' $CURRENT_DIRECTORY"_mopub".html
	
	# Remove temp files
	echo "Removing temp files ..."
	rm *.bak
	rm temp	
	echo "Done!"      
}

prep_adx(){
	echo "Preparing "$CURRENT_DIRECTORY"_adx".html
	URL=$'"%%CLICK_URL_UNESC%%https%3A%2F%2Fnode{SERVER_NODE}.fractionalmedia.com%2Fclick2_bin%3Fbid_id%3D{BIDID}_{CAMP_ID}";'
	sed -i.bak 's/var clickurl.*;/var clickurl = '"$URL"'/g' $CURRENT_DIRECTORY"_adx".html
	sed -i.bak 's/preloaderStartCountdown: true,/preloaderStartCountdown: false,/g' $CURRENT_DIRECTORY"_adx".html
	echo "Done!"        
}

prep_mopub(){
	echo "Preparing "$CURRENT_DIRECTORY"_mopub".html
	URL=$'"https:\/\/node0.fractionalmedia.com\/click2_bin?bid_id={BIDID}_{CAMPAIGNID}\&hp=1\&version=" + adInfo.version;'
	sed -i.bak 's/var clickurl.*;/var clickurl = '"$URL"'/g' $CURRENT_DIRECTORY"_mopub".html
	sed -i.bak 's/preloaderStartCountdown: false,/preloaderStartCountdown: true,/g' $CURRENT_DIRECTORY"_mopub".html
	echo "Done!"        
}

compile_game (){
	echo "Compiling game.js for testing ..."                
	java -jar compiler.jar \
	--warning_level=QUIET \
	--js=glue/playable_sdk.min.js \
	--js=glue/css-injection.js \
	--js=glue/game-settings.js \
	--js=glue/sprite-sheets.js \
	--js=glue/handlers.js \
	--js=media/texts/localised-strings.js \
	--js=game.min.js \
	--js_output_file=game.js \
	--language_in=ECMASCRIPT5        
	echo "Done!"
	
	# Remove temp files
	echo "Removing temp files ..."
	rm *.bak
	rm game.min.js
	echo "Done!"
}

encode_media (){
    python encode_media.py game.js
}

compress () {
	echo "Building archive ..."
	
	if [ ! -f "$ARCHIVE_NAME.zip" ]; 
	then
	    echo "File not found!"
	else
	echo "File exist. Removing"
	rm "$ARCHIVE_NAME.zip"
	fi
	
	zip -r "../$ARCHIVE_NAME.zip" ./$CURRENT_DIRECTORY"_adx".html ./$CURRENT_DIRECTORY"_mopub".html ./game.js -x "*.DS_Store" 
	
	echo "Done"
}

report_size () {
	echo "$CURRENT_DIRECTORY Size Report"
	
	echo ""
	echo $CURRENT_DIRECTORY"_adx".html
	du -h -s $CURRENT_DIRECTORY"_adx".html
	
	echo ""
	echo $CURRENT_DIRECTORY"_mopub".html
	du -h -s $CURRENT_DIRECTORY"_mopub".html
	
	echo ""
	echo "game.js"
	du -h -s game.js
	
	echo ""
	echo "Done"
}

while getopts ":hbcr" opt; do
	case $opt in
		h)
			usage
		;;
		b)
			inject_build_timestamp
			bake
			prep_production
			prep_adx
			prep_mopub
			compile_game
            encode_media
        ;;
		c)
			compress
		;;
		r)
			report_size
	    ;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			usage
		exit;;
	esac
done

if [ $OPTIND -eq 1 ]
then 
	echo "No options were passed"
	usage
fi
