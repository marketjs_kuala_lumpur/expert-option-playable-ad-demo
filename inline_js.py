#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import re

FILE_NAMES = sys.argv[1:]
GAME_SCRIPT_REGEX = re.compile('<script.*?src="game.js(.*?)</script>')
SOURCE_CODE_MACRO = "{SOURCE_CODE}"

if not FILE_NAMES:
    print 'Please supply two files'
    print 'Usage: python inline_js.py game.html game.js'
else:
    source_code = open(FILE_NAMES[1], 'r').read()
    
    # Read in the file
    with open(FILE_NAMES[0], 'r') as file :
        html_file_data = file.read()
    
    # Replace the target string
    # html_file_data = html_file_data.replace('<script charset="utf-8" id="gameloader" type="text/javascript" src="game.js"></script>', '<script charset="utf-8" id="gameloader" type="text/javascript">' + source_code + '</script>')
    
    # TODO: try not to hardcode the script tag to be replaced, might lost some important attributes here.
    html_file_data = re.sub(GAME_SCRIPT_REGEX, '<script charset="utf-8" id="gameloader" type="text/javascript">' + SOURCE_CODE_MACRO + '</script>', html_file_data)
    html_file_data = html_file_data.replace(SOURCE_CODE_MACRO, source_code)
    
    # Write the file out again
    with open(FILE_NAMES[0], 'w') as file:
        file.write(html_file_data)
    