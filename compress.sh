#! /bin/bash
# Usage: zip-media-folder.sh en  : en = English
# Will zip media folder to media.zip, into _factory/localization/en/media.zip
NOW=$(date +"%d-%b-%Y")
CURRENT_DIRECTORY=${PWD##*/}
ARCHIVE_NAME=$CURRENT_DIRECTORY"-"$NOW
temparchive=${ARCHIVE_NAME// /_}
echo "Building archive ..."

if [ ! -f "./$temparchive.zip" ]; 
then
    echo "File not found!"
else
echo "File exist. Removing"
rm "./$temparchive.zip"
fi

zip -r "./$temparchive.zip" ./index.html ./game.js -x "*.DS_Store" 

echo "Done"
