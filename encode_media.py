#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import re
import base64
import mimetypes

# Mimetypes Definition 
mimetypes.add_type('application/x-font-woff', '.woff')

FILE_NAMES = sys.argv[1:]
MEDIA_FILES = []
MEDIA_REGEX = re.compile("\"media\/.*?\"|\'media\/.*?\'")

if not FILE_NAMES:
    print 'Please supply a file'
    print 'Usage: python encode_media.py game.js'
else:
    for file_name in FILE_NAMES:

        with open(file_name, 'r+') as game_file:
            game_file.seek(0)

            for line in game_file:
                matched_pattern = MEDIA_REGEX.findall(line)
                if matched_pattern:
                    for match_file in matched_pattern:
                        match_file = match_file[1:-1]  # trim starting & tailing quotes
                        if not match_file in MEDIA_FILES:
                            MEDIA_FILES.append(match_file)

            game_file.seek(0)
            source_code = game_file.read()
            for media in MEDIA_FILES:
                media_mime_type = \
                    mimetypes.MimeTypes().guess_type(media)[0]
                media_encoded_data = base64.b64encode(open(media, 'rb'
                        ).read())

                encoded_data_uri = 'data:' + media_mime_type \
                    + ';base64,' + media_encoded_data
                source_code = source_code.replace(media,
                        encoded_data_uri)

            game_file.seek(0)
            game_file.write(source_code)

        game_file.closed

			