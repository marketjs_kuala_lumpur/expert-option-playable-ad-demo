#!/bin/bash
rm -rf parseoutput
mkdir parseoutput

#GREP CHECK
STRINGS_TO_CHECK1=('PlayableSdk.cfg.get')
for string in "${STRINGS_TO_CHECK1[@]}"; do 
    find "./glue/game-settings.js" -type f -print0 | xargs -0 grep -Fn "$string" --exclude "playable_sdk.min.js" > "parseoutput/settings.js"
done

#sed 's/^.*(//;s/)$//' parseoutput/settings.js > parseoutput/temp.js
cat parseoutput/settings.js |cut -d "(" -f2- > parseoutput/temp.js
sed -e 's/PlayableSdk.cfg.get(//g' parseoutput/temp.js > parseoutput/temp2.js
#sed 's/PlayableSdk.cfg.get(\(.*\))/\1/' parseoutput/settings.js > parseoutput/temp.js
#sed -n '/PlayableSdk.cfg.get/!p' parseoutput/temp2.js > parseoutput/temp3.js
cat parseoutput/temp2.js | cut -d ")" -f1 > parseoutput/settingstext.js

STRINGS_TO_CHECK2=('PlayableSdk')
#to get tracking events
#for each folder in this directory
find * -prune -type d | while read d; do
    echo "###$d###"
    for string in "${STRINGS_TO_CHECK2[@]}"; do 
        echo "##Searching for $string in $d##"
        
        find "./$d" -type f -print0 | xargs -0 grep -Fn "$string" --exclude "playable_sdk.min.js" --exclude "game.js" --exclude="*.txt" | grep -vF '//PlayableSdk' | grep -vF '// PlayableSdk' > "parseoutput/$d-plugins.txt"
        #find "./$d" -type f -print0 | xargs -0 grep -Fn "$string" --exclude "playable_sdk.min.js" --exclude "game.js" --exclude="*.txt" |grep -vF '//PlayableSdk' | grep -vF '// PlayableSdk' > "parseoutput/$d-entities.txt"
    done
done

cat parseoutput/*.txt > parseoutput/output.txt
cat parseoutput/output.txt | grep -Fn "first_user_action" > parseoutput/FUA.txt
cat parseoutput/output.txt | grep -Fn '"tutorial", "action", "completed"' > parseoutput/tute.txt
cat parseoutput/output.txt | grep -Fn "time" > parseoutput/time.txt
cat parseoutput/output.txt | grep -Fn '//PlayableSdk.track' | grep -vF '//	PlayableSdk' > parseoutput/TRACK.txt
